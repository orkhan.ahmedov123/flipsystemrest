/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.utils;

import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author otahmadov
 */
public class CryptoUtils {
    private static final String secret = "EMSYS_SECRET_KEY";
//    private static final Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
//    private static final Cipher cipher = Cipher.getInstance("AES");
    private static final byte[] salt = {
            (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
            (byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03
        };
    private static int iterationCount = 19;
    private static KeySpec keySpec = new PBEKeySpec(secret.toCharArray(), salt, iterationCount);
    
    private static Cipher cipher;
    public static String encryptDataPBEWithMD5AndDES(String text) {
        try {
            
            SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
            cipher = Cipher.getInstance(key.getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
            String charSet = "UTF-8";
            byte[] in = text.getBytes(charSet);
            byte[] out = cipher.doFinal(in);
            String encStr = new String(Base64.getEncoder().encode(out));
            return encStr;
            
        } catch (Exception e) {
        }
        return "";
    }
    
    public static String decryptDataPBEWithMD5AndDES(String text) {
        try {
            SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
            cipher = Cipher.getInstance(key.getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            byte[] enc = Base64.getDecoder().decode(text);
            byte[] utf8 = cipher.doFinal(enc);
            String charSet3 = "UTF-8";
            String plainStr = new String(utf8, charSet3);
            return plainStr;
        } catch (Exception e) {
        }
        return "";
    }
}
