/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.utils;

/**
 *
 * @author ahmadov
 */
public class OrganizationStructureCode {
    public static String UNIVERSITY = "110000018";
    public static String UNIVERSITY_FORMULA = "110000018";
    public static String BRANCH = "110000028";
    public static String BRANCH_FORMULA = "110000018*110000028";
    public static String FACULTY = "110000029";
    public static String FACULTY_FORMULA = "110000018*110000028*110000029";
    public static String DEPARTMENT = "110000030";
    public static String DEPARTMENT_FORMULA = "110000018*110000028*110000029*110000030";
    public static String MASTER = "110000031";
    public static String MASTER_FORMULA = "110000018*110000028*110000029*110000031";
    public static String BACHOLER = "110000032";
    public static String BACHOLER_FORMULA = "110000018*110000028*110000029*110000032";
    public static String MASTER_SPEC = "110000033";
    public static String MASTER_SPEC_FORMULA = "110000018*110000028*110000029*110000031*110000033";
    public static String BACHOLAVR_SPEC = "110000034";
    public static String BACHOLAVR_SPEC_FORMULA = "110000018*110000028*110000029*110000032*110000034";
    
}
