/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.utils;

import com.flip.Exception.QException;
import com.flip.domain.SessionManager;
import com.flip.domain.Transporter;
import static com.flip.utils.SQLGenerator.getTableNameBasedOnEntityForView;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author otahmadov
 */
public class EntityManager {
    public static String ENTITY = "ENTITY";
    public static String ENTITY_PREFIX = "entity";
    public static String SQL_QUERY = "sqlQuery";
    public static String SQL_POOL_TABLE = "sqlPool";
    private static final String PARAM_KEY = ":param";
    private static final String REFERENCE_KEY_NAME = "db.reference.key-name.primary";
    private static final String HAS_INSERT_DATE = "db.has.insert-date";
    private static final String HAS_MODIFICATION_DATE = "hasModificationDate";
    private static final String DELETE_STATUS = "db.delete.status.primary";
    public static String GET = "get";
    public static String SEPERATOR_DOT = ".";
    public static String SEPERATOR_COMMA = ",";
    public static String UNDERSCORE = "_";
    public static String UNION = "UNION";
    public static String SPACE = " ";
    public static String SUM_BY = "sumBy";
    public static String GROUP_BY = "groupBy";
    public static String INTERVAL_START_DATE = "intervalStartDate";
    public static String INTERVAL_END_DATE = "intervalEndDate";
    public static String INTERVAL_FIELD = "intervalField";
    public static String INTERVAL_SEPERATOR = "intervalSeperator";
    public static String EMPTY_VALUE = "A%%##%%##A";
    public static String ENTITY_LABEL_TYPE = "INTEGER";

    public static String convertEntityNameToDBTableName(LocalEntity core) {
        return SQLGenerator.getTableNameBasedOnEntity(core);
    }

    public static void insert(LocalEntity entity) throws QException, Exception {
        ArrayList valueList = new ArrayList();
        if(entity.getId() == null || entity.getId().trim().isEmpty())
            entity.setId(IdGenerator.getId());
        addDateToInsert(entity);
        addCreateUserToInsert(entity);
        entity.setActive("1");
        valueList.addAll(SQLGenerator.getValuesOfAllGetMethodsOfEntityForInsert(entity)); // insertList de olan parametrlerin value sini goturur
        String query = SQLGenerator.insertGeneratorNew(entity);
        if (!query.isEmpty()) {
                SQLConnection.execInsertSql(query, valueList);
        }
        
    }

    private static void addDateToInsert(LocalEntity entity) throws QException {
        
          entity.setCreateDate(QDate.getCurrentDate());
          entity.setUpdateDate(QDate.getCurrentDate());
    }
    
    private static void addCreateUserToInsert(LocalEntity entity) throws QException, Exception {
        if(entity.getCreateUserId().trim().isEmpty())
          entity.setCreateUserId(SessionManager.getCurrentUserId());
        if(entity.getUpdateUserId().trim().isEmpty())
          entity.setUpdateUserId(SessionManager.getCurrentUserId());
    }
    
    private static void addDateToUpdate(LocalEntity entity) throws QException {
          entity.setUpdateDate(QDate.getCurrentDate());
    }
    
    private static void addCreateUserToUpdate(LocalEntity entity) throws QException, Exception {
          entity.setUpdateUserId(SessionManager.getCurrentUserId());
    }

    private static void setReferenceValue(LocalEntity entity) throws QException, Exception {
            String refName = "id";
            String val = IdGenerator.getId(entity);
            setReferenceKeyValue(entity, refName, val);
    }

    public static Transporter select(LocalEntity entity, Statement statement) throws QException {
        return select(entity, LocalLabel.DB_PRIMARY);
    }

    public static Transporter select(LocalEntity entity) throws QException {
        return select(entity, entity.toTableName());
    }

    public static Transporter selectSpec(LocalEntity entity) throws QException {// carrieri sonadek istifade edib butun tableleri icerisine yigiriq;
         return selectSpec(entity, entity.toTableName());
    }

    public static Transporter select(LocalEntity entity, String tablename) throws QException {
        Transporter carrier = new Transporter();
        try {
            carrier = coreSelect(entity, tablename);
            return carrier;
        } catch (Exception ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }

    public static Transporter selectSpec(LocalEntity entity, String tablename) throws QException { // carrieri sonadek istifade edib butun tableleri icerisine yigiriq;
        Transporter carrier = new Transporter();
        try {
             carrier = coreSelectSpec(entity, tablename);
             
             return carrier;
        } catch (Exception ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }

    private static Transporter coreSelect(LocalEntity entity, String tablename) throws QException, Exception {

        String methodNames[] = SQLGenerator.getAllGetMethodNames(entity); 
        
        ArrayList valueList = new ArrayList();
        valueList.addAll(SQLGenerator.getValuesOfAllGetMethodsOfEntityForAndStatement(entity)); // andStatementList de olan parametrlerin value sini goturur
       
        String query = SQLGenerator.selectGenerator(entity, methodNames);
//        System.out.println("Query: " + query);
        Transporter carrier = new Transporter();
        carrier = SQLConnection.execSelectSql(query, tablename, valueList);
        int allRow = getRowCount(entity, valueList, false);
        carrier.addTableAllRow(tablename.toLowerCase(), String.valueOf(allRow));
            
        if (carrier.getTableRowCount(tablename) == 1) {
            mapCarrierToEntity(carrier, tablename, 0, entity);
        }
        return carrier;
    }

    private static Transporter coreSelectSpec(LocalEntity entity, String tablename) throws QException, Exception {
        Transporter carrier = new Transporter();
        String methodNames[] = SQLGenerator.getAllGetMethodNames(entity); 

        ArrayList valueList = new ArrayList();
        valueList.addAll(SQLGenerator.getValuesOfAllGetMethodsOfEntityForAndStatement(entity)); // andStatementList de olan parametrlerin value sini goturur
       
        String query = SQLGenerator.selectGenerator(entity, methodNames);
//        System.out.println("Query: " + query);
        carrier = SQLConnection.execSelectSqlSpec(query, tablename, valueList); // carrieri sonadek istifade edib butun tableleri icerisine yigiriq;
        return carrier;
    }

    public static Transporter selectTableSequenceAndHidden(LocalEntity entity, Transporter c) throws QException, Exception {
        String userId = SessionManager.getCurrentUserId();
        if(!userId.trim().isEmpty() && Long.parseLong(userId) > 0) {
            String query1 = SQLGenerator.tableSequenceGenerator(entity);
//            String query2 = SQLGenerator.hiddenColumnGenerator(entity, userId);

            String result1 = SQLConnection.specSelectSql(query1);
//            String result2 = SQLConnection.specSelectSql(query2);
            String seqColumn = SessionManager.sessionTransporter().getValue("seqColumn").toString();
            if(seqColumn != null && !seqColumn.trim().isEmpty()) {
                result1 = seqColumn;
            }
            String viewName = entity.toTableName().toLowerCase();
            c.addTableSequence(viewName, result1 != null ? result1 : "");
            c.addTableHidden(viewName, "");
//            c.addTableHidden(viewName, result2 != null ? result2 : "");
        }
        
        return c;
    }

    public static Transporter selectTableSequenceAndHiddenSpec(LocalEntity entity, String viewName) throws QException, Exception {
        String userId = SessionManager.getCurrentUserId();
        Transporter c = SessionManager.sessionTransporter();
        if(Long.parseLong(userId) > 0) {
            String query1 = SQLGenerator.tableSequenceGenerator(entity);
//            String query2 = SQLGenerator.hiddenColumnGenerator(entity, userId);

            String result1 = SQLConnection.specSelectSql(query1);
//            String result2 = SQLConnection.specSelectSql(query2);
            String seqColumn = SessionManager.sessionTransporter().getValue("seqColumn").toString();
            if(seqColumn != null && !seqColumn.trim().isEmpty()) {
                result1 = seqColumn;
            }
            c.addTableSequence(viewName, result1 != null ? result1 : "");
            c.addTableHidden(viewName, "");
//            c.addTableHidden(viewName, result2 != null ? result2 : "");
        }
        
        return c;
    }

    private static String getAttributeNameFromMethodName(String methodname) {
        String fname = methodname.substring("GET".length(), methodname.length());
        fname = lowerFirstLetter(fname);
        return fname;
    }

    public static Transporter getFieldTitlesByMethodnames(Transporter carrier, LocalEntity entity, String[] methodnames) {
        return carrier;
    }

    public static Transporter selectBySqlId(String sqlId, String[] params) throws QException {
        return selectBySqlId(sqlId, params, LocalLabel.DB_PRIMARY);
    }

    private static ArrayList convertArrayToArrayList(String[] arg) {
        ArrayList arr = new ArrayList();
        for (int i = 0; i < arg.length; i++) {
            arr.add(arg[i]);
        }
        return arr;
    }

    public static Transporter selectBySqlId(String sqlId, String[] params, String destinationDatabaseNumber) throws QException {
        try {
            Transporter c = null;
            return c;
        } catch (Exception ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }

    public static Transporter executeUpdateByQuery(String query) throws QException {
        try {
            Transporter c = new Transporter();
            if (!query.trim().equals("")) {
//                c = SQLConnection.execDeleteSql(query);
            }

            return c;
        } catch (Exception ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }

    public static Transporter selectBySql(String sqlQuery) throws QException {
        return selectBySql(sqlQuery, new String[]{});
    }

    public static Transporter selectBySql(String sqlQuery, String[] params) throws QException {
        try {
            String query = sqlQuery;
            Transporter c = null;
            if (!query.trim().equals("")) {
                c = SQLConnection.execSelectSql(query, LocalLabel.RESULT_SET, convertArrayToArrayList(params));
            }
            return c;
        } catch (Exception ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }

    public static Transporter selectBySqlId(String sqlId) throws QException {
        return selectBySqlId(sqlId, new String[]{});
    }

    public static String replaceParamsInSqlQuery(String sqlQuery, String[] params) {
        for (int i = 1; i <= params.length; i++) {
            sqlQuery = sqlQuery.replace(PARAM_KEY + i, params[i - 1]);
        }
        return sqlQuery;
    }

    public static void update(LocalEntity entity) throws QException, Exception {
        ArrayList valueList = new ArrayList();
        if (entity.getId().trim().equals("")) {
            throw new QException("id is empty");
        }
        addDateToUpdate(entity);
        addCreateUserToUpdate(entity);

        valueList.addAll(SQLGenerator.getValuesOfAllGetMethodsOfEntityForUpdate(entity)); // updateList de olan parametrlerin value sini goturur
        String query = SQLGenerator.updateGeneratorForUpdate(entity, valueList);
        if (!query.isEmpty()) {
                SQLConnection.execUpdateSql(query, valueList);
        }
    }

    public static void updateByOtherParams(LocalEntity entity, Transporter carrier, List<String> params) throws QException, Exception {
        ArrayList valueList = new ArrayList();
        addDateToUpdate(entity);
        addCreateUserToUpdate(entity);
        valueList.addAll(SQLGenerator.getValuesOfAllGetMethodsOfEntityForUpdate(entity)); // updateList de olan parametrlerin value sini goturur
        String query = SQLGenerator.updateGeneratorForUpdateByOtherParams(entity, valueList, carrier, params);
//        System.out.println(query);
        if (!query.isEmpty()) {
                SQLConnection.execUpdateSql(query, valueList);
        }
    }

    private static String[] removeReferenceKeyFromMethodsList(String[] methods, String rKey) {
        String[] t = new String[methods.length - 1];
        int idx = 0;
        for (String method : methods) {
            if (!method.equals(GET + capitalizeOnlyFirstLetter(rKey))) {
                t[idx] = method;
                idx++;
            }
        }
        return t;
    }

    public static String getReferenceKeyName(String databaseNumber) {
        String propKeyName = REFERENCE_KEY_NAME + "." + databaseNumber.toLowerCase();
        String keyName = null;
        return keyName;
    }

    public static String getReferenceKeyValue(LocalEntity entity, String keyName) throws QException {
        try {
            String methodName = "get" + capitalizeFirstLetter(keyName);

            Method method = entity.getClass().getMethod(methodName);
            Object retObj = method.invoke(entity);
            String rs = (String) retObj;
            return rs;
        } catch (Exception ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }

    private static String setReferenceKeyValue(LocalEntity entity, String keyName, String keyValue) throws QException, NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            String methodName = "set" + capitalizeFirstLetter(keyName);
            Method method = entity.getClass().getMethod(methodName, String.class);
            Object retObj;
            retObj = method.invoke(entity, keyValue);
            String rs = (String) retObj;

            return rs;
    }

    static String capitalizeFirstLetter(String arg) {
        arg = arg.substring(0, 1).toUpperCase() + arg.substring(1, arg.length()).toLowerCase();
        return arg;
    }
    
    public static void delete(LocalEntity entity) throws QException, SQLException {
        ArrayList valueList = new ArrayList();
        if (entity.getId().trim().equals("")) {
            throw new QException("id is empty");
        }
        addDateToUpdate(entity);
        String methodNames[] = new String[]{"getActive", "getUpdateDate"};
        valueList.add("0");
        valueList.add(QDate.getCurrentDate());

        String query = SQLGenerator.updateGeneratorForDelete(entity, methodNames, valueList);
        if (!query.isEmpty()) {
                SQLConnection.execDeleteSql(query, valueList);
        }
    }
    
    public static void deleteWihtoutActive0(LocalEntity entity) throws QException, Exception {
        ArrayList valueList = new ArrayList();
        
        valueList.addAll(SQLGenerator.getValuesOfAllGetMethodsOfEntityForDeleteWithoutActive0(entity));
        
        String query = SQLGenerator.deleteGeneratorForWithoutActive0(entity);
        if (!query.isEmpty()) {
                SQLConnection.execDeleteSqlWithoutActive0(query, valueList);
        }
    }
    
    public static void deleteWihtoutActive0(LocalEntity entity, Transporter carrier) throws QException, Exception {
        ArrayList valueList = new ArrayList();
        mapCarrierToEntity(carrier, entity);
        valueList.addAll(SQLGenerator.getValuesOfAllGetMethodsOfEntityForDeleteWithoutActive0(entity));
        
        String query = SQLGenerator.deleteGeneratorForWithoutActive0(entity);
        if (!query.isEmpty()) {
                SQLConnection.execDeleteSqlWithoutActive0(query, valueList);
        }
    }

    public static String getStatusValueOfDeleteByDatabaseNumber(String databaseNumber) {
        String propKeyName = DELETE_STATUS + "." + databaseNumber.toLowerCase();
        String keyName = null;
        return keyName;
    }

    public static String[] getAllAtributes(LocalEntity core) {
        int idx = 0;
        String[] ListOfGetMethods = new String[1001];
        Method m[] = core.getClass().getMethods();
        Method m1[] = LocalEntity.class.getMethods();

        for (Method m2 : m) {
            if (m2.toString().contains(core.getClass().getName() + SEPERATOR_DOT + GET)) {
                String[] S = m2.toString().split(" ");
                ListOfGetMethods[idx] = S[S.length - 1].substring(core.getClass().getName().length() + 1, S[S.length - 1].length() - 2);
                idx++;
            }
        }

        //select all GET methods from LocalEntity
        for (Method m11 : m1) {
            if (m11.toString().contains(LocalEntity.class.getName() + SEPERATOR_DOT + GET)) {
                String[] S = m11.toString().split(" ");
                ListOfGetMethods[idx] = S[S.length - 1].substring(LocalEntity.class.getName().length() + 1, S[S.length - 1].length() - 2);
                idx++;
            }
        }

        String[] ls = new String[idx];
        for (int i = 0; i < idx; i++) {
            String t = ListOfGetMethods[i];
            ls[i] = lowerFirstLetter(t.substring(GET.length(), t.length()));
        }
        return ls;
    }

    private static String lowerFirstLetter(String arg) {
        return arg = arg.substring(0, 1).toLowerCase() + arg.substring(1, arg.length());
    }
    
    //Carrier den gelen deyerleri entity classimizin icerisindeki uygun field e menimsedir
    public static void mapCarrierToEntity(Transporter carrier, LocalEntity entity, boolean setAll) throws QException {
        String[] attributes = getAllAtributes(entity);
        for (String attribute : attributes) {
            String val = carrier.isKeyExist(attribute) ? carrier.getValue(attribute).toString() : EMPTY_VALUE;
            if (setAll && (!val.equals(EMPTY_VALUE))) {
                setEntityValue(entity, attribute, val);
            } else if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
        if (carrier.isKeyExist(LocalLabel.START_LIMIT)) {
            entity.setStartLimit(carrier.getValue(LocalLabel.START_LIMIT).toString());
        }
        if (carrier.isKeyExist(LocalLabel.END_LIMIT)) {
            entity.setEndLimit(carrier.getValue(LocalLabel.END_LIMIT).toString());
        }

        if (carrier.isKeyExist(LocalLabel.SORT_TABLE)) {
            entity.addSortBy(carrier.getValue(LocalLabel.SORT_TABLE).toString().split(LocalLabel.WS_SORT_TABLE_SEPERATOR));
        }

        if (carrier.isKeyExist(INTERVAL_END_DATE)) {
            entity.setIntervalEndDate(carrier.getValue(INTERVAL_END_DATE).toString().trim());
        }

        if (carrier.isKeyExist(INTERVAL_FIELD)) {
            entity.setIntervalField(carrier.getValue(INTERVAL_FIELD).toString().trim());
        }

        if (carrier.isKeyExist(INTERVAL_SEPERATOR)) {
            entity.setIntervalSeperator(carrier.getValue(INTERVAL_SEPERATOR).toString().trim());
        }

        if (carrier.isKeyExist(INTERVAL_START_DATE)) {
            entity.setIntervalStartDate(carrier.getValue(INTERVAL_START_DATE).toString().trim());
        }

        if (carrier.isKeyExist(SUM_BY)) {
            entity.setSumBy(carrier.getValue(SUM_BY).toString().trim());
        }

        if (carrier.isKeyExist(GROUP_BY)) {
            String[] groupByFields = carrier.getValue("groupBy").toString().split(LocalLabel.IN);
            entity.addGroupBy(groupByFields);
        }

        if (carrier.isKeyExist(LocalLabel.WS_SORT_TABLE_TYPE_ASC)) {
            entity.addSortBy(carrier.getValue(LocalLabel.WS_SORT_TABLE_TYPE_ASC).toString().split(LocalLabel.WS_SORT_TABLE_SEPERATOR));
            entity.setSortByAsc(true);
        }

        if (carrier.isKeyExist(LocalLabel.WS_SORT_TABLE_TYPE_DESC)) {
            entity.addSortBy(carrier.getValue(LocalLabel.WS_SORT_TABLE_TYPE_DESC).toString().split(LocalLabel.WS_SORT_TABLE_SEPERATOR));
            entity.setSortByAsc(false);
        }

        if (carrier.isKeyExist(LocalLabel.DISTINCT_FIELDS)) {
            entity.addDistinctField(carrier.getValue(LocalLabel.DISTINCT_FIELDS).toString().split(LocalLabel.WS_SORT_TABLE_SEPERATOR));
        }

        if (carrier.isKeyExist(LocalLabel.DISTINCT_FIELDS)) {
            entity.addDistinctField(carrier.getValue(LocalLabel.DISTINCT_FIELDS).toString().split(LocalLabel.WS_SORT_TABLE_SEPERATOR));
        }

        if (carrier.isKeyExist(LocalLabel.SORT_TABLE_TYPE)) {
            boolean sortType = carrier.getValue(LocalLabel.SORT_TABLE_TYPE).toString().
                    trim().equals(LocalLabel.WS_SORT_TABLE_TYPE_ASC);
            entity.setSortByAsc(sortType);
        }
    }
    public static void mapCarrierToEntityForUpdate(Transporter carrier, LocalEntity entity) throws QException {
        
        for (String attribute : entity.currentUpdateList()) {
            String val = carrier.isKeyExist(attribute) ? carrier.getValue(attribute).toString() : EMPTY_VALUE;
            if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
    }
    public static void mapCarrierToEntityForInsert(Transporter carrier, LocalEntity entity) throws QException {
        
        for (String attribute : entity.currentInsertList()) {
            String val = carrier.isKeyExist(attribute) ? carrier.getValue(attribute).toString() : EMPTY_VALUE;
            if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
    }
    public static void mapCarrierToEntityForAndStatement(Transporter carrier, LocalEntity entity) throws QException {
        
        for (String attribute : entity.currentAndStatementFildListForEqual()) {
            String val = carrier.isKeyExist(attribute) ? carrier.getValue(attribute).toString() : EMPTY_VALUE;
            if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
        for (String attribute : entity.currentAndStatementFildListForNotEqual()) {
            String val = carrier.isKeyExist(attribute) ? carrier.getValue(attribute).toString() : EMPTY_VALUE;
            if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
        for (String attribute : entity.currentAndStatementFildListForLike()) {
            String val = carrier.isKeyExist(attribute) ? carrier.getValue(attribute).toString() : EMPTY_VALUE;
            if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
        for (String attribute : entity.currentAndStatementFildListForFullLike()) {
            String val = carrier.isKeyExist(attribute) ? carrier.getValue(attribute).toString() : EMPTY_VALUE;
            if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
        for (String attribute : entity.currentAndStatementFildListForRightLike()) {
            String val = carrier.isKeyExist(attribute) ? carrier.getValue(attribute).toString() : EMPTY_VALUE;
            if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
        for (String attribute : entity.currentAndStatementFildListForLeftLike()) {
            String val = carrier.isKeyExist(attribute) ? carrier.getValue(attribute).toString() : EMPTY_VALUE;
            if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
        for (String attribute : entity.currentAndOrStatementFildListFullLike()) {
            String val = carrier.isKeyExist(attribute) ? carrier.getValue(attribute).toString() : EMPTY_VALUE;
            if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
        if (carrier.isKeyExist(LocalLabel.START_LIMIT)) {
                entity.setStartLimit(carrier.getValue(LocalLabel.START_LIMIT).toString());
        }
        if (carrier.isKeyExist(LocalLabel.END_LIMIT)) {
            entity.setEndLimit(carrier.getValue(LocalLabel.END_LIMIT).toString());
        }
        if (carrier.isKeyExist(LocalLabel.ORDER_BY_COLUMN) && carrier.isKeyExist(LocalLabel.ORDER_BY_SORT)) {
                entity.setOrderByColumn(carrier.getValue(LocalLabel.ORDER_BY_COLUMN).toString());
                entity.setOrderBySort(carrier.getValue(LocalLabel.ORDER_BY_SORT).toString());
        }
        if (carrier.isKeyExist("returnColumn")) {
                entity.setReturnColumn(carrier.getValue("returnColumn").toString());
        }
    }
    public static void mapCarrierToEntityForInsert(Transporter carrier, LocalEntity entity, String tableName, int row) throws QException {
        
        for (String attribute : entity.currentInsertList()) {
            String val = carrier.isKeyExist(tableName, row, attribute)
                    ? carrier.getValue(tableName, row, attribute).toString() : EMPTY_VALUE;
            if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
            
        }
    }
    
    public static void mapCarrierToEntityForDelete(Transporter carrier, LocalEntity entity) throws QException {
        
        for (String attribute : entity.currentDeleteWhereParamsList()) {
            String val = carrier.isKeyExist(attribute) ? carrier.getValue(attribute).toString() : EMPTY_VALUE;
            if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
    }

    public static void mapCarrierToEntity(Transporter carrier, LocalEntity entity) throws QException {
        mapCarrierToEntity(carrier, entity, true);
    }

    public static void mapEntityToCarrier(LocalEntity entity, Transporter carrier, String tableName, boolean setNulls) throws QException, Exception {
        String methodNames[] = SQLGenerator.getAllGetMethodNames(entity);
        String[] values = SQLGenerator.getValuesOfAllGetMethodsOfEntity(entity, methodNames);

        for (int i = 0; i < methodNames.length; i++) {
            String val = values[i];
            if (setNulls) {
                int row = carrier.getTableRowCount(tableName);
                String col = lowerFirstLetter(methodNames[i].substring(GET.length(), methodNames[i].length()));
                carrier.setValue(tableName, row, col, values[i]);
            } else if (!val.equals("")) {
                int row = carrier.getTableRowCount(tableName);
                String col = lowerFirstLetter(methodNames[i].substring(GET.length(), methodNames[i].length()));
                carrier.setValue(tableName, row, col, values[i]);
            }

        }
    }

    public static void mapEntityToCarrier(LocalEntity entity, Transporter carrier, boolean setNulls) throws QException, Exception {
        String methodNames[] = SQLGenerator.getAllGetMethodNames(entity);
        String[] values = SQLGenerator.getValuesOfAllGetMethodsOfEntity(entity, methodNames);

        for (int i = 0; i < methodNames.length; i++) {
            String val = values[i];
            if (setNulls) {
                String col = lowerFirstLetter(methodNames[i].substring(GET.length(), methodNames[i].length()));
                carrier.setValue(col, values[i]);
            } else if (!val.equals("")) {
                String col = lowerFirstLetter(methodNames[i].substring(GET.length(), methodNames[i].length()));
                carrier.setValue(col, values[i]);
            }

        }
    }

    public static void mapCarrierToEntity(Transporter carrier, String tableName, int row, LocalEntity entity, boolean setAll) throws QException {
        String[] attributes = getAllAtributes(entity);
        for (String attribute : attributes) {
            String f1 = entity.selectLongEntityFieldName(attribute);
            String val = carrier.isKeyExist(tableName, row, f1)
                    ? carrier.getValue(tableName, row, f1).toString() : EMPTY_VALUE;
            if (setAll && (!val.equals(EMPTY_VALUE))) {
                setEntityValue(entity, attribute, val);//EGER KEY(COLUMNNAME) MOVCUD DEYILSE O ZAMAN HEMIN ADLI SET METODU MOVCUD OLMAYACAQ
            } else if (!val.equals(EMPTY_VALUE)) {
                setEntityValue(entity, attribute, val);
            }
        }
        String startDate = carrier.isKeyExist(tableName, row, LocalLabel.START_LIMIT)
                ? carrier.getValue(tableName, row, LocalLabel.START_LIMIT).toString() : "";
        String endDate = carrier.isKeyExist(tableName, row, LocalLabel.END_LIMIT)
                ? carrier.getValue(tableName, row, LocalLabel.END_LIMIT).toString() : "";
        entity.setStartLimit(startDate);
        entity.setEndLimit(endDate);
        
        if (carrier.isKeyExist(tableName, row, LocalLabel.SORT_TABLE)) {
            entity.addSortBy(carrier.getValue(tableName, row, LocalLabel.SORT_TABLE).toString().split(LocalLabel.WS_SORT_TABLE_SEPERATOR));
        }

        if (carrier.isKeyExist(tableName, row, LocalLabel.WS_SORT_TABLE_TYPE_ASC)) {
            entity.addSortBy(carrier.getValue(tableName, row, LocalLabel.WS_SORT_TABLE_TYPE_ASC).toString().split(LocalLabel.WS_SORT_TABLE_SEPERATOR));
        }

        if (carrier.isKeyExist(tableName, row, LocalLabel.WS_SORT_TABLE_TYPE_DESC)) {
            entity.addSortBy(carrier.getValue(tableName, row, LocalLabel.WS_SORT_TABLE_TYPE_DESC).toString().split(LocalLabel.WS_SORT_TABLE_SEPERATOR));
        }

        if (carrier.isKeyExist(tableName, row, LocalLabel.DISTINCT_FIELDS)) {
            entity.addDistinctField(carrier.getValue(tableName, row, LocalLabel.DISTINCT_FIELDS).toString().split(LocalLabel.WS_INCLUDED_FIELDS_SEPERATOR));
        }

        if (carrier.isKeyExist(tableName, row, LocalLabel.SORT_TABLE_TYPE)) {
            boolean sortType = carrier.getValue(tableName, row, LocalLabel.SORT_TABLE_TYPE).toString().
                    trim().equals(LocalLabel.WS_SORT_TABLE_TYPE_ASC);
            entity.setSortByAsc(sortType);
        }
    }

    public static void mapCarrierToEntity(Transporter carrier, String tableName, LocalEntity entity, boolean setAll) throws QException {
        String[] attributes = getAllAtributes(entity);
        int rowc = carrier.getTableRowCount(tableName);
        for (int row = 0; row < rowc; row++) {
            mapCarrierToEntity(carrier, tableName, row, entity, setAll);
        }
    }

    public static void mapCarrierToEntity(Transporter carrier, String tableName, LocalEntity entity) throws QException {
        mapCarrierToEntity(carrier, tableName, entity, true);
    }

    public static void mapCarrierToEntity(Transporter carrier, String tableName, int row, LocalEntity entity) throws QException {
        mapCarrierToEntity(carrier, tableName, row, entity, true);
    }

    public static void setEntityValue(LocalEntity entity, String keyName, String keyValue) throws QException {
        try {
            String rs = "";
            String methodName = "set" + capitalizeOnlyFirstLetter(keyName);
            Method method = entity.getClass().getMethod(methodName, String.class);
            Object retObj = method.invoke(entity, keyValue);
        } catch (Exception ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }

    public static String getEntityValue(LocalEntity entity, String keyName) throws QException {
        try {
            String rs = "";
            String methodName = "get" + capitalizeOnlyFirstLetter(keyName);
            Method method = entity.getClass().getMethod(methodName);
            Object retObj = method.invoke(entity);
            return (String) retObj;
        } catch (Exception ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }

    static String capitalizeOnlyFirstLetter(String arg) {
        arg = arg.substring(0, 1).toUpperCase() + arg.substring(1, arg.length());
        return arg;
    }
    public static int getRowCount(LocalEntity entity, ArrayList valList, boolean withoutThis) throws QException {
        try {
            
            
            for(int i = 0; i < entity.currentAndStatementFildListForEqual().size(); i++) {
                if(entity.currentAndStatementFildListForEqual().get(i).toString().equals("id")) {
                    
                        entity.currentAndStatementFildListForEqual().remove(i);
                    if(!withoutThis)
                        valList.remove(i);
                }   
            }
            
            String query = "SELECT COUNT(ID)::text AS ROW_COUNT ";
            query += " FROM " + getTableNameBasedOnEntityForView(entity) + SPACE;
            query += SQLGenerator.generateWherePartOfSelectForAndStatement(entity);
            
            if(withoutThis) {
                query += " and id <> ?";
            }

            Transporter c = SQLConnection.execSelectSqlSpec(query, entity.toTableName(), valList);
            int rowCount = Integer.valueOf(c.getValue(entity.toTableName(), 0, "rowCount").toString() != null 
                                            && !c.getValue(entity.toTableName(), 0, "rowCount").toString().isEmpty() ? c.getValue(entity.toTableName(), 0, "rowCount").toString() : "0");
            return rowCount;
        } catch (Exception ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }
    public static int getRowCountWithDistinct(LocalEntity entity, Transporter carrier) throws QException {
        try {
            entity.setAndStatementFildList(carrier);
            mapCarrierToEntityForAndStatement(carrier, entity);
            String query = "SELECT COUNT(distinct ID) AS ROW_COUNT ";
            query += " FROM " + getTableNameBasedOnEntityForView(entity) + SPACE;
            query += SQLGenerator.generateWherePartOfSelectForAndStatement(entity);
            ArrayList valueList = new ArrayList();
            valueList.addAll(SQLGenerator.getValuesOfAllGetMethodsOfEntityForAndStatement(entity)); // andStatementList de olan parametrlerin value sini goturur

            Transporter c = SQLConnection.execSelectSqlSpec(query, entity.toTableName(), valueList);
            int rowCount = Integer.parseInt(c.getValue(entity.toTableName(), 0, "rowCount").toString());
            return rowCount;
        } catch (Exception ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }
    
    
    
    public static boolean checkParamsBySelectRowCount(String paramName, Transporter c, LocalEntity ent, boolean withoutThis) throws QException, Exception {
        
        if (!c.isKeyExist(paramName) || c.getValue(paramName).toString().trim().equals("")) {
            return false;
        }
        
        Transporter carrier = new Transporter();
        carrier.setValue(paramName, c.getValue(paramName).toString());
        
        if(withoutThis) {
            carrier.setValue("id", c.getValue("id").toString());
        }
        ent.setAndStatementFildList(carrier);
        
        mapCarrierToEntityForAndStatement(carrier, ent);
        
        ArrayList valueList = new ArrayList();
        valueList.addAll(SQLGenerator.getValuesOfAllGetMethodsOfEntityForAndStatement(ent));
        
        int count = getRowCount(ent, valueList, withoutThis);
        
        if(count > 0) {
            c.addError(paramName, "Duplicate " + paramName + " : " + c.getValue(paramName).toString());
            return true;
        }
            
        
        return false;
    }
    
    public static boolean checkParamsBySelectRowCountSpec(Transporter carrier, LocalEntity ent, boolean withoutThis, String param) throws QException, Exception {
        Transporter c = SessionManager.sessionTransporter();
        String id = c.getValue("id").toString();
        carrier = getEntValuesByCarrier(ent, carrier);
        int count = carrier.getTableRowCount(ent.toTableName());
        if(count > 0) {
            for(int i = 0; i < count; i++) {
                if(withoutThis) {
                    if(!id.equals(carrier.getValue(ent.toTableName(), i, "id")) && c.getValue(param).equals(carrier.getValue(ent.toTableName(), i, param))) {
                        c.addError(param, "Duplicate " + c.getValue(param));
                        return true;
                    }
                } else {
                    if(c.getValue(param).equals(carrier.getValue(ent.toTableName(), i, param))) {
                        c.addError("param", "Duplicate " + c.getValue(param));
                        return true;
                    }
                }
                
            }
        }
        
        return false;
    }
    
    public static boolean checkParamsByOriginalValue(Transporter transporter, String tableName, String columnName, String paramValue) throws QException, Exception {
       
        int count = transporter.getTableRowCount(tableName);
        if(count > 0) {
            for(int i = 0; i < count; i++) {
                    if(paramValue.equals(transporter.getValue(tableName, i, columnName))) {
                        
                        return true;
                    }
                }
        }
        
        return false;
    }
    
    
    public static boolean checkDuplicateRowCount(Transporter carrier, LocalEntity ent, boolean withoutThis) throws QException, Exception {
        String id = "";
//        if(withoutThis) {
//            id = carrier.getValue("id").toString();
//            carrier.removeKey("id");
//        }
        
        ent.setAndStatementFildList(carrier);
        
        mapCarrierToEntityForAndStatement(carrier, ent);
        
        ArrayList valueList = new ArrayList();
        valueList.addAll(SQLGenerator.getValuesOfAllGetMethodsOfEntityForAndStatement(ent));
        
        int count = getRowCount(ent, valueList, withoutThis);
        
        if(count > 0) {
            return true;
        }
            
        
        return false;
    }
    
    public static boolean checkUserOrgPrivilege(LocalEntity ent, String id) throws QException, Exception {
       
        Transporter carrier = new Transporter();
       
        carrier.setValue("id", id);
        String formula = getColumnValueByColumnName(ent, "formula", carrier);
        String orgId = SessionManager.getCurrentUserOrg();
        
        return checkOrgIdInFormula(formula, orgId);
    }
    
//    public static boolean checkUserOrgPrivilege(String orgId) throws QException, Exception {
//       
//        Transporter carrier = new Transporter();
//        EntityV_BasicOrganizations ent = new EntityV_BasicOrganizations();
//        carrier.setValue("id", orgId);
//        String formula = getColumnValueByColumnName(ent, "formula", carrier);
//        String userOrgId = SessionManager.getCurrentUserOrg();
//        
//        return checkOrgIdInFormula(formula, userOrgId);
//    }
    
    public static String getColumnValueByColumnName(LocalEntity ent, String columnName, Transporter c) throws QException, Exception {
       
        ent.setAndStatementFildList(c);
        
        mapCarrierToEntityForAndStatement(c, ent);
        c = EntityManager.selectSpec(ent);
        String tableName = ent.toTableName();
        String value = c.getValue(tableName, 0, columnName).toString();
        
        return value;
    }
    
    public static Transporter getEntValuesByCarrier(LocalEntity ent, Transporter c) throws QException, Exception {
       
        ent.setAndStatementFildList(c);
        
        mapCarrierToEntityForAndStatement(c, ent);
        c = EntityManager.selectSpec(ent);
        return c;
    }
    public static int getRowCount(LocalEntity ent, Transporter c) throws QException, Exception {// validation da istifade ucun
       
        ent.setAndStatementFildList(c);
        
        mapCarrierToEntityForAndStatement(c, ent);
        c = EntityManager.selectSpec(ent);
        int count = c.getTableRowCount(ent.toTableName());
        return count;
    }
    
    public static boolean checkOrgIdInFormula(String formula, String orgId) throws QException, Exception {
       Transporter carrier = new Transporter();
       
       
       String[] splitFormula = formula.split("\\*");
       
        int count = 0;
        
        for(String s: splitFormula) {
            if(s.equals(orgId)) {
                ++count;
                break;
            }
        }
        
        if(splitFormula.length == 0 || count == 0) {
            return true;
        }
            
        
        return false;
    }
    
    public static boolean checkAfterIdInFormula(String parentFormula, String userRoleId, String parentRoleId) throws QException, Exception {
       Transporter carrier = new Transporter();
       
       
       String[] splitFormula = parentFormula.split("\\*");
       
        int count1 = 0;
        int count2 = 0;
        
        for(String s: splitFormula) {
            if(s.equals(userRoleId)) {
                ++count1;
            }
            
            if(count1 > 0 && s.equals(parentRoleId)) {
                ++count2;
                break;
            }
            
        }
        
        if(splitFormula.length == 0 || count2 == 0) {
            return true;
        }
            
        
        return false;
    }
    public static boolean checkFormulaSubParams(String parentFormula, String id) throws QException, Exception {
       Transporter carrier = new Transporter();
       
       
       String[] splitFormula = parentFormula.split("\\*");
       
        int count1 = 0;
        int count2 = 0;
        
        for(String s: splitFormula) {
            if(s.equals(id)) {
                ++count1;
            }
            
            if(count1 > 0) {
                ++count2;
                break;
            }
            
        }
        
        if(count2 > 0) {
            return true;
        }
            
        
        return false;
    }

    private static Date getNextIntervalDate(Date date, String intervalSeperator, String intervalEndDate) {
        Date newDate = new Date();
        switch (intervalSeperator) {
            case "1":
                newDate = QDate.addDay(date, 1);
                break;
            case "2":
                newDate = QDate.addDay(date, 7);
                break;
            case "3":
                newDate = QDate.addMonth(date, 1);
                break;
            case "4":
                newDate = QDate.addMonth(date, 3);
                break;
            case "5":
                newDate = QDate.addYear(date, 1);
                break;
            default:
                break;
        }
        if (newDate.after(QDate.convertStringToDate(intervalEndDate))) {
            newDate = QDate.convertStringToDate(intervalEndDate);
        }
        return newDate;
    }
    
    public static void doInsert(LocalEntity ent) throws QException, Exception {
        Transporter c = SessionManager.sessionTransporter();
        
        ent.setInsertList(c, ent);
        mapCarrierToEntityForInsert(c, ent);
        insert(ent);
        c.setValue("id", ent.getId());
    }
    
    public static void doInsertSpec(LocalEntity ent, Transporter c) throws QException, Exception {
        ent.setInsertList(c, ent);
        mapCarrierToEntityForInsert(c, ent);
        insert(ent);
        c.setValue("id", ent.getId());
    }
    
    
    
    public static void doUpdate(LocalEntity ent) throws QException, Exception {
        Transporter c = SessionManager.sessionTransporter();
        String id = c.getValue("id").toString();
        ent.setId(id);
        ent.setUpdateList(c, ent); //update olunacaq fieldlerin siyahisini duzeldir
        mapCarrierToEntityForUpdate(c, ent);
        update(ent);
    }
    
    public static void doUpdateByOtherParams(LocalEntity ent, Transporter carrier, List<String> params) throws QException, Exception {
        Transporter c = new Transporter();
        for(String s: params) {
            c.setValue(s, carrier.getValue(s));
            carrier.removeKey(s);
        }
        
        ent.setUpdateList(carrier, ent); //update olunacaq fieldlerin siyahisini duzeldir
        
        mapCarrierToEntityForUpdate(carrier, ent);
        
        updateByOtherParams(ent, c, params);
    }
    
    public static void doDeleteByOtherParams(LocalEntity ent, Transporter carrier, List<String> params) throws QException, Exception {
        Transporter c = new Transporter();
        for(String s: params) {
            c.setValue(s, carrier.getValue(s));
            carrier.removeKey(s);
        }
        carrier.setValue("active", "0");
        ent.setUpdateList(carrier, ent); //update olunacaq fieldlerin siyahisini duzeldir
        mapCarrierToEntityForUpdate(carrier, ent);
        
        updateByOtherParams(ent, c, params);
    }
    
    public static void doUpdateSpec(LocalEntity ent, Transporter c) throws QException, Exception {
        String id = c.getValue("id").toString();
        ent.setId(id);
        ent.setUpdateList(c, ent); //update olunacaq fieldlerin siyahisini duzeldir
        mapCarrierToEntityForUpdate(c, ent);
        update(ent);
    }
    
    public static void doDelete(LocalEntity ent) throws QException, Exception {
        Transporter c = SessionManager.sessionTransporter();
        mapCarrierToEntity(c, ent);
        delete(ent);
    }
    
    public static void doDeleteSpec(LocalEntity ent, Transporter c) throws QException, Exception {
        mapCarrierToEntity(c, ent);
        delete(ent);
    }
    
    public static void doDeleteWithoutActive0(LocalEntity ent) throws QException, Exception {
        Transporter c = SessionManager.sessionTransporter();
        
        mapCarrierToEntity(c, ent);
        deleteWihtoutActive0(ent);
    }
    
    public static Transporter doSelect(LocalEntity ent) throws QException, Exception {
        Transporter c = SessionManager.sessionTransporter();
            ent.setAndStatementFildList(c);
            mapCarrierToEntityForAndStatement(c, ent);
            c = EntityManager.select(ent);
            selectTableSequenceAndHidden(ent, c);
            SessionManager.setSessionTransporter(c);
            
            return c;
    }
    
    public static Transporter doSelectByFullLikeParams(LocalEntity ent, List<String> params) throws QException, Exception {
        Transporter c = SessionManager.sessionTransporter();
            ent.setAndStatementFildList(c);
            Transporter fullLikeCarrier = new Transporter();
            
            for(String s: params) {
               fullLikeCarrier.setValue(s, c.getValue(s));
            }
            ent.setAndStatementFildListForFullLike(fullLikeCarrier);
            mapCarrierToEntityForAndStatement(c, ent);
            c = EntityManager.select(ent);
            selectTableSequenceAndHidden(ent, c);
            SessionManager.setSessionTransporter(c);
            
            return c;
    }
    
    public static Transporter doSelectSpecByFullLikeParams(LocalEntity ent, Transporter c, List<String> params) throws QException, Exception {
            ent.setAndStatementFildList(c);
            Transporter fullLikeCarrier = new Transporter();
            
            for(String s: params) {
               fullLikeCarrier.setValue(s, c.getValue(s));
            }
            
            ent.setAndStatementFildListForFullLike(fullLikeCarrier);
            mapCarrierToEntityForAndStatement(c, ent);
            c = EntityManager.select(ent);
            
            return c;
    }
    
    public static Transporter doSelectSpec(Transporter carrier, LocalEntity ent) throws QException, Exception {
        Transporter c = new Transporter();
            ent.setAndStatementFildList(carrier);
            mapCarrierToEntityForAndStatement(carrier, ent);
            c = EntityManager.selectSpec(ent);
            return c;
    }
    
    public static Transporter createLikeCarrier(String key, String value) throws QException, Exception {
        Transporter c = new Transporter();
        c.setValue(key, value);
            
        return c;
    }
    
    public static Transporter addTBLToSessionCarrier(Transporter carrier, LocalEntity ent, String tblName) throws QException, Exception {
        Transporter c = SessionManager.sessionTransporter();
        int rowCount = carrier.getTableRowCount(ent.toTableName());
        String[] columnNames = carrier.getTableColumnNames(ent.toTableName());
        for(int i = 0; i < rowCount; i++) {
            for(String s: columnNames) {
                String value = carrier.getValue(ent.toTableName(), i, s).toString();
                c.setValue(tblName.toLowerCase(), i, s, value);
            }
        }
        selectTableSequenceAndHiddenSpec(ent, tblName.toLowerCase());
        return c;
    }
    
    public static String getOrgFormulaByParamOrCurrentUser(LocalEntity ent) throws QException, Exception {
        Transporter c = SessionManager.sessionTransporter();
        String orgId = c.getValue("orgId").toString();
        String orgFormula = SessionManager.getCurrentUserOrgFormula();
        String userOrgId = SessionManager.getCurrentUserOrg();
        if(orgId != null && !orgId.trim().isEmpty()) {
            Transporter carrier = new Transporter();
            carrier.setValue("id", orgId);
            orgFormula = getColumnValueByColumnName(ent, "formula", carrier);
            if(checkOrgIdInFormula(orgFormula, userOrgId)) {
                c.addError("orgId", "you do not permission org id");
            }
            
        }
        orgFormula = orgFormula.replace("*", "\\*");
        return orgFormula;
    }
    
//    public static String getOrgFormulaByParamOrCurrentUser() throws QException, Exception {
//        Transporter c = SessionManager.sessionTransporter();
//        String orgId = c.getValue("orgId").toString();
//        String orgFormula = SessionManager.getCurrentUserOrgFormula();
//        EntityV_BasicOrganizations ent = new EntityV_BasicOrganizations();
//        if(orgId != null && !orgId.trim().isEmpty()) {
//            Transporter carrier = new Transporter();
//            carrier.setValue("id", orgId);
//            orgFormula = getColumnValueByColumnName(ent, "formula", carrier);
//        }
//        orgFormula = orgFormula.replace("*", "\\*");
//        return orgFormula;
//    }
    
    public static String getEduYearIdByDate(String date, LocalEntity ent) throws QException, Exception {
        Transporter eduYearCarrier = new Transporter();
        eduYearCarrier = EntityManager.getEntValuesByCarrier(ent, eduYearCarrier);
        int rowCount = eduYearCarrier.getTableRowCount(ent.toTableName());
        int mainDate = Integer.parseInt(date.split("/")[2]);
        String id = "";
        for(int i = 0; i < rowCount; i++) {
            int startDate = Integer.parseInt(eduYearCarrier.getValue(ent.toTableName(), i, "startDate").toString().split("/")[2].trim());
            int endDate = Integer.parseInt(eduYearCarrier.getValue(ent.toTableName(), i, "endDate").toString().split("/")[2].trim());
            if(mainDate >= startDate && mainDate < endDate) {
                id = eduYearCarrier.getValue(ent.toTableName(), i, "id").toString();
                break;
            }
        }
        return id;
    }
    
    public static void createLog(String code, String tablename, String forAdmin, String messageAz, String messageEn, String messageRu, String operationUrl, String userId) throws QException, Exception {
        Transporter eduYearCarrier = new Transporter();
        SQLConnection.createActionLog(code, messageAz, messageEn, messageRu, operationUrl, forAdmin, userId, tablename);
    }
    
    public static Transporter getLogs(LocalEntity ent, String id) throws QException, Exception {
        
        Transporter transporter = new Transporter();
        transporter.setValue("id", id);
        transporter =  getEntValuesByCarrier(ent, transporter);
        transporter.setValue("tableName", ent.toTableName());
        transporter.setValue("viewname", getTableNameBasedOnEntityForView(ent));
        return transporter;
    }
    
    public static void createLogForUpdate(Transporter before, LocalEntity ent, String id, String operationUrl, String userId) throws QException, Exception {
        
         Transporter after = new Transporter();
         after.setValue("id", id);
         after =  getEntValuesByCarrier(ent, after);
        
         String messageAz = before.getValue(before.getValue("tableName").toString(), 0, "messageBeforeUpdateAz").toString() + " --> " +
                            before.getValue(before.getValue("tableName").toString(), 0, "messageAz") + " --- " +
                            after.getValue(ent.toTableName(), 0, "messageAfterUpdateAz") + " --> " +
                            after.getValue(ent.toTableName(), 0, "messageAz");
         String messageEn = before.getValue(before.getValue("tableName").toString(), 0, "messageBeforeUpdateEn").toString() + " --> " +
                            before.getValue(before.getValue("tableName").toString(), 0, "messageEn") + " --- " +
                            after.getValue(ent.toTableName(), 0, "messageAfterUpdateEn") + " --> " +
                            after.getValue(ent.toTableName(), 0, "messageEn");
         String messageRu = before.getValue(before.getValue("tableName").toString(), 0, "messageBeforeUpdateRu").toString() + " --> " +
                            before.getValue(before.getValue("tableName").toString(), 0, "messageRu") + " --- " +
                            after.getValue(ent.toTableName(), 0, "messageAfterUpdateRu") + " --> " +
                            after.getValue(ent.toTableName(), 0, "messageRu");
         
         String forAdmin = before.getValue(before.getValue("tableName").toString(), 0, "messageBeforeUpdateAz").toString() + " --> " +
                            before.getValue(before.getValue("tableName").toString(), 0, "forAdmin") + " --- " +
                            after.getValue(ent.toTableName(), 0, "messageAfterUpdateRu") + " --> " +
                            after.getValue(ent.toTableName(), 0, "forAdmin");
        SQLConnection.createActionLog("UPDATE", messageAz, messageEn, messageRu, operationUrl, forAdmin, userId, before.getValue("viewname").toString());
    }
    
    public static void createLogForInsert(LocalEntity ent, String id, String operationUrl, String userId) throws QException, Exception {
            Transporter t = new Transporter();
            t.setValue("id", id);
            t =  getEntValuesByCarrier(ent, t);
         String messageAz = t.getValue(ent.toTableName(), 0, "messageInsertAz").toString() + " --> " +
                            t.getValue(ent.toTableName(), 0, "messageAz");
         String messageEn = t.getValue(ent.toTableName(), 0, "messageInsertEn").toString() + " --> " +
                            t.getValue(ent.toTableName(), 0, "messageEn");
         String messageRu = t.getValue(ent.toTableName(), 0, "messageInsertRu").toString() + " --> " +
                            t.getValue(ent.toTableName(), 0, "messageRu");
         
         String forAdmin = t.getValue(ent.toTableName(), 0, "messageInsertAz").toString() + " --> " +
                            t.getValue(ent.toTableName(), 0, "forAdmin");
        SQLConnection.createActionLog("INSERT", messageAz, messageEn, messageRu, operationUrl, forAdmin, userId, getTableNameBasedOnEntityForView(ent));
    }
    public static void createLogForDelete(LocalEntity ent, String id, String operationUrl, String userId) throws QException, Exception {
            Transporter t = new Transporter();
            t.setValue("id", id);
            t =  getEntValuesByCarrier(ent, t);
         String messageAz = t.getValue(ent.toTableName(), 0, "messageDeleteAz").toString() + " --> " +
                            t.getValue(ent.toTableName(), 0, "messageAz");
         String messageEn = t.getValue(ent.toTableName(), 0, "messageDeleteEn").toString() + " --> " +
                            t.getValue(ent.toTableName(), 0, "messageEn");
         String messageRu = t.getValue(ent.toTableName(), 0, "messageDeleteRu").toString() + " --> " +
                            t.getValue(ent.toTableName(), 0, "messageRu");
         
         String forAdmin = t.getValue(ent.toTableName(), 0, "messageDeleteAz").toString() + " --> " +
                            t.getValue(ent.toTableName(), 0, "forAdmin");
        SQLConnection.createActionLog("DELETE", messageAz, messageEn, messageRu, operationUrl, forAdmin, userId, getTableNameBasedOnEntityForView(ent));
    }
    public static void createLogForView(LocalEntity ent, String operationUrl, String userId) throws QException, Exception {
        
        SQLConnection.createActionLog("ALL_VIEW", "", "", "", operationUrl, "", userId, getTableNameBasedOnEntityForView(ent));
    }
}
