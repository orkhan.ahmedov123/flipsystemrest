/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.utils;

import com.flip.Exception.QException;
import com.flip.domain.SessionManager;
import com.flip.domain.Transporter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author otahmadov
 */
public class SQLConnection {

    private static final Logger log = Logger.getLogger(SQLConnection.class);
    
    // Duzeldilmis uygun query e gore insert gedir
    public static void execInsertSql(String arg, ArrayList valueList) throws QException, SQLException {
//        Connection conn = SessionManager.getCurrentConnection();
        try {
            try (PreparedStatement stmt = SessionManager.getCurrentConnection().prepareStatement(arg)) {
                String line = "";
                for (int i = 0; i < valueList.size(); i++) {
                    if(valueList.get(i).toString().trim().length() > 0)
                            stmt.setObject(i + 1, valueList.get(i), java.sql.Types.OTHER);
                        else
                            stmt.setNull(i + 1, Types.NULL);
                    line = line + i + "->" + valueList.get(i) + ";";
                }
                log.info(arg + " --> " + line);
                stmt.executeUpdate();
                
//                String tableName = arg.substring(SQLGenerator.INSERT_INTO.length()).trim().split(" ")[0];
//                execInsertLogSql(SessionManager.getCurrentUserId(), "INSERT", arg + " --> " + line, tableName);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
//    private static void execInsertLogSql(String userId, String code, String query, String tableName) throws QException, SQLException {
////        Connection conn = SessionManager.getCurrentConnection();
//        try {
//            String insertQuery = "insert into user_action_log (id, user_id, code, query, tbl, create_date) " +
//"                                  values (?,?,?,?,?, now()) ";
//            try (PreparedStatement stmt = SessionManager.getCurrentConnection().prepareStatement(insertQuery)) {
//                stmt.setObject(1, IdGenerator.getId(), java.sql.Types.OTHER);
//                stmt.setObject(2, userId, java.sql.Types.OTHER);
//                stmt.setObject(3, code, java.sql.Types.OTHER);
//                stmt.setObject(4, query, java.sql.Types.OTHER);
//                stmt.setObject(5, tableName, java.sql.Types.OTHER);
//                stmt.executeUpdate();
//            }
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        }
//    }
    
    public static void createActionLog(String code, String az, String en, String ru, String url, String forAdmin, String userId, String tableName) throws QException, SQLException {
//        Connection conn = SessionManager.getCurrentConnection();
        try {
            String insertQuery = "insert into common_action_log (id, user_id, message_az, message_en, message_ru, operation_url, for_admin, tablename, code, create_date) " +
"                                  values (?,?,?,?,?,?,?,?,?, now()) ";
            try (PreparedStatement stmt = SessionManager.getCurrentConnection().prepareStatement(insertQuery)) {
                stmt.setObject(1, IdGenerator.getId(), java.sql.Types.OTHER);
                stmt.setObject(2, userId, java.sql.Types.OTHER);
                stmt.setObject(3, az, java.sql.Types.OTHER);
                stmt.setObject(4, en, java.sql.Types.OTHER);
                stmt.setObject(5, ru, java.sql.Types.OTHER);
                stmt.setObject(6, url, java.sql.Types.OTHER);
                stmt.setObject(7, forAdmin, java.sql.Types.OTHER);
                stmt.setObject(8, tableName, java.sql.Types.OTHER);
                stmt.setObject(9, code, java.sql.Types.OTHER);
                stmt.executeUpdate();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private static void addValueToStatement(PreparedStatement stmt, int ind, String value, ArrayList array) {
        try {
            stmt.setObject(ind, value, java.sql.Types.OTHER);
            array.add(value);
        } catch (SQLException ex) {
            new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }

    public static Transporter execSelectSql(String arg, String tableName, ArrayList values) throws QException {
        Transporter qc1 = new Transporter();
        ArrayList logLine = new ArrayList();
        try {
//            Connection conn = SessionManager.getCurrentConnection();
            try(PreparedStatement stmt = SessionManager.getCurrentConnection().prepareStatement(arg)){

                int idx = 1;
                String line = "";
                String query = arg;
                for (int i = 0; i < values.size(); i++) {
                    if (!values.get(i).toString().trim().equals("")) {
                        String val = values.get(i).toString().trim();
                        stmt.setObject(idx, val, java.sql.Types.OTHER);
                        
                        line = line + i + "->" + values.get(i) + ";";
                        ++idx;
                    }
                }
                log.info(query + " --> " + line);
                try (ResultSet rs = stmt.executeQuery()) {
                    qc1 = convertResultSetToCarrier(rs, tableName);
                }
            }
        } catch (SQLException | QException e) {
            log.error(e.getMessage(), e);
        }
        return qc1;
    }
    
    public static Transporter execSelectSqlSpec(String arg, String tableName, ArrayList values) throws QException {
        Transporter carrier = new Transporter();
        ArrayList logLine = new ArrayList();
        try {
//            Connection conn = SessionManager.getCurrentConnection();
            try(PreparedStatement stmt = SessionManager.getCurrentConnection().prepareStatement(arg)){
                int idx = 1;
                String line = "";
                String query = arg;
                for (int i = 0; i < values.size(); i++) {
                    if (!values.get(i).toString().trim().equals("")) {
                        String val = values.get(i).toString().trim();
                        stmt.setObject(idx, val, java.sql.Types.OTHER);
                        
                        line = line + i + "->" + values.get(i) + ";";
                        ++idx;
                    }
                }
                log.info(query + " --> " + line);
                try (ResultSet rs = stmt.executeQuery()) {
                  carrier = convertResultSetToCarrierSpec(rs, tableName);
                }
            }
        } catch (SQLException | QException e) {
            log.error(e.getMessage(), e);
        }
        
        return carrier;
    }
    
    public static String specSelectSql(String arg) throws QException {
        String result="";
        try {
//            Connection conn = SessionManager.getCurrentConnection();
            try(PreparedStatement stmt = SessionManager.getCurrentConnection().prepareStatement(arg)){

                try (ResultSet rs = stmt.executeQuery()) {
                    if(rs.next()) return rs.getString(1);
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

//    public static Transporter execSelectSql(String arg, String tableName, ArrayList values, Connection conn1) throws QException {
//        Transporter qc1 = new Transporter();
//        ArrayList logLine = new ArrayList();
//        try {
////            Connection conn = SessionManager.getCurrentConnection();
//            
//            try(PreparedStatement stmt = SessionManager.getCurrentConnection().prepareStatement(arg)){
//                int idx = 1;
//                String line = "";
//                String query = arg;
//                for (int i = 0; i < values.size(); i++) {
//                    if (!values.get(i).toString().trim().equals("")) {
//                        String val = values.get(i).toString().trim();
//                        addValueToStatement(stmt, idx++, val, logLine);
//                    }
//                }
//                log.info(query + " --> " + line);
//                try (ResultSet rs = stmt.executeQuery()) {
//                    qc1 = convertResultSetToCarrier(rs, tableName);
//                }
//                stmt.close();
//            }
//        } catch (SQLException | QException e) {
//           log.error(e.getMessage(), e);
//        }
//        return qc1;
//    }

    public static void execUpdateSql(String arg, ArrayList valueList) throws QException, SQLException {
//        Connection conn = SessionManager.getCurrentConnection();
        try {
            try(PreparedStatement stmt = SessionManager.getCurrentConnection().prepareStatement(arg)){
                String line = "";
                for (int i = 0; i < valueList.size(); i++) {
                    String value = valueList.get(i).toString().trim();
                        if(value.length() > 0)
                            stmt.setObject(i + 1, value, java.sql.Types.OTHER);
                        else
                            stmt.setNull(i + 1, Types.NULL);
                        line = line + i + "->" + value + ";";
                }
                log.info(arg + " --> " + line);
                stmt.executeUpdate();
                String tableName = arg.substring(SQLGenerator.UPDATE.length()).trim().split(" ")[0];
//                execInsertLogSql(SessionManager.getCurrentUserId(), "UPDATE", arg + " --> " + line, tableName);
            }    
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public static void execDeleteSql(String arg, ArrayList valueList) throws QException, SQLException {
//        Connection conn = SessionManager.getCurrentConnection();
        try {
            try(PreparedStatement stmt = SessionManager.getCurrentConnection().prepareStatement(arg)){
                String line = "";
                for (int i = 0; i < valueList.size(); i++) {
                    String value = valueList.get(i).toString().trim();
                        if(value.length() > 0)
                            stmt.setObject(i + 1, value, java.sql.Types.OTHER);
                        else
                            stmt.setNull(i + 1, Types.NULL);
                        line = line + i + "->" + value + ";";
                }
                log.info(arg + " --> " + line);
                stmt.executeUpdate();
                String tableName = arg.substring(SQLGenerator.UPDATE.length()).trim().split(" ")[0];
//                execInsertLogSql(SessionManager.getCurrentUserId(), "DELETE_ACTIVE_0", arg + " --> " + line, tableName);
            }    
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public static void execDeleteSqlWithoutActive0(String arg, ArrayList valueList) throws QException {
        try {

//            Connection conn = SessionManager.getCurrentConnection();
            try(PreparedStatement stmt = SessionManager.getCurrentConnection().prepareStatement(arg)){
                String line = "";
                for (int i = 0; i < valueList.size(); i++) {
                    String value = valueList.get(i).toString().trim();
                        stmt.setObject(i + 1, value, java.sql.Types.OTHER);
                        line = line + i + "->" + value + ";";
                }
                log.info(arg + " --> " + line);
                stmt.executeUpdate();
                String tableName = arg.substring(SQLGenerator.DELETE.length() + 1 + SQLGenerator.FROM.length()).trim().split(" ")[0];
//                execInsertLogSql(SessionManager.getCurrentUserId(), "DELETE_ROW", arg + " --> " + line, tableName);
            }    
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public static Transporter execDeleteSql(String query, String[] values) throws QException {
        try {
//            Connection conn = SessionManager.getCurrentConnection();
            try(PreparedStatement preparedStatement = SessionManager.getCurrentConnection().prepareStatement(query)){
                for (int i = 0; i < values.length; i++) {
                    preparedStatement.setObject(i + 1, values[i].trim(), java.sql.Types.OTHER);
                }
                preparedStatement.executeUpdate();
            }    
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private static String[] getColumnNames(ResultSet rs) throws QException {
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            String[] columnNames = new String[columnCount];
            for (int i = 1; i < columnCount + 1; i++) {
                columnNames[i - 1] = rsmd.getColumnName(i);
            }
            return columnNames;
        } catch (Exception e) {
            log.error(e.getMessage(), e);

        }
        return null;
    }

    private static String getTableName(ResultSet rs) throws QException {
        String cc = "";
        try {
            rs.next();
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            String[] columnNames = new String[columnCount];
            cc = rsmd.getTableName(1) + rsmd.getTableName(2);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
 
        return "table";
    }

    private static Transporter convertResultSetToCarrier(ResultSet rs, String tableName) throws QException {
        String[] colNames = getColumnNames(rs);
        Transporter qCarry1 = new Transporter();
        String currentLang = SessionManager.getCurrentLang();
        try {   
            int row = 0;
            while (rs.next()) {
                for (int i = 0; i <= colNames.length - 1; i++) {
                    String vl = rs.getString(colNames[i]) == null ? "" : rs.getString(colNames[i]).trim();
                    String fieldName = convertTableFieldNameToEntityfieldName(colNames[i]);
                            
                    qCarry1.setValue(tableName, row, fieldName, vl);
                }
                row++;
            }
        } catch (SQLException | QException e) {
            log.error(e.getMessage(), e);
        }
        return qCarry1;
    }
    private static Transporter convertResultSetToCarrierSpec(ResultSet rs, String tableName) throws QException { // Hansisa melumat icerisinde list ile qaytaririrqsa o zaman istifade edilir
        String[] colNames = getColumnNames(rs);
        Transporter qCarry1 = new Transporter();
        String currentLang = SessionManager.getCurrentLang();
        try {   
            int row = 0;
            while (rs.next()) {
                for (int i = 0; i <= colNames.length - 1; i++) {
                    String vl = rs.getString(colNames[i]) == null ? "" : rs.getString(colNames[i]).trim();
                    String fieldName = convertTableFieldNameToEntityfieldName(colNames[i]);
                    qCarry1.setValue(tableName, row, fieldName, vl);
                }
                row++;
            }
        } catch (SQLException | QException e) {
            log.error(e.getMessage(), e);
        }
        return qCarry1;
    }

    private static Transporter convertResultSetToCarrierBoth(ResultSet rs, String tableName) throws QException {
        String[] colNames = getColumnNames(rs);
        Transporter qCarry1 = new Transporter();
        try {
            int row = 0;
            while (rs.next()) {
                for (int i = 0; i <= colNames.length - 1; i++) {
                    qCarry1.setValue(tableName, row, i, rs.getString(colNames[i]).trim());
                    qCarry1.setValue(tableName, row, convertTableFieldNameToEntityfieldName(colNames[i]), rs.getString(colNames[i]).trim());
                }
                row++;
            }
        } catch (SQLException | QException e) {
            log.error(e.getMessage(), e);
        }
        return qCarry1;
    }

    static String convertTableFieldNameToEntityfieldName(String arg) {
        String UNDERSCORE = "_";
        String st[] = arg.split(UNDERSCORE);
        String res = st[0].toLowerCase();
        for (int i = 1; i <= st.length - 1; i++) {
            res = res + st[i].substring(0, 1).toUpperCase() + st[i].substring(1, st[i].length()).toLowerCase();
        }
        return res;
    }
}
