/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.utils;

import com.flip.Exception.QException;
import com.flip.domain.Transporter;
import com.flip.domain.SessionManager;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author otahmadov
 */
public class SQLGenerator {
    public static String ENTITY = "Entity";
    public static String INSERT_INTO = "INSERT INTO ";
    public static String SEPERATOR_COMMA = ",";
    public static String QUESTION_MARK = "?";
    public static String SEPERATOR_DOT = ".";
    public static String OPEN_BRACKET = "(";
    public static String CLOSE_BRACKET = ")";
    public static String GET = "get";
    public static String UNDERSCORE = "_";
    public static String VALUES = "VALUES";
    public static String QUOTE = "'";
    public static String SELECT = "SELECT";
    public static String FROM = "FROM";
    public static String STAR = "*";
    public static String SPACE = " ";
    public static String WHERE = "WHERE";
    public static String UPDATE = "UPDATE";
    public static String DELETE = "DELETE";
    public static String SET = "SET";
    public static String TWO_DOTS = ":";
    public static String AND = "AND";
    public static String OR = "OR";
    public static String EQUAL = "=";
    public static String GT = "GT%";
    public static String LT = "LT%";
    public static String GE = "GE%";
    public static String LE = "LE%";
    public static String NE = "NE%";
    public static String BN = "%BN%";
    public static String LK = "LK";
    public static String IN = "%IN%";
    public static String FAIZ = "%%";
    public static String COMMAND_GT = ">";
    public static String COMMAND_LT = "<";
    public static String COMMAND_GE = ">=";
    public static String COMMAND_LE = "<=";
    public static String COMMAND_NE = "<>";
    public static String COMMAND_BN = "BETWEEN";
    public static String COMMAND_LK = "LIKE";
    public static String COMMAND_IN = "IN";

    private static final String TABLE_SQL_SCRIPT = "SQL_SCRIPT";
    private static final String FIELD_ID = "ID";
    private static final String FIELD_PART = "PART";
    private static final String FIELD_SQL = "SQL";
    private static final String ORDER_BY = "ORDER BY";
    private static final String PATTERN_FOR_PARAMS = ":param";
    private static final String HAS_UNDERSCORE_IN_TABLE_NAME = "hasUnderscoreInTableName";
    private static final String HAS_UNDERSCORE_IN_FIELD_NAME = "db.has.underscore.field-name.primary";
    

    // insert generasiya edir
//    public static String insertGenerator(LocalEntity core, String[] methodNames) {
//
//        String retQuery = INSERT_INTO + SPACE + getTableNameBasedOnEntity(core) + SPACE + OPEN_BRACKET  
//                + getTableFieldNameFromList(methodNames) + CLOSE_BRACKET
//                + VALUES + SPACE + OPEN_BRACKET
//                + getParamQuestionPartOfInsert(methodNames) + CLOSE_BRACKET;
//
//        return retQuery;
//    }
    
    public static String insertGeneratorNew(LocalEntity core) {
        
        String retQuery = INSERT_INTO + SPACE + getTableNameBasedOnEntity(core) + SPACE + OPEN_BRACKET  
                + getTableFieldNameFromListForInsert(core) + CLOSE_BRACKET
                + VALUES + SPACE + OPEN_BRACKET
                + getParamQuestionPartOfInsertNew(core) + CLOSE_BRACKET;

        return retQuery;
    }

//    private static String getParamQuestionPartOfInsert(String[] methodNames) {
//        String s = "";
//        for (String methodName : methodNames) {
//            s = s + "?,";
//        }
//        s = s.substring(0, s.length() - 1);
//        return s;
//    }

    private static String getParamQuestionPartOfInsertNew(LocalEntity core) {
        String s = "";
        for (String methodName : core.currentInsertList()) {
            s = s + "?,";
        }
        s = s.substring(0, s.length() - 1);
        return s;
    }
    public static String updateGenerator(LocalEntity entity, String[] methodNames, ArrayList valueList) {

        String updateSql = UPDATE + SPACE + getTableNameBasedOnEntity(entity) + SPACE + SET + SPACE
                + generateSetPartOfUpdateSql(entity, methodNames);
        String wPart = generateWherePartOfUpdate(entity, entity.getId().trim(), valueList);
        if (wPart.equals("")) {
            return "";
        }
        updateSql += wPart;
        return updateSql;
    }
    
    public static String updateGeneratorForUpdate(LocalEntity entity, ArrayList valueList) {

        String updateSql = UPDATE + SPACE + getTableNameBasedOnEntity(entity) + SPACE + SET + SPACE  
                + generateSetPartOfUpdateSqlForUpdate(entity); // updateList de olan parametrlere uygun SET hissesini yaradir
        String wPart = generateWherePartOfUpdate(entity, entity.getId().trim(), valueList);
        if (wPart.equals("")) {
            return "";
        }
        updateSql += wPart;
        return updateSql;
    }
    
    public static String updateGeneratorForUpdateByOtherParams(LocalEntity entity, ArrayList valueList, Transporter carrier, List<String> params) {

        String updateSql = UPDATE + SPACE + getTableNameBasedOnEntity(entity) + SPACE + SET + SPACE  
                + generateSetPartOfUpdateSqlForUpdate(entity); // updateList de olan parametrlere uygun SET hissesini yaradir
        String wPart = "";
        try {
           wPart = generateWherePartOfUpdateByOtherParams(carrier, params);
        } catch (Exception e) {
            
        }
        if(!entity.currentSpecSqlCode().trim().isEmpty()) {
            if(!wPart.trim().isEmpty()) 
                wPart += " and " + entity.currentSpecSqlCode();
            else
                wPart = entity.currentSpecSqlCode();
        }
        if (wPart.equals("")) {
            return "";
        }
        updateSql += wPart;
        return updateSql;
    }
    public static String updateGeneratorForDelete(LocalEntity entity, String[] methodNames, ArrayList valueList) {

        String updateSql = UPDATE + SPACE + getTableNameBasedOnEntity(entity) + SPACE + SET + SPACE
                + generateSetPartOfUpdateSqlForDelete(entity, methodNames);
        String wPart = generateWherePartOfUpdate(entity, entity.getId().trim(), valueList);
        if (wPart.equals("")) {
            return "";
        }
        updateSql += wPart;
        return updateSql;
    }
    public static String deleteGeneratorForWithoutActive0(LocalEntity entity) throws QException {

        String deleteSql = DELETE + SPACE + FROM + SPACE + getTableNameBasedOnEntity(entity) + SPACE + WHERE +  SPACE
                            + generateWherePartOfDeleteSql(entity);
        
        return deleteSql;
    }

//    public static String deleteGenerator(LocalEntity coreEnt) throws QException {
//        try {
//            /*fist element of ListOfGetMethods keep the number of GetMethod.Calling part of this method should start number in from 1*/
// /*String methodNames[] =getAllGetMethodNames(core);
//            
//            String retQuery = INSERT_INTO + getTableNameBasedOnEntity(core) + OPEN_BRACKET + getTableFieldNameFromList(methodNames) +CLOSE_BRACKET+
//            VALUES + OPEN_BRACKET +getValuesOfAllGetMethodsOfEntityInOneString(core,methodNames) +CLOSE_BRACKET ;
//             */
//            String deleteSql = DELETE + SPACE + getTableNameBasedOnEntity(coreEnt);
//            String wPart = generateWherePartOfUpdateSql(coreEnt).trim();
//            if (!wPart.equals("")) {
//                deleteSql += SPACE + WHERE + SPACE + wPart;
//            }
//            return deleteSql;
//        } catch (QException ex) {
//            throw new QException(new Object() {
//            }.getClass().getEnclosingClass().getName(),
//                    new Object() {
//            }.getClass().getEnclosingMethod().getName(),
//                    ex);
//        }
//    }

    private static String generateSetPartOfUpdateSql(LocalEntity coreEnt, String[] methodNames) {
        String setPart = SPACE;
//        Carrier c = SessionManager.sessionCarrier();
//        String[] methodNames = getAllGetMethodNames(coreEnt);
        for (String methodName : methodNames) {
//            String m = methodName.substring("GET".length(), methodName.length()).substring(0,1).toLowerCase() + methodName.substring("GET".length(), methodName.length()).substring(1);
//            if(c.hasKey(m)){
                setPart += seperateTableFieldNameWithUnderscore(methodName.substring("GET".length(), methodName.length())).toUpperCase() + EQUAL + "?" + SPACE + SEPERATOR_COMMA + SPACE;
//            }
//            String vl = executeMethod(coreEnt, methodNames[i]).toString();
//			if (!vl.trim().isEmpty()){
//			}
        }
        return setPart.substring(1, setPart.length() - SEPERATOR_COMMA.length() - 1);
    }
    
    private static String generateSetPartOfUpdateSqlForUpdate(LocalEntity coreEnt) {
        String setPart = SPACE;
        for (String methodName : coreEnt.currentUpdateList()) {
                methodName = methodName.substring(0,1).toUpperCase() + methodName.substring(1);
                setPart += seperateTableFieldNameWithUnderscore(methodName).toUpperCase() + EQUAL + "?" + SPACE + SEPERATOR_COMMA + SPACE;
        }
        return setPart.substring(1, setPart.length() - SEPERATOR_COMMA.length() - 1);
    }
    
    private static String generateSetPartOfWhereSqlForAndStatement(LocalEntity coreEnt) {
        String setPart = SPACE;
        int setPartLength = 0;
        for (String methodName : coreEnt.currentAndStatementFildListForEqual()) {
                methodName = methodName.substring(0,1).toUpperCase() + methodName.substring(1);
                if(seperateTableFieldNameWithUnderscore(methodName).toUpperCase().equals("ID"))
                    setPart += seperateTableFieldNameWithUnderscore(methodName).toUpperCase() + EQUAL + "?" + SPACE + AND + SPACE;
                else {
                    if(methodName.toUpperCase().equals("ORGFORMULA") || methodName.toUpperCase().equals("FORMULA")) {
                        setPart += seperateTableFieldNameWithUnderscore(methodName).toUpperCase()+" ~ concat('^',upper(?))"  + SPACE + AND + SPACE;
                    } else {
                        setPart += "UPPER(" + seperateTableFieldNameWithUnderscore(methodName).toUpperCase()+") ~ concat('^',upper(?))"  + SPACE + AND + SPACE;
                    }
                }
                    
//                    setPart += "REGEXP_LIKE(upper("+seperateTableFieldNameWithUnderscore(methodName).toUpperCase()+"), '^'||upper(?))"  + SPACE + AND + SPACE;
        }
        for (String methodName : coreEnt.currentAndStatementFildListForNotEqual()) {
                methodName = methodName.substring(0,1).toUpperCase() + methodName.substring(1);
                setPart += seperateTableFieldNameWithUnderscore(methodName).toUpperCase() + SPACE + COMMAND_NE + "?" + SPACE + AND + SPACE;
        }
        for (String methodName : coreEnt.currentAndStatementFildListForFullLike()) {
                methodName = methodName.substring(0,1).toUpperCase() + methodName.substring(1);
                setPart += "lower(" + seperateTableFieldNameWithUnderscore(methodName).toUpperCase()+ ") " +SPACE + COMMAND_LK + SPACE + "concat('%',lower(?),'%')" + SPACE + AND + SPACE;
//                setPart += "lower(" + seperateTableFieldNameWithUnderscore(methodName).toUpperCase()+ ") " +SPACE + COMMAND_LK + SPACE + "'%'||lower(?)||'%'" + SPACE + AND + SPACE;
        }
        
        if(coreEnt.currentAndStatementFildForIn().size() > 0) {
            
            for(Object k: coreEnt.currentAndStatementFildForIn().keySet()) {
                String key = k.toString();
                String value = coreEnt.currentAndStatementFildForIn().get(k).toString();
                
                setPart += " " + key + " in (";
                for(String s: value.split(",")) {
                    setPart +="?,";
                }
                setPart = setPart.substring(0, setPart.length() - 1);
                setPart += " )"+ SPACE + AND + SPACE;
            }
        }
        setPartLength = setPart.trim().length();
        if(setPartLength > 0)
            setPart = setPart.substring(1, setPart.length() - AND.length() - 1);
        
        if(coreEnt.currentAndOrStatementFildListFullLike().size() > 0) {
            if(setPartLength > 0) 
                setPart += " and ( ";
            else
                setPart += " ( ";
            for (String methodName : coreEnt.currentAndOrStatementFildListFullLike()) {
                    methodName = methodName.substring(0,1).toUpperCase() + methodName.substring(1);
                    setPart += "lower(" + seperateTableFieldNameWithUnderscore(methodName).toUpperCase()+ ") " +SPACE + COMMAND_LK + SPACE + "concat('%',lower(?),'%')" + SPACE + OR + SPACE;
            }
            
            setPart = setPart.substring(0, setPart.length() - OR.length() - 1) + " )";
        }
        
        if(!coreEnt.currentSpecSqlCode().trim().isEmpty()) {
            if(setPartLength > 0) 
                setPart += " and " + coreEnt.currentSpecSqlCode();
            else
                setPart += coreEnt.currentSpecSqlCode();
        }
        
        
//        bu hissede diger sertler yazilacaq
        
//        return setPart.substring(1, setPart.length() - AND.length() - 1);
        return setPart;
    }

    private static String generateSetPartOfUpdateSqlForDelete(LocalEntity coreEnt, String[] methodNames) {
        String setPart = SPACE;
//        Carrier c = SessionManager.sessionCarrier();
//        String[] methodNames = getAllGetMethodNames(coreEnt);
        for (String methodName : methodNames) {
//            String m = methodName.substring("GET".length(), methodName.length()).substring(0,1).toLowerCase() + methodName.substring("GET".length(), methodName.length()).substring(1);
//            if(c.hasKey(m)){
                setPart += seperateTableFieldNameWithUnderscore(methodName.substring("GET".length(), methodName.length())).toUpperCase() + EQUAL + "?" + SPACE + SEPERATOR_COMMA + SPACE;
//            }
//            String vl = executeMethod(coreEnt, methodNames[i]).toString();
//			if (!vl.trim().isEmpty()){
//			}
        }
        return setPart.substring(1, setPart.length() - SEPERATOR_COMMA.length() - 1);
    }

    public static String generateSelectFieldPartOfSelectSql(LocalEntity coreEnt, boolean addEntityNameToAS) {
        String st = "";
        if (coreEnt.hasDistinctFields()) {
            st = selectFieldNameGeneratorByDistinctFileds(coreEnt, addEntityNameToAS);
//        } else if (coreEnt.hasIncludedField()) {
//            st = selectFieldNameGeneratorByIncludedFileds(coreEnt, addEntityNameToAS);
        } else {
            st = selectFieldNameGeneratorByMethodnames(coreEnt, addEntityNameToAS);
        }
        return st;
    }

    public static String selectFieldNameGeneratorByDistinctFileds(LocalEntity coreEnt, boolean addEntityNameToAS) {
        String tablename = coreEnt.toDBTableName();
        String st = SPACE + " DISTINCT ";
        String[] arg = coreEnt.selectDistinctFields();
        for (String arg1 : arg) {
            String fname = seperateTableFieldNameWithUnderscore(arg1).toUpperCase();
            if (addEntityNameToAS) {
                st += tablename + "." + fname;
                st += " AS " + tablename.trim() + UNDERSCORE + fname.trim();
            } else {
                st += fname;
                // st += " AS " + tablename + UNDERSCORE + fname;
            }
            st += SPACE + SEPERATOR_COMMA + SPACE;
        }
        return st.substring(1, st.length() - SEPERATOR_COMMA.length() - 1);
    }

    public static String selectFieldNameGeneratorByIncludedFileds(LocalEntity coreEnt, boolean addEntityNameToAS) {
        String tablename = coreEnt.toDBTableName();
        String st = SPACE;
        String[] arg = coreEnt.selectIncludedFields();
        for (String arg1 : arg) {
            String fname = seperateTableFieldNameWithUnderscore(arg1).toUpperCase();
            if (addEntityNameToAS) {
                st += tablename + "." + fname;
                st += " AS " + tablename + UNDERSCORE + fname;
            } else {
                st += fname;
                // st += " AS " + tablename + UNDERSCORE + fname;
            }
            st += SPACE + SEPERATOR_COMMA + SPACE;
        }
        return st.substring(1, st.length() - SEPERATOR_COMMA.length() - 1);
    }

    private static String selectFieldNameGeneratorByMethodnames(LocalEntity coreEnt, boolean addEntityNameToAS) {
        String tablename = coreEnt.toDBTableName();
        String st = SPACE;
        String lang = SessionManager.getCurrentLang();
        String returnColumn = coreEnt.selectReturnColumn();
        String[] methodNames = getAllGetMethodNames(coreEnt);
        for (String methodName : methodNames) {
            String fname = methodName.substring("GET".length(), methodName.length());
            fname = lowerFirstLetter(fname).trim();
            
            String fname1 = seperateTableFieldNameWithUnderscore(fname).trim().toUpperCase();
            if (addEntityNameToAS) {
                st += tablename + "." + fname1;
                st += " AS " + tablename + UNDERSCORE + fname1;
            } else {
                
                boolean continueStatus = true;
                if(returnColumn.trim().isEmpty() || Arrays.asList(returnColumn.split(",")).contains(fname)) {
                    st += fname1;
                    continueStatus = false;
                }
                
                if(fname1.length() > 3 && fname1.substring(fname1.length() - 3).equals("_" + lang.toUpperCase())){
                    if(returnColumn.trim().isEmpty() || Arrays.asList(returnColumn.split(",")).contains(fname.substring(0, fname.length() - 2))) {
                       st += (st.trim().isEmpty() ? "" : rtrim(st).substring(rtrim(st).length() - 1).equals(",") ?
                               SPACE : SPACE + SEPERATOR_COMMA + SPACE)
                              + fname1 + " AS " + fname1.substring(0, fname1.length() - 3);
                       continueStatus = false;
                    }
                    
                }
                if(continueStatus) continue;
                // st += " AS " + tablename + UNDERSCORE + fname1;
            }
            st += SPACE + SEPERATOR_COMMA + SPACE;
//            }
//			}
        }
        return st.substring(1, st.length() - SEPERATOR_COMMA.length() - 1);
    }

    
    // Objectin icerisindeki fildlerin deyerlerinin listini qaytarir
    static String[] getValuesOfAllGetMethodsOfEntity(LocalEntity core, String methodNames[]) throws Exception {
            String[] rs = new String[methodNames.length];

            /*first element of arg[] must contain a number of getMethods*/
            for (int i = 0; i < methodNames.length; i++) {
                Method method = core.getClass().getMethod(methodNames[i]);
                Object retObj = method.invoke(core);
                rs[i] = retObj.toString();
            }
            return rs;
    }

    static List<String> getValuesOfAllGetMethodsOfEntityForUpdate(LocalEntity core) throws Exception {
            List<String> valueList = new ArrayList<>();
            List<String> methodNames = core.currentUpdateList();
            /*first element of arg[] must contain a number of getMethods*/
            for (String s: methodNames) {
                String methodName = "get" + s.substring(0,1).toUpperCase() + s.substring(1);
                Method method = core.getClass().getMethod(methodName);
                Object retObj = method.invoke(core);
                valueList.add(retObj.toString());
            }
            return valueList;
    }
    public static List<String> getValuesOfAllGetMethodsOfEntityForAndStatement(LocalEntity core) throws Exception {
            List<String> valueList = new ArrayList<>();
            
            for (String s: core.currentAndStatementFildListForEqual()) {
                String methodName = "get" + s.substring(0,1).toUpperCase() + s.substring(1);
                Method method = core.getClass().getMethod(methodName);
                Object retObj = method.invoke(core);
                valueList.add(retObj.toString());
            }
            for (String s: core.currentAndStatementFildListForNotEqual()) {
                String methodName = "get" + s.substring(0,1).toUpperCase() + s.substring(1);
                Method method = core.getClass().getMethod(methodName);
                Object retObj = method.invoke(core);
                valueList.add(retObj.toString());
            }
            for (String s: core.currentAndStatementFildListForLike()) {
                String methodName = "get" + s.substring(0,1).toUpperCase() + s.substring(1);
                Method method = core.getClass().getMethod(methodName);
                Object retObj = method.invoke(core);
                valueList.add(retObj.toString());
            }
            for (String s: core.currentAndStatementFildListForLeftLike()) {
                String methodName = "get" + s.substring(0,1).toUpperCase() + s.substring(1);
                Method method = core.getClass().getMethod(methodName);
                Object retObj = method.invoke(core);
                valueList.add(retObj.toString());
            }
            for (String s: core.currentAndStatementFildListForFullLike()) {
                String methodName = "get" + s.substring(0,1).toUpperCase() + s.substring(1);
                Method method = core.getClass().getMethod(methodName);
                Object retObj = method.invoke(core);
                valueList.add(retObj.toString());
            }
            for (String s: core.currentAndStatementFildListForRightLike()) {
                String methodName = "get" + s.substring(0,1).toUpperCase() + s.substring(1);
                Method method = core.getClass().getMethod(methodName);
                Object retObj = method.invoke(core);
                valueList.add(retObj.toString());
            }
            for (String s: core.currentAndOrStatementFildListFullLike()) {
                String methodName = "get" + s.substring(0,1).toUpperCase() + s.substring(1);
                Method method = core.getClass().getMethod(methodName);
                Object retObj = method.invoke(core);
                valueList.add(retObj.toString());
            }
            
            for(Object s: core.currentAndStatementFildForIn().keySet()){
                String key = s.toString();
                String value = core.currentAndStatementFildForIn().get(s).toString();
                for(String ss: value.split(",")) {
                    valueList.add(ss);
                }
            }
            
            return valueList;
    }

    static List<String> getValuesOfAllGetMethodsOfEntityForInsert(LocalEntity core) throws Exception {
            List<String> valueList = new ArrayList<>();
            List<String> methodNames = core.currentInsertList();
            /*first element of arg[] must contain a number of getMethods*/
            for (String s: methodNames) {
                String methodName = "get" + s.substring(0,1).toUpperCase() + s.substring(1);
                Method method = core.getClass().getMethod(methodName);
                Object retObj = method.invoke(core);
                valueList.add(retObj.toString());
            }
            return valueList;
    }

    static List<String> getValuesOfAllGetMethodsOfEntityForDeleteWithoutActive0(LocalEntity core) throws Exception {
            List<String> valueList = new ArrayList<>();
            List<String> methodNames = core.currentDeleteWhereParamsList();
            /*first element of arg[] must contain a number of getMethods*/
            for (String s: methodNames) {
                String methodName = "get" + s.substring(0,1).toUpperCase() + s.substring(1);
                Method method = core.getClass().getMethod(methodName);
                Object retObj = method.invoke(core);
                valueList.add(retObj.toString());
            }
            return valueList;
    }

    static String getValuesOfAllGetMethodsOfEntityInOneStringNEW(LocalEntity core, String arg[]) {

        String rs = "";

        /*first element of arg[] must contain a number of getMethods*/
        for (String arg1 : arg) {
            rs = rs + "?" + SEPERATOR_COMMA;
        }
        rs = rs.substring(0, rs.length() - 1);
        return rs;
    }

    static Object executeMethod(LocalEntity core, String methodName) throws QException {

        try {
            String rs = "";

            /*first element of arg[] must contain a number of getMethods*/
            Method method = core.getClass().getMethod(methodName);
            Object retObj = method.invoke(core);

            return retObj;
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(),
                    ex);
        }
    }

    public static String getTableNameBasedOnEntity(LocalEntity core) {
        String mtSt[] = core.getClass().getName().split("\\.");
        String[] splitClassName = mtSt[mtSt.length - 1].substring(ENTITY.length()).split("(?=[A-Z])"); //Class in adindan Entitini cixarir ve Boyuk herflere gore split edir

        String tableName = "";
        String schemaName = "";
        
        if(SessionManager.isSchema()) {
            schemaName = SessionManager.SCHEMA_PREFIX + "_" + splitClassName[0] + ".";
            for(int i=1; i<splitClassName.length;i++) {// eger schema varsa i=1 den baslamalidir
                tableName += splitClassName[i].toLowerCase() + ((splitClassName.length - 1) == i ? "" : "_");// camalCase le yazilmis classin adindaki boyuk herfleri _ ile evez edir
            }
        } else {
            for(int i=0; i<splitClassName.length;i++) {// eger schema varsa i=1 den baslamalidir
                tableName += splitClassName[i].toLowerCase() + ((splitClassName.length - 1) == i ? "" : "_");// camalCase le yazilmis classin adindaki boyuk herfleri _ ile evez edir
            }
        }
        
        
        tableName = schemaName + tableName; // eger Schema varsa burda adi birlesdirilir
        return tableName;
    }

    public static String getTableNameBasedOnEntityForView(LocalEntity core) {
        String mtSt[] = core.getClass().getName().split("\\.");
        String[] splitClassName; //Class in adindan Entitini cixarir ve Boyuk herflere gore split edir
//        String schemaName = "UB_" + splitClassName[0].toUpperCase(); // Eger schema varsa onun adini goturur
        
        String tableName = "";
        String schemaName = "";
        if(SessionManager.isSchema()) {
            String classNameWithoutEntity = mtSt[mtSt.length - 1].substring(ENTITY.length());
            schemaName = classNameWithoutEntity.split("(?=[A-Z])")[0];
            splitClassName = classNameWithoutEntity.substring(schemaName.length() + 2).split("(?=[A-Z])");
            schemaName = SessionManager.SCHEMA_PREFIX + "_" + schemaName + ".";
        } else {
            splitClassName= mtSt[mtSt.length - 1].substring(ENTITY.length() + 2).split("(?=[A-Z])");
        }
        for(int i=0; i<splitClassName.length;i++) {// eger schema varsa i=1 den baslamalidir
            tableName += splitClassName[i].toLowerCase() + ((splitClassName.length - 1) == i ? "" : "_");// camalCase le yazilmis classin adindaki boyuk herfleri _ ile evez edir
        }
        tableName = schemaName + "v_" + tableName;
//        tableName = schemaName + "." + tableName; // eger Schema varsa burda adi birlesdirilir
        return tableName;
    }

//    public static String getTableNameBasedOnEntity(CoreEntity core) {
//        String mtSt[] = core.getClass().getName().split("\\.");
//        String tableName = mtSt[mtSt.length - 1].substring(ENTITY.length());
//        tableName = seperateTableFieldNameWithUnderscore(tableName).toUpperCase();
//        tableName = !core.isFunction() ? tableName : convertTableNameToFunctionFormatWithParams(core, tableName);
//
//        String dbname = core.selectDbname().trim().length() == 0
//                ? SessionManager.getCurrentDomain() : core.selectDbname().trim();
//        tableName = dbname + "." + tableName;
//        return tableName;
//    }

    private static String convertTableNameToFunctionFormatWithParams(LocalEntity core, String tableName) {
        String t = OPEN_BRACKET;
        int s = core.selectFunctionParamSize();
        for (int i = 0; i < s; i++) {
            t += "'" + core.selectFunctionParam(i) + "'";
            t += i == s - 1 ? "" : SEPERATOR_COMMA;
        }
        t += CLOSE_BRACKET;
        tableName += t;
        return tableName;
    }

    static String getTableNameBasedOnEntity(LocalEntity core, String databaseNumber) {
        String mtSt[] = core.getClass().getName().split("\\.");
        String tableName = "";
        if(SessionManager.isSchema()) {
            String tableNameWithoutEntity = mtSt[mtSt.length - 1].substring(ENTITY.length());
            tableName = tableNameWithoutEntity.substring(tableNameWithoutEntity.split("(?=[A-Z])")[0].length());
            
        } else {
           tableName = mtSt[mtSt.length - 1].substring(ENTITY.length()); 
        }
        tableName = seperateTableFieldNameWithUnderscore(tableName).toUpperCase();

        String dbname = core.selectDbname().trim().length() == 0
                ? SessionManager.getCurrentDomain() : core.selectDbname().trim();
        tableName = dbname + "." + tableName;
        return tableName;
    }

    static String capitalizeFirstLetter(String arg) {
        arg = arg.substring(0, 1).toUpperCase() + arg.substring(1, arg.length()).toLowerCase();
        return arg;
    }

    static String lowerFirstLetter(String arg) {
        arg = arg.substring(0, 1).toLowerCase() + arg.substring(1, arg.length());
        return arg;
    }

    private static Method[] concat(Method[] a, Method[] b) {
        int aLen = a.length;
        int bLen = b.length;
        Method[] c = new Method[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    public static String[] getAllGetMethodNames(LocalEntity core) {
        int idx = 0;
        String[] ListOfGetMethods = new String[1001];
        Method m[] = core.getClass().getMethods();
        Method m1[] = LocalEntity.class
                .getMethods();

        for (Method m2 : m) {
            if (m2.toString().contains(core.getClass().getName() + SEPERATOR_DOT + GET)) {
                String[] S = m2.toString().split(" ");
                ListOfGetMethods[idx] = S[S.length - 1].substring(core.getClass().getName().length() + 1, S[S.length - 1].length() - 2);
                idx++;
            }
        }

        //select all GET methods from CoreEntity
        for (Method m11 : m1) {
            if (m11.toString().contains(LocalEntity.class
                    .getName() + SEPERATOR_DOT + GET)) {
                String[] S = m11.toString().split(" ");
                ListOfGetMethods[idx] = S[S.length - 1].substring(LocalEntity.class
                        .getName().length() + 1, S[S.length - 1].length() - 2);
                idx++;
            }
        }

        String[] ls = new String[idx];
        System.arraycopy(ListOfGetMethods, 0, ls, 0, idx);
        return ls;
    }

    public static String[] getAllAttributes(LocalEntity core) {
        int idx = 0;
        String[] ListOfGetMethods = new String[1001];
        Method m[] = core.getClass().getMethods();
        Method m1[] = LocalEntity.class
                .getMethods();

        for (Method m2 : m) {
            if (m2.toString().contains(core.getClass().getName() + SEPERATOR_DOT + GET)) {
                String[] S = m2.toString().split(" ");
                ListOfGetMethods[idx] = S[S.length - 1].substring(core.getClass().getName().length() + 1, S[S.length - 1].length() - 2);
                idx++;
            }
        }

        //select all GET methods from CoreEntity
        for (Method m11 : m1) {
            if (m11.toString().contains(LocalEntity.class
                    .getName() + SEPERATOR_DOT + GET)) {
                String[] S = m11.toString().split(" ");
                ListOfGetMethods[idx] = S[S.length - 1].substring(LocalEntity.class
                        .getName().length() + 1, S[S.length - 1].length() - 2);
                idx++;
            }
        }

        String[] ls = new String[idx];
        System.arraycopy(ListOfGetMethods, 0, ls, 0, idx);
        return ls;
    }

    public static String seperateTableFieldNameWithUnderscore(String arg) {
        String rs = arg.substring(0, 1);
        String lt = "";
        for (int i = 1; i < arg.length(); i++) {
            lt = arg.substring(i, i + 1);
            char lastCh = rs.charAt(rs.length() - 1);
            String lastChar = String.valueOf(lastCh);
            if (isNumeric(lt) && (!isNumeric(lastChar)) && (!lastChar.equals(UNDERSCORE))) {
//                lt = UNDERSCORE + lt;
                lt = lt;
            } else if (isNumeric(lt) || (lt.equals(UNDERSCORE))) {
                lt = lt;
            } else if (lt.toUpperCase().equals(lt)) {
                lt = UNDERSCORE + lt;
            }

            rs = rs + lt;
        }
        return rs;
    }

    public static String seperateGroupByField(LocalEntity ent) {
        String query = "";
        for(String arg: ent.selectGroupBy()) {
            String rs = arg.substring(0, 1);
            String lt = "";
            for (int i = 1; i < arg.length(); i++) {
                lt = arg.substring(i, i + 1);
                char lastCh = rs.charAt(rs.length() - 1);
                String lastChar = String.valueOf(lastCh);
                if (isNumeric(lt) && (!isNumeric(lastChar)) && (!lastChar.equals(UNDERSCORE))) {
                    lt = UNDERSCORE + lt;
                } else if (isNumeric(lt) || (lt.equals(UNDERSCORE))) {
                    lt = lt;
                } else if (lt.toUpperCase().equals(lt)) {
                    lt = UNDERSCORE + lt;
                }

                rs = rs + lt;
            }
            
            query += rs + ",";
        }
        
        
        return query.substring(0, query.length() - 1);
    }

    static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    static String getTableFieldNameFromList(String arg[]) {
        String tbNames = "";
        /*first element of arg[] must contain a number of getMethods
         * */
        for (String arg1 : arg) {
            tbNames = tbNames + seperateTableFieldNameWithUnderscore(arg1.substring(GET.length())) + SEPERATOR_COMMA;
        }
        tbNames = tbNames.toUpperCase();
        tbNames = tbNames.substring(0, tbNames.length() - 1);

        return tbNames;
    }

    static String getTableFieldNameFromListForInsert(LocalEntity coreEnt) {
        String tbNames = "";
        /*first element of arg[] must contain a number of getMethods
         * */
        for (String methodName : coreEnt.currentInsertList()) {
            methodName = methodName.substring(0,1).toUpperCase() + methodName.substring(1);
            tbNames += seperateTableFieldNameWithUnderscore(methodName) + SEPERATOR_COMMA;
        }
        tbNames = tbNames.toUpperCase();
        tbNames = tbNames.substring(0, tbNames.length() - 1);

        return tbNames;
    }

    public static String selectGenerator(LocalEntity coreEnt, String[] methodNames) throws QException {
        String selectSql = "";
        String orderByColumn = coreEnt.selectOrderByColumn();// Eger GET yazilarsa o zaman selectin icerisine de atar ona gore xususi deyisenler basqa adla yaradilir method adi
        String orderBySort = coreEnt.selectOrderBySort();
        String slimit = coreEnt.selectStartLimit();
        String elimit = coreEnt.selectEndLimit();
        
        
        selectSql = "SELECT ";
        selectSql += coreEnt.hasDistinctFields() ? " DISTINCT " : "";
        selectSql += SPACE + generateSelectFieldPartOfSelectSql(coreEnt, false);
        selectSql += SPACE + FROM;
        selectSql += SPACE + getTableNameBasedOnEntityForView(coreEnt);
        if(coreEnt.currentAndStatementFildList().size() > 0 || 
           coreEnt.currentAndStatementFildListForNotEqual().size() > 0 || 
           coreEnt.currentAndStatementFildForIn().size() > 0 ||
           coreEnt.currentAndOrStatementFildListFullLike().size() > 0 || !coreEnt.currentSpecSqlCode().trim().isEmpty()) {
            
          selectSql +=  generateWherePartOfSelectForAndStatement(coreEnt); 
          
        }
        selectSql += coreEnt.hasSortBy() ? selectSortPartOfSelect(coreEnt) : "";

        //add limit 
        if(coreEnt.hasGroupBy()) {  
            selectSql += SPACE + "GROUP BY" + SPACE + seperateGroupByField(coreEnt) + SPACE;
        }
            
        if(!orderByColumn.trim().isEmpty() && !orderBySort.trim().isEmpty()) {
            
            selectSql += SPACE + ORDER_BY + SPACE + seperateTableFieldNameWithUnderscore(orderByColumn).toUpperCase() +  
                         SPACE + orderBySort.toUpperCase() + SPACE; 
        } else {
            selectSql += SPACE + ORDER_BY + SPACE + " ROW_NUMBER() OVER() " + SPACE; 
        }
        
//        String resultSelect = "SELECT * FROM ( SELECT ROWNUM R, A.* FROM ( " + selectSql + " ) A ) B WHERE B.R BETWEEN "+slimit+" AND " + elimit;
        String resultSelect = selectSql;
        
        if(elimit.trim().length() > 0) {
            slimit = slimit.equals("0") ? "0" : String.valueOf(Integer.parseInt(slimit) - 1);
            int countLimit = Integer.parseInt(elimit) - Integer.parseInt(slimit);
            if(countLimit != 10000)
                resultSelect += " LIMIT " + String.valueOf(countLimit) + " OFFSET " + slimit;
        }
//        int idx = Integer.parseInt(elimit) - Integer.parseInt(slimit) + 1;
//        selectSql += " LIMIT " + slimit + ", " + idx;
        return resultSelect;
    }

    public static String tableSequenceGenerator(LocalEntity ent) throws QException {
        String selectSql = "";
        String tableName = "v_show_column";
        String viewName = getTableNameBasedOnEntityForView(ent).toLowerCase();
        
        selectSql = "select show_column from " + tableName + " where view_name like '" + viewName + "'";
        
        return selectSql;
    }

    public static String hiddenColumnGenerator(LocalEntity ent, String userId) throws QException {
        String selectSql = "";
        String tableName = "v_hide_column";
        String viewName = getTableNameBasedOnEntityForView(ent).toLowerCase();
        
        selectSql = "SELECT hide_column from " + tableName + " where view_name like '"+ viewName + "' and create_user_id = " + userId;
        
        return selectSql;
    }

    public static String selectGenerator(LocalEntity coreEnt, String[] methodNames, String[] values, ArrayList valueArr) throws QException {
        return selectGenerator(coreEnt, methodNames, values, valueArr, true, false);
    }

    public static String selectGenerator(LocalEntity coreEnt, String[] methodNames,
            String[] values, ArrayList valueArr, boolean withLimit) throws QException {
        return selectGenerator(coreEnt, methodNames, values, valueArr, withLimit, false);
    }

    public static String selectGenerator(LocalEntity coreEnt,
            String[] methodNames, String[] values, ArrayList valueArr,
            boolean withLimit, boolean addEntityName) throws QException {
        String selectSql = "";

        selectSql = "SELECT ";
        selectSql += coreEnt.hasDistinctFields() ? " DISTINCT " : "";

        selectSql += SPACE + generateSelectFieldPartOfSelectSql(coreEnt, addEntityName);
        selectSql += SPACE + FROM;
        selectSql += SPACE + getTableNameBasedOnEntity(coreEnt);
        String wherePart = " where active = 1 ";
//        String wherePart = generateWherePartOfSelect(coreEnt,
//                methodNames, values, valueArr, withLimit, addEntityName);
        selectSql += wherePart;

        //add order by
        selectSql += coreEnt.hasSortBy() ? selectSortPartOfSelect(coreEnt) : " ORDER BY ID DESC ";

        //add limit 
//        String slimit = coreEnt.selectStartLimit();
//        String elimit = coreEnt.selectEndLimit();
//        int idx = Integer.parseInt(elimit) - Integer.parseInt(slimit) + 1;
//        selectSql += " LIMIT " + slimit + ", " + idx;
        return selectSql;
    }

    private static String get1stFieldOfEntityForDistinctSort(LocalEntity ent) {
        String res = "";
//        if (!ent.seIncludedField() && !ent.hasExcludedFields()){
//            res = "INSERT_DATE";
//        }
        return res;

    }

    private static String modifyKey(boolean addEntityNameToAS, String tablename, String key) {
        if (addEntityNameToAS) {
            return tablename + "." + key;
        } else {
            return key;
        }
    }

    public static String generateWherePartOfSelect(LocalEntity coreEnt, String[] methodNames, String[] values, ArrayList valueArr,
            boolean withLimit, boolean addEntityNameToAS) {
        String whereCondition = SPACE;
        String tablename = coreEnt.toDBTableName();
        String sumByField = coreEnt.selectSumBy().trim().equals("") ? "" : seperateTableFieldNameWithUnderscore(coreEnt.selectSumBy()).toUpperCase();;
        try {
            for (int i = 0; i < methodNames.length; i++) {
                if (!values[i].trim().isEmpty()) {
                    String val = values[i].trim();
                    String atrName = getAttributeNameFromMethodName(methodNames[i]);
                    String operation = EQUAL;
                    String key = seperateTableFieldNameWithUnderscore(atrName)
                            .toUpperCase();
                    String singleClause = "";
                    if (!key.trim().equals(sumByField)) {
                        singleClause = coreEnt.isDeepWhere()
                                && !val.contains(LocalLabel.IN)
                                ? new WhereSingle(key, val, valueArr).exec()
                                : getSingleClausOfWherePartOfSelect(tablename,
                                        key, val, valueArr, addEntityNameToAS);
                        singleClause = singleClause.trim().length() == 0
                                ? singleClause : singleClause + AND;
                        whereCondition = whereCondition + singleClause + SPACE;
                    }

                }
            }

            //add and statement
            if (coreEnt.hasAndStatement()) {
                String andStatement = getAndStatementOfWhereClause(coreEnt, valueArr);
                if (andStatement.length() > 0) {
                    whereCondition += andStatement;
                    whereCondition += SPACE + AND;
                }

            }

            //add andOr statement
            if (coreEnt.hasAndOrStatement()) {
                String andOrStatement = getAndOrStatementOfWhereClause(coreEnt, valueArr);
                if (andOrStatement.length() > 0) {
                    whereCondition += andOrStatement;
                    whereCondition += SPACE + AND;
                }

            }

            //add or statement
            if (coreEnt.hasOrStatement()) {
                String orStatement = getOrStatementOfWhereClause(coreEnt, valueArr);
                if (orStatement.length() > 0) {
                    whereCondition += orStatement;
                    whereCondition += SPACE + AND;
                }

            }

            //add deepWhere statement
            if (coreEnt.hasDeepWhereStatementField()) {
                int rc = coreEnt.selectDeepWhereStatementKeySize();
                for (int i = 0; i < rc; i++) {
                    String key = coreEnt.selectDeepWhereStatementKey(i);
                    String val = coreEnt.selectDeepWhereStatementValue(i);
                    key = seperateTableFieldNameWithUnderscore(key);
                    String stmt = new WhereSingle(key, val, valueArr).exec();
                    if (stmt.length() > 0) {
                        whereCondition += stmt;
                        whereCondition += SPACE + AND;
                    }
                }
            }

            if (!whereCondition.trim().equals("")) {
                whereCondition = SPACE + WHERE + SPACE + whereCondition.substring(0, whereCondition.length() - AND.length() - 1);
            }
        } catch (Exception e) {
            new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(),
                    e);
        }
        return whereCondition;
    }
    
    public static String generateWherePartOfSelectForAndStatement(LocalEntity coreEnt) {
        
        String whereCondition = SPACE + WHERE + SPACE +
        generateSetPartOfWhereSqlForAndStatement(coreEnt); // andStatementList de olan parametrlere uygun where hissesini yaradir
        
        if(whereCondition.trim().length() > WHERE.trim().length())
            return whereCondition;
        
        return "";
    }

    public static String getAndOrStatementOfWhereClause(LocalEntity entity, ArrayList valueArr) {
        String res = "";
        entity.sortAndOrStatement();
        int rc = entity.selectAndOrstatementSize();
        boolean f = true;
        String oldKey = entity.selectAndOrStatementKey(0);
        if (rc > 0) {
            String ln = "";
            for (int i = 0; i < rc; i++) {
                String key = entity.selectAndOrStatementKey(i);
                key = seperateTableFieldNameWithUnderscore(key).toUpperCase();
                String val = entity.selectAndOrStatementValue(i);
                String tname = entity.toTableName();
                String singleClause = getSingleClausOfWherePartOfSelect(tname, key, val, valueArr);
                if (i + 1 == rc) {
                    String o = "";
                    if (!entity.selectAndOrStatementKey(i).equals(oldKey)) {
                        ln = ln.substring(0, ln.length() - AND.length()) + "  " + OR;
                    }
                    oldKey = "";
                    ln += SPACE + singleClause;
                    ln += rc > 2 ? AND : OR;

                }

                if (!entity.selectAndOrStatementKey(i).equals(oldKey)) {
                    res += "( " + ln.substring(0, ln.length() - AND.length()) + ") " + OR;
                    oldKey = entity.selectAndOrStatementKey(i);
                    f = false;
                    ln = "";
                }

                if (singleClause.trim().length() > 0) {
                    ln += SPACE + singleClause;
                    ln += rc > 2 ? AND : OR;
                }

            }

            if (f) {
                res = ln;
            }

            if (res.length() > 0) {
                res = "(" + SPACE + res;
                res = res.substring(0, res.length() - 3);
                res += ")" + SPACE;
            }

        }
        return res;
    }

    public static String getAndStatementOfWhereClause(LocalEntity entity, ArrayList valueArr) {
        String res = "";
        int rc = entity.selectAndStatementSize();
        if (rc > 0) {
            for (int i = 0; i < rc; i++) {
                String key = entity.selectAndStatementKey(i);
                key = seperateTableFieldNameWithUnderscore(key).toUpperCase();
                String val = entity.selectAndStatementValue(i);
                String tname = entity.toTableName();
                String singleClause = getSingleClausOfWherePartOfSelect(tname, key, val, valueArr);
                if (singleClause.trim().length() > 0) {
                    res += SPACE + singleClause + AND;
                }
            }

            if (res.length() > 0) {
                res = "(" + SPACE + res;
                res = res.substring(0, res.length() - AND.length());
                res += ")" + SPACE;
            }

        }
        return res;
    }

    public static String getOrStatementOfWhereClause(LocalEntity entity, ArrayList valueArr) {
        String res = "";
        int rc = entity.selectOrStatementSize();
        if (rc > 0) {
            for (int i = 0; i < rc; i++) {
                String key = entity.selectOrStatementKey(i);
                key = seperateTableFieldNameWithUnderscore(key).toUpperCase();
                String val = entity.selectOrStatementValue(i);
                String tname = entity.toTableName();
                String singleClause = getSingleClausOfWherePartOfSelect(tname, key, val, valueArr);
                if (singleClause.trim().length() > 0) {
                    res += SPACE + singleClause + OR;
                }
            }

            if (res.length() > 0) {
                res = "(" + SPACE + res;
                res = res.substring(0, res.length() - OR.length());
                res += ")" + SPACE;
            }

        }
        return res;
    }

    public static String getSingleClausOfWherePartOfSelect(String tablename, String key, String val, ArrayList valueArr) {
        return getSingleClausOfWherePartOfSelect(tablename, key, val, valueArr, false);
    }

    public static String getSingleClausOfWherePartOfSelect(String tablename, String key, String val, ArrayList valueArr, boolean addEntityNameToAS) {
        String res = "";
        String operVal = "?";
        String operation = "";
        if (val.startsWith(GT)) {
            val = val.replaceFirst(GT, "");
            if (val.trim().length() > 0) {
                operation = modifyKey(addEntityNameToAS, tablename, key) + SPACE + COMMAND_GT;
                valueArr.add(val);
            }
        } else if (val.startsWith(GE)) {
            val = val.replaceFirst(GE, "");
            if (val.trim().length() > 0) {
                operation = modifyKey(addEntityNameToAS, tablename, key) + SPACE + COMMAND_GE;
                valueArr.add(val);
            }
        } else if (val.startsWith(LE)) {
            val = val.replaceFirst(LE, "");
            if (val.trim().length() > 0) {
                operation = modifyKey(addEntityNameToAS, tablename, key) + SPACE + COMMAND_LE;
                valueArr.add(val);
            }
        } else if (val.startsWith(LT)) {
            val = val.replaceFirst(LT, "");
            if (val.trim().length() > 0) {
                operation = modifyKey(addEntityNameToAS, tablename, key) + SPACE + COMMAND_LT;
//            operation = modifyKey(addEntityNameToAS, tablename, key) + SPACE + COMMAND_LT;
                valueArr.add(val);
            }
        } else if (val.startsWith(NE)) {
            val = val.replaceFirst(NE, "");
            if (val.trim().length() > 0) {
                operation = modifyKey(addEntityNameToAS, tablename, key) + SPACE + COMMAND_NE;
                valueArr.add(val);
            }
        } else if (val.startsWith(FAIZ) || val.endsWith(FAIZ)) {
//            operation = "LOWER(" + modifyKey(addEntityNameToAS, tablename, key.trim().toLowerCase()) + ")" + SPACE + COMMAND_LK;
            operation = modifyKey(addEntityNameToAS, tablename, key.trim().toLowerCase()) + SPACE + COMMAND_LK;

            valueArr.add(val.replaceAll(FAIZ, "%"));
        } else if (val.contains(BN)) {
            String v[] = val.split(BN);
            try {
                if (v[0].trim().length() > 0 && v[1].trim().length() > 0) {
                    operation = modifyKey(addEntityNameToAS, tablename, key) + SPACE + COMMAND_BN;
                    operVal = QUESTION_MARK + SPACE + AND + SPACE + QUESTION_MARK;
                    valueArr.add(v[0].trim());
                    valueArr.add(v[1].trim());
                }
            } catch (Exception e1) {
                val = ""; //bu sorgu sifirlans;
            }

        } else if (val.contains(IN)) {
            try {
                operation = modifyKey(addEntityNameToAS, tablename, key) + SPACE + COMMAND_IN;
                operVal = OPEN_BRACKET;
                String v[] = val.split(IN);
                for (String v1 : v) {
                    if (v1.trim().length() > 0) {
                        operVal = operVal + QUESTION_MARK + SEPERATOR_COMMA;
                        valueArr.add(v1.trim());
                    }
                }
                if (operVal.trim().length() > 0) {
                    operVal = operVal.substring(0, operVal.length() - 1) + CLOSE_BRACKET;
                }
            } catch (Exception e1) {
                val = ""; //bu sorgu sifirlans;
            }
        }

        if (operation.trim().length() == 0) {
            if (val.trim().length() > 0) {
                operation = modifyKey(addEntityNameToAS, tablename, key) + SPACE + EQUAL;
                valueArr.add(val);
                res = operation + SPACE + operVal + SPACE;
            }
        } else if (val.trim().length() > 0) {
            res = operation + SPACE + operVal + SPACE;
        }

        return res;
    }

        public static String generateWherePartOfUpdate(LocalEntity coreEnt, String value, ArrayList valueArr) {
        String whereCondition = "";
        String tablename = coreEnt.toDBTableName();
        String operation = "";
        String key = "ID";
        String operVal;
        try {
            if (!value.trim().isEmpty()) {
                if (value.contains(IN)) {
                    operation = key + SPACE + COMMAND_IN;
                    operVal = OPEN_BRACKET;
                    String v[] = value.split(IN);
                    for (String v1 : v) {
                        operVal = operVal + QUESTION_MARK + SEPERATOR_COMMA;
                        valueArr.add(v1.trim());
                    }
                    operVal = operVal.substring(0, operVal.length() - 1) + CLOSE_BRACKET;
                    operation += operVal;
                } else {
                    operation = key + SPACE + EQUAL + QUESTION_MARK;
                    valueArr.add(value);
                }
            }
            if (!operation.trim().equals("")) {
                whereCondition = SPACE + WHERE + SPACE + operation;
            }
        } catch (Exception e) {
            new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(),
                    e);
        }
        return whereCondition;
    }

    public static String generateWherePartOfUpdateByOtherParams(Transporter carrier, List<String> params) throws QException {
        String whereCondition = "";
        String operation = "";
        for(String s: params) {
            String param = s.substring(0,1).toUpperCase() + s.substring(1);
            String value = carrier.getValue(s).toString();
            try {
                Long.valueOf(value);
                operation += " and " + seperateTableFieldNameWithUnderscore(param).toUpperCase() + " = " + value;
            } catch (Exception e) {
                
                operation += " and " + seperateTableFieldNameWithUnderscore(param).toUpperCase() + " = '" + value + "'";
            }
            
        }
        
        operation = operation.substring(4);
                if (!operation.trim().equals("")) {
                    whereCondition = SPACE + WHERE + SPACE + operation;
                }
        return whereCondition;
    }

    private static String getAttributeNameFromMethodName(String methodname) {
        String fname = methodname.substring("GET".length(), methodname.length());
        fname = lowerFirstLetter(fname);
        return fname;
    }

    public static String selectSortPartOfSelect(LocalEntity ent) {
        String s = "";
        String[] st = ent.selectSort();
        String s1[] = ent.selectDistinctFields();

        for (String st1 : st) {
            if (st1.trim().length() > 0) {
                if ((s1.length == 0) || (s1.length > 0 && Arrays.asList(s1).contains(st1))) {
                    String sby = ent.selectSortByAsc() ? " ASC " : " DESC ";
                    String t = seperateTableFieldNameWithUnderscore(st1).toUpperCase() + sby;
                    s = s + t + SEPERATOR_COMMA;
                }
            }

        }
        s = s.substring(0, s.length() - 1);
        s = s.trim().length() == 0 ? " ORDER BY ID DESC" : "ORDER BY " + s;
        return s;
    }

    public static String selectByIdGenerator(String sqlId, String params[]) throws QException {
        try {
            String querySql = "";
            String tempTableName = "tempTableName";
            String tmpSqlQuery = generateSqlQueryById(sqlId);
            querySql = mergeSqlQueriesByRequiredSqlId(tempTableName, tmpSqlQuery, params);
            //Carrier qc = getSqlQeuryById(tempTableName,tmpSqlQuery);
            return querySql;
        } catch (Exception ex) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(),
                    ex);
        }
    }

    private static String mergeSqlQueriesByRequiredSqlId(String tableName, String coreSqlQuery, String params[])
            throws Exception {
//        Carrier qc = getSqlQeuryById(tableName, coreSqlQuery);
//        String st = generateMainSqlQueryBasedOnCarrier(tableName, qc);
//        String updateStr = replaceParamsInMainSql(st, params);
//        return updateStr;
        return null;
    }

    private static String generateMainSqlQueryBasedOnCarrier(String tableName, Transporter qc) throws Exception {
        String query = "";
        int cnt = qc.getTableRowCount(tableName);
        for (int i = 0; i < cnt; i++) {
            query += qc.getValue(tableName, i, FIELD_SQL.toLowerCase()).toString().trim();
        }
        return query;
    }

    private static String replaceParamsInMainSql(String mainSql, String params[]) throws Exception {
        String query = mainSql;
        for (int i = 0; i < params.length; i++) {
            query = query.replace(PATTERN_FOR_PARAMS + Integer.toString(i + 1), params[i]);
        }
        return query;
    }

    private static String generateSqlQueryById(String sqlId) throws QException {
        String query = SELECT + SPACE + STAR + SPACE + FROM + SPACE + TABLE_SQL_SCRIPT + SPACE + WHERE + SPACE + SPACE + FIELD_ID + EQUAL
                + QUOTE + sqlId.trim() + QUOTE + SPACE + ORDER_BY + SPACE + FIELD_PART;
        return query;
    }

    public static String selectByConditionGenerator(LocalEntity coreEnt) throws QException {
        String selectSql = SELECT + SPACE + STAR + SPACE + FROM + SPACE + getTableNameBasedOnEntity(coreEnt);
        try {
            String wCon = generateWherePartOfSelectMultipeSql(coreEnt);
            if (!wCon.trim().equals("")) {
                selectSql += SPACE + WHERE + SPACE + wCon;
            }
        } catch (Exception e) {
            throw new QException(e);
        }
        return selectSql;
    }

    private static String generateWherePartOfSelectMultipeSql(LocalEntity coreEnt) throws QException {
        String wCon = SPACE;
        String[] methodNames = getAllGetMethodNames(coreEnt);
        for (int i = 0; i < methodNames.length; i++) {
            try {
                String fieldName = seperateTableFieldNameWithUnderscore(methodNames[i].substring("GET".length(), methodNames[i].length())).toUpperCase();
                String value = executeMethod(coreEnt, methodNames[i]).toString().trim();
                String prefix = startPrefix(value);
                String pureValue = removePrefixFromTheValue(value);
                if (!pureValue.trim().isEmpty()) {
                    wCon = wCon + generateWhereCondByStartPrefix(fieldName, prefix, pureValue.trim()) + SPACE + AND + SPACE;
                }
            } catch (QException ex) {
                throw new QException(new Object() {
                }.getClass().getEnclosingClass().getName(),
                        new Object() {
                }.getClass().getEnclosingMethod().getName(),
                        ex);
            }
        }
        return wCon.substring(1, wCon.length() - AND.length() - 1);
    }

    private static String generateWherePartOfUpdateSql(LocalEntity coreEnt) throws QException {
        String wCon = SPACE;
        String[] methodNames = getAllGetMethodNames(coreEnt);
        for (String methodName : methodNames) {
            try {
                String fieldName = seperateTableFieldNameWithUnderscore(methodName.substring("GET".length(), methodName.length())).toUpperCase();
                String value = executeMethod(coreEnt, methodName).toString().trim();
                String prefix = startPrefix(value);
                String pureValue = removePrefixFromTheValue(value);
                if (!pureValue.trim().isEmpty()) {
                    wCon = wCon + generateWhereCondByStartPrefix(fieldName, prefix, pureValue.trim()) + SPACE + AND + SPACE;
                }
            } catch (QException ex) {
                throw new QException(new Object() {
                }.getClass().getEnclosingClass().getName(),
                        new Object() {
                }.getClass().getEnclosingMethod().getName(),
                        ex);
            }
        }
        return wCon.substring(1, wCon.length() - AND.length() - 1);
    }

    private static String generateWherePartOfDeleteSql(LocalEntity coreEnt) throws QException {
        String query = "1=1 ";
        
        for(String s: coreEnt.currentDeleteWhereParamsList()) {
            query += " and " + seperateTableFieldNameWithUnderscore(s) + "=?";
        }
        
        return query;
    }

    private static String removePrefixFromTheValue(String value) {
        String prefix = startPrefix(value);
        String pureValue = value;
        if (!prefix.trim().equals("")) {
            pureValue = value.substring(3, value.length());
        }//bizim sertimize goze valuenin ilk uc caracter'i prefix olacaq.pureValue 
        //hissesi ise 3-cu karakterden sonra olan hisse nezerde tutulacaq;
        return pureValue;
    }

    private static String splitValueBy2Dots(String value, String prefix) {
        String st = "";
        if (!prefix.equals("")) {
            st = value.trim().split(TWO_DOTS)[1];
        } else {
            st = value;
        }
        return st.trim();
    }

    private static String[] splitValueBy2DotsForBetweenCommand(String value) {
        String[] st = value.trim().split(TWO_DOTS);
        //lse{st = value;}
        return st;
    }

    public static String startPrefix(String arg) {
        String st = "";
        if (arg.startsWith(NE + TWO_DOTS)) {
            st = NE;
        } else if (arg.startsWith(LT + TWO_DOTS)) {
            st = LT;
        } else if (arg.startsWith(GT + TWO_DOTS)) {
            st = GT;
        } else if (arg.startsWith(GE + TWO_DOTS)) {
            st = GE;
        } else if (arg.startsWith(LE + TWO_DOTS)) {
            st = LE;
        } else if (arg.startsWith(BN + TWO_DOTS)) {
            st = BN;
        } else if (arg.startsWith(LK + TWO_DOTS)) {
            st = LK;
        } else if (arg.startsWith(IN + TWO_DOTS)) {
            st = IN;
        }

        return st;
    }

    public static String generateWhereCondByStartPrefix(String fieldName, String prefix, String value) {
        String rs = "";
        //String vl = splitValueBy2Dots(value.trim(),prefix).trim();
        String vl = value;
        if (prefix.equals(NE)) {
            rs = fieldName + " " + COMMAND_NE + " " + QUOTE + vl + QUOTE;
        } else if (prefix.equals(GT) && (!vl.equals(""))) {
            rs = fieldName + " " + COMMAND_GT + " " + QUOTE + vl + QUOTE;
        } else if (prefix.equals(GE) && (!vl.equals(""))) {
            rs = fieldName + " " + COMMAND_GE + " " + QUOTE + vl + QUOTE;
        } else if (prefix.equals(LT) && (!vl.equals(""))) {
            rs = fieldName + " " + COMMAND_LT + " " + QUOTE + vl + QUOTE;
        } else if (prefix.equals(LE) && (!vl.equals(""))) {
            rs = fieldName + " " + COMMAND_LE + " " + QUOTE + vl + QUOTE;
        } else if (prefix.equals(LK) && (!vl.equals(""))) {
            rs = fieldName + " " + COMMAND_LK + " " + QUOTE + vl + QUOTE;
        } else if (prefix.equals(BN)) {
            String st[] = splitValueBy2DotsForBetweenCommand(value);
            rs = fieldName + " " + COMMAND_BN + " " + concatStringArrayForBetweenStatement(st);
        } else if (prefix.equals(IN)) {
            String st[] = splitValueBy2DotsForBetweenCommand(value);
            if (st.length > 0) {
                rs = fieldName + " " + COMMAND_IN + " " + OPEN_BRACKET + concatStringArrayForInStatement(st) + CLOSE_BRACKET + SPACE;
            }
        } else {
            rs = fieldName + " " + EQUAL + " " + QUOTE + value.trim() + QUOTE;
        }

        return rs;
    }

    private static String concatStringArrayForInStatement(String arg[]) {
        String st = "";
        for (String arg1 : arg) {
            st = st + QUOTE + arg1 + QUOTE + ",";
        }
        return st.substring(0, st.length() - 1);//-1 : silinmesine goredir
    }

    private static String concatStringArrayForBetweenStatement(String arg[]) {
        String st = "";
        if (arg.length >= 2) {//for between statement we need 2 values;
            for (int i = 1; i < arg.length; i++) {
                st = st + QUOTE + arg[0] + QUOTE
                        + SPACE + AND + SPACE + QUOTE + arg[1] + QUOTE + " ";
            }
        } else if (arg.length == 1) {
            st = st + QUOTE + arg[0] + QUOTE
                    + SPACE + AND + SPACE + QUOTE + "" + QUOTE + " ";
        } else {
            st = st + QUOTE + "" + QUOTE
                    + SPACE + AND + SPACE + QUOTE + "" + QUOTE + " ";
        }

        return st.substring(0, st.length() - 1);//-1 : silinmesine goredir
    }

    private static boolean isStartsWithPrefix(String arg) {

        return true;
    }
    
    private static String rtrim(String st) {
        String value = "";
        if(st.trim().isEmpty()) return st;
        
        value = "." + st;
        
        return value.trim().substring(1);
    }
}
