/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.validation;

import com.flip.domain.SessionManager;
import com.flip.domain.Transporter;
import com.flip.utils.Crypto;
import com.flip.utils.EntityManager;
import com.flip.utils.QDate;
import com.flip.view.EntityV_BasicRent;
import com.flip.view.EntityV_Users;
import org.apache.log4j.Logger;

/**
 *
 * @author ahmadov
 */
public class RentValidator {
    private static final Logger log = Logger.getLogger(RentValidator.class);
    
    public void add() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            t.setValue("flipCoin", "0");
//            t.checkParams("ownerTypeStatus,countryId,cityId,address,addressLongitude,addressLatitude,"
//                    + "roomCountId,repairId,balconiesCountId,floors,buildingFloors,roomArea,"
//                    + "elevatorStatusId,elevatorCountId,windowCountId,courtyardWindowStatus,"
//                    + "streetWindowStatus,heatingTypeId,deedOfSaleStatus,mortgageStatus,bathroomTypeId,"
//                    + "bathroomCountId,kitchenTypeId,kitchenArea,furnitureStatus,airConditionerStatus,"
//                    + "kitchenFurnitureStatus,guardStatus,title,description,amount,pictureIds");
            if(SessionManager.getCurrentUserId().equals("0")) {
                t.checkParams("createUserFullname,createUserContact,createUserEmail");
                
            } else {
                EntityV_Users users = new EntityV_Users();
                Transporter transporter = new Transporter();
                transporter.setValue("id", SessionManager.getCurrentUserId());
                String flipCoin = EntityManager.getColumnValueByColumnName(users, "flipCoin", transporter);
                t.setValue("flipCoin", flipCoin);
            }
            t.setValue("operationCode", Crypto.randomNumber(6));
            String code = "R-" + Crypto.randomNumber(8);
            t.setValue("activityStatus", "1");
            t.setValue("code", code);
            t.setValue("publishDate", QDate.getCurrentDate());
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void edit() {
        try {
            Transporter t = SessionManager.sessionTransporter();
//            t.checkParams("id,ownerTypeStatus,countryId,cityId,address,addressLongitude,addressLatitude,"
//                    + "roomCountId,repairId,balconiesCountId,floors,buildingFloors,roomArea,"
//                    + "elevatorStatusId,elevatorCountId,windowCountId,courtyardWindowStatus,"
//                    + "streetWindowStatus,heatingTypeId,deedOfSaleStatus,mortgageStatus,bathroomTypeId,"
//                    + "bathroomCountId,kitchenTypeId,kitchenArea,furnitureStatus,airConditionerStatus,"
//                    + "kitchenFurnitureStatus,guardStatus,title,description,amount,pictureIds");
            if(SessionManager.getCurrentUserId().equals("0") || 
               !t.getValue("operationCode").toString().isEmpty()) {
                t.checkParams("operationCode");
                EntityV_BasicRent apartment = new EntityV_BasicRent();
                Transporter transporter = new Transporter();
                transporter.setValue("id", t.getValue("id"));
                transporter.setValue("operationCode", t.getValue("operationCode"));
                transporter = EntityManager.getEntValuesByCarrier(apartment, transporter);
                String userId = transporter.getValue(apartment.toTableName(), 0, "createUserId").toString();
                if(transporter.getTableRowCount(apartment.toTableName()) == 0) {
                    t.addErrorMessage("invalidCode", "Invalid code");
                    return;
                }
                t.removeKey("createUserEmail");
                if(!userId.trim().isEmpty() && !userId.trim().equals("0")) {
                    t.removeKey("createUserFullname");
                    t.removeKey("createUserContact");
                    
                } else {
                   t.checkParams("createUserFullname,createUserContact"); 
                }
                
                
                
            } else {
                EntityV_BasicRent apartment = new EntityV_BasicRent();
                Transporter transporter = new Transporter();
                transporter.setValue("id", t.getValue("id"));
                transporter.setValue("createUserId", SessionManager.getCurrentUserId());
                if(EntityManager.getRowCount(apartment, transporter) == 0) {
                    t.addErrorMessage("invalidParameters", "Invalid parameters");
                }
            }
            
            t.setValue("activityStatus", "1");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    public void delete() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            t.checkParams("id");
            if(SessionManager.getCurrentUserId().equals("0") || 
               !t.getValue("operationCode").toString().isEmpty()) {
                t.checkParams("operationCode");
                EntityV_BasicRent apartment = new EntityV_BasicRent();
                Transporter transporter = new Transporter();
                transporter.setValue("id", t.getValue("id"));
                transporter.setValue("operationCode", t.getValue("operationCode"));
                if(EntityManager.getRowCount(apartment, transporter) == 0) {
                    t.addErrorMessage("invalidCode", "Invalid code");
                }
//                t.setValue("operationCode", Crypto.randomNumber(6));
            } else {
                EntityV_BasicRent apartment = new EntityV_BasicRent();
                Transporter transporter = new Transporter();
                transporter.setValue("id", t.getValue("id"));
                transporter.setValue("createUserId", SessionManager.getCurrentUserId());
                if(EntityManager.getRowCount(apartment, transporter) == 0) {
                    t.addErrorMessage("invalidParameters", "Invalid parameters");
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    public void getRentList() {
        try {
            SessionManager.sessionTransporter().setValue("activityStatus", "1");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void getOwnRentList() {
        try {
            SessionManager.sessionTransporter().setValue("createUserId", SessionManager.getCurrentUserId());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void advanceSearch() {
        try {
            SessionManager.sessionTransporter().setValue("activityStatus", "1");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void pushTheAdForward() {
        try {
            
            Transporter t = SessionManager.sessionTransporter();
            t.checkParams("id");
            
            if(SessionManager.getCurrentUserId().equals("0")) {
                t.addErrorMessage("unregisteredUser", "Qeydiyyatsız istifadəçi");
                return;
            }
            
            EntityV_Users ent = new EntityV_Users();
            Transporter transporter = new Transporter();
            transporter.setValue("id", SessionManager.getCurrentUserId());
            String flipCoin = EntityManager.getColumnValueByColumnName(ent, "flipCoin", transporter);
            if(Double.valueOf(flipCoin) < 1) {
               t.addErrorMessage("lowBalance", "You don't have enough balance for this operation."); 
               return;
            }
            
            t.setValue("flipCoin", flipCoin);
            t.setValue("publishDate", QDate.getCurrentDate());
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void getSimilarAnnouncement() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            t.checkParams("id");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public void forgetOperationCode() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            t.checkParams("id,createUserEmail");
            String email = t.getValue("createUserEmail").toString();
            EntityV_BasicRent ent = new EntityV_BasicRent();
            Transporter transporter = new Transporter();
            transporter.setValue("id", t.getValue("id"));
            transporter = EntityManager.getEntValuesByCarrier(ent, transporter);
            String operationCode = transporter.getValue(ent.toTableName(), 0, "operationCode").toString();
            String createUserEmail = transporter.getValue(ent.toTableName(), 0, "createUserEmail").toString();
            if(!createUserEmail.equals(email)) {
                t.addErrorMessage("invalidEmail", "Email yanlışdır!");
                return;
            }
            
            t.setValue("operationCode", operationCode);
            t.setValue("createUserEmail", createUserEmail);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
