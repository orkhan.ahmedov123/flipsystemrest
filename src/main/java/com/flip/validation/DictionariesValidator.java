/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.validation;

import com.flip.domain.SessionManager;
import com.flip.domain.Transporter;
import com.flip.utils.EntityManager;
import com.flip.view.EntityV_Dictionaries;
import com.flip.view.EntityV_DictionaryTypes;
import com.flip.enums.RequiredConstants;


/**
 *
 * @author otahmadov
 */
public class DictionariesValidator {
    
    public void add() throws Exception {
        
        Transporter t = SessionManager.sessionTransporter();
        EntityV_Dictionaries ent = new EntityV_Dictionaries();
        
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN")) {
            t.addError("userType", "Invalid user type");
            return;
        }
        Transporter transporter = new Transporter();
        transporter.setValue("code", t.getValue("code"));
        if(EntityManager.checkParamsBySelectRowCountSpec(transporter, ent, false, "code")) {
            return;
        }
        
        t.checkParams(((RequiredConstants)SessionManager.getRequiredConstants()).getAddDictionaries());
    }
    
    public void edit() throws Exception {
        
        Transporter t = SessionManager.sessionTransporter();
        
        EntityV_Dictionaries ent = new EntityV_Dictionaries();
        
        if(SessionManager.getCurrentUserType().equals("STUDENT") || SessionManager.getCurrentUserType().equals("TEACHER")) {
            t.addError("userType", "Invalid user type");
            return;
        }
        
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN")) {
            Transporter dic = new Transporter();
            EntityV_DictionaryTypes ent2 = new EntityV_DictionaryTypes();

            dic.setValue("id", t.getValue("typeId").toString());
            String hiddenStatus = EntityManager.getColumnValueByColumnName(ent2, "hiddenStatus", dic);

            if(hiddenStatus.equals("1")) {
                t.addError("typeId", "Invalid dic type id");
                return;
            }
        }
        
        Transporter transporter = new Transporter();
        transporter.setValue("code", t.getValue("code"));
        if(EntityManager.checkParamsBySelectRowCountSpec(transporter, ent, true, "code")) {
            return;
        }
        
        t.checkParams(((RequiredConstants)SessionManager.getRequiredConstants()).getEditDictionaries());
    }
    
    public void delete() throws Exception  {
        Transporter t = SessionManager.sessionTransporter();
        
        if(SessionManager.getCurrentUserType().equals("STUDENT") || SessionManager.getCurrentUserType().equals("TEACHER")) {
            t.addError("userType", "Invalid user type");
            return;
        }
        
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN")) {
            Transporter dic = new Transporter();
            EntityV_DictionaryTypes ent2 = new EntityV_DictionaryTypes();

            dic.setValue("id", t.getValue("typeId").toString());
            String hiddenStatus = EntityManager.getColumnValueByColumnName(ent2, "hiddenStatus", dic);

            if(hiddenStatus.equals("1")) {
                t.addError("typeId", "Invalid dic type id");
                return;
            }
        }
        t.checkParams(((RequiredConstants)SessionManager.getRequiredConstants()).getDeleteDictionaries());
    }
    
    public void allView() throws Exception {
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN")) {
            SessionManager.sessionTransporter().setValue("hiddenStatus", "0");
        }
        SessionManager.sessionTransporter().checkParams(((RequiredConstants)SessionManager.getRequiredConstants()).getGetDictionaries());
    }
    
    public void getDictionaryTypeList() throws Exception {
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN")) {
            SessionManager.sessionTransporter().setValue("hiddenStatus", "0");
        }
    }
    
    public void getDictionariesListByCommon() throws Exception {
        Transporter t = SessionManager.sessionTransporter();
        t.checkParams("typeId");
    }
    
    public void getDictionariesListByMultiCode() throws Exception {
        Transporter t = SessionManager.sessionTransporter();
        t.checkParamsTBL("code", "dictionaries");
    }
}
