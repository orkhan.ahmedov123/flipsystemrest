/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.validation;

import com.flip.domain.SessionManager;
import com.flip.domain.Transporter;
import com.flip.utils.EntityManager;
import com.flip.view.EntityV_BasicFavoriteAnnouncement;
import org.apache.log4j.Logger;

/**
 *
 * @author ahmadov
 */
public class FavoriteValidator {
    private static final Logger log = Logger.getLogger(FavoriteValidator.class);
    public void addOrDelete() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            t.checkParams("announcementId,announcementType,operationType");
            String operationType = t.getValue("operationType").toString();
            String id = t.getValue("id").toString();
            
            if(operationType.equals("DELETE")) {
                t.checkParams("id");
            }
            
            if(t.hasError()) {
                return;
            }
            
            String announcementId = t.getValue("announcementId").toString();
            EntityV_BasicFavoriteAnnouncement ent = new EntityV_BasicFavoriteAnnouncement();
            Transporter transporter = new Transporter();
            if(operationType.equals("ADD")) {
                transporter.setValue("announcementId", announcementId);
                transporter.setValue("createUserId", SessionManager.getCurrentUserId());
                if(EntityManager.getRowCount(ent, transporter) > 0) {
                    t.addErrorMessage("duplicateAnnouncement", "This announcement already in favorite list");
                    return;
                }
            } else if(operationType.equals("DELETE")) {
                transporter.setValue("id", id);
                transporter.setValue("createUserId", SessionManager.getCurrentUserId());
                if(EntityManager.getRowCount(ent, transporter) == 0) {
                    t.addErrorMessage("invalidParams", "Invalid params");
                    return;
                }
            }
            
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void allView() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            t.setValue("createUserId", SessionManager.getCurrentUserId());
            
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void getUserFavoriteAnnouncementCount() {
        
    }
}
