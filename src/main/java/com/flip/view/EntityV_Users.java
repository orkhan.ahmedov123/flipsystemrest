/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.view;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityV_Users extends LocalEntity {
    private String email = "";
    private String phone = "";
    private String userType = "";
    private String registerType = "";
    private String photoFileId = "";
    private String fullname = "";
    private String confirmStatus = "";
    private String flipCoin = "";
}
