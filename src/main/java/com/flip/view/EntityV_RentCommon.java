/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.view;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntityV_RentCommon extends LocalEntity {
    
    private String activityStatus = "";
    private String typeId = "";
    private String typeNameAz = "";
    private String typeNameEn = "";
    private String typeNameRu = "";
    private String typeParentId = "";
    private String countryId = "";
    private String countryNameAz = "";
    private String countryNameEn = "";
    private String countryNameRu = "";
    private String cityId = "";
    private String cityNameAz = "";
    private String cityNameEn = "";
    private String cityNameRu = "";
    private String rentalPeriodId = "";
    private String rentalPeriodNameAz = "";
    private String rentalPeriodNameEn = "";
    private String rentalPeriodNameRu = "";
    private String roomCountId = "";
    private String roomCountName = "";
    private String repairId = "";
    private String repairNameAz = "";
    private String repairNameEn = "";
    private String repairNameRu = "";
    private String balconiesCountId = "";
    private String balconiesCountName = "";
    private String elevatorStatusId = "";
    private String elevatorStatusNameAz = "";
    private String elevatorStatusNameEn = "";
    private String elevatorStatusNameRu = "";
    private String elevatorCountId = "";
    private String elevatorCountName = "";
    private String windowCountId = "";
    private String windowCountName = "";
    private String metroId = "";
    private String metroNameAz = "";
    private String metroNameEn = "";
    private String metroNameRu = "";
    private String heatingTypeId = "";
    private String heatingTypeNameAz = "";
    private String heatingTypeNameEn = "";
    private String heatingTypeNameRu = "";
    private String bathroomTypeId = "";
    private String bathroomTypeNameAz = "";
    private String bathroomTypeNameEn = "";
    private String bathroomTypeNameRu = "";
    private String bathroomCountId = "";
    private String bathroomCountName = "";
    private String kitchenTypeId = "";
    private String kitchenTypeNameAz = "";
    private String kitchenTypeNameEn = "";
    private String kitchenTypeNameRu = "";
    private String objectBuildingTypeId = "";
    private String objectBuildingTypeNameAz = "";
    private String objectBuildingTypeNameEn = "";
    private String objectBuildingTypeNameRu = "";
    private String mansardId = "";
    private String mansardNameAz = "";
    private String mansardNameEn = "";
    private String mansardNameRu = "";
    private String undergroundParkingId = "";
    private String undergroundParkingNameAz = "";
    private String undergroundParkingNameEn = "";
    private String undergroundParkingNameRu = "";
    private String floorCountId = "";
    private String floorCountNameAz = "";
    private String floorCountNameEn = "";
    private String floorCountNameRu = "";
    private String ownerTypeStatus = "";
    private String childrenAllowedStatus = "";
    private String animalAllowedStatus = "";
    private String onlyFamilyStatus = "";
    private String tvStatus = "";
    private String washerStatus = "";
    private String dishWashindStatus = "";
    private String refrigeratorStatus = "";
    private String showerCubicleStatus = "";
    private String courtyardWindowStatus = "";
    private String streetWindowStatus = "";
    private String furnitureStatus = "";
    private String airConditionerStatus = "";
    private String kitchenFurnitureStatus = "";
    private String guardStatus = "";
    private String bathroomStatus = "";
    private String parkingStatus = "";
    private String electricityStatus = "";
    private String gasStatus = "";
    private String sewerageStatus = "";
    private String waterSupplyStatus = "";
    private String liftStatus = "";
    private String garageStatus = "";
    private String telephoneStatus = "";
    private String internetStatus = "";    
    private String poolStatus = "";
    private String yardBathroomStatus = "";
    private String alcoveStatus = "";
    private String summerKitchenStatus = "";
    private String saunaStatus = "";
    private String securityCameraStatus = "";
    
    private String address = "";
    private String addressLongitude = "";
    private String addressLatitude = "";
    private String youtubeLink = "";
    private String title = "";
    private String description = "";
    private String amount = "";
    private String curfewPayment = "";
    private String floors = "";
    private String buildingFloors = "";
    private String roomArea = "";
    private String kitchenArea = "";
    private String square = "";
    private String bedroomCount = "";
    private String houseArea = "";
    private String landArea = "";
    private String distanceMainHighway = "";
    private String distanceSea = "";
    private String pictureIds = "";
    private String createUserFullname = "";
    private String createUserContact = "";
    private String createUserEmail = "";
    private String createUserPhotoId = "";
    private String code = "";
    private String viewCount = "";
}
