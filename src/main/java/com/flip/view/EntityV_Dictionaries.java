/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.view;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntityV_Dictionaries extends LocalEntity {
    private String nameAz = "";
    private String nameEn = "";
    private String nameRu = "";
    private String aboutAz = "";
    private String aboutEn = "";
    private String aboutRu = "";
    private String typeId = "";
    private String parentId = "";
    private String code = "";
    private String icon = "";
    private String parentIds = "";
}
