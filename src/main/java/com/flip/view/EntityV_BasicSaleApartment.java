/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.view;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntityV_BasicSaleApartment extends LocalEntity {
    private String ownerTypeStatus = "";
    private String countryId = "";
    private String cityId = "";
    private String address = "";
    private String addressLongitude = "";
    private String addressLatitude = "";
    private String roomCountId = "";
    private String repairId = "";
    private String balconiesCountId = "";
    private String floors = "";
    private String buildingFloors = "";
    private String roomArea = "";
    private String elevatorStatusId = "";
    private String elevatorCountId = "";
    private String windowCountId = "";
    private String courtyardWindowStatus = "";
    private String streetWindowStatus = "";
    private String metroId = "";
    private String heatingTypeId = "";
    private String deedOfSaleStatus = "";
    private String mortgageStatus = "";
    private String bathroomTypeId = "";
    private String bathroomCountId = "";
    private String kitchenTypeId = "";
    private String kitchenArea = "";
    private String furnitureStatus = "";
    private String airConditionerStatus = "";
    private String kitchenFurnitureStatus = "";
    private String guardStatus = "";
    private String youtubeLink = "";
    private String title = "";
    private String description = "";
    private String amount = "";
    private String activityStatus = "";
    private String operationCode = "";
}
