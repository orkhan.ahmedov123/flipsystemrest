/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.view;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntityV_FavoriteAnnouncement extends LocalEntity {

    private String announcementId = "";
    private String typeId = "";
    private String announcementType = "";
    private String announcementTitle = "";
    private String announcementDescription = "";
    private String announcementAmount = "";
    private String announcementPictureIds = "";
    private String announcementFloors = "";
    private String announcementRoomArea = "";
    private String announcementLivingSpace = "";
    private String announcementSquare = "";
    private String announcementHouseArea = "";
    private String announcementLandArea = "";
    private String districtId = "";
    private String districtNameAz = "";
    private String districtNameEn = "";
    private String districtNameRu = "";
    private String cityNameAz = "";
    private String cityNameEn = "";
    private String cityNameRu = "";
    private String roomCountId = "";
    private String roomCountName = "";
    private String announcementAddress = "";
    private String metroId = "";
    private String metroName = "";

}
