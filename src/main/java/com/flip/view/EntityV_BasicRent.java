/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.view;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntityV_BasicRent extends LocalEntity {
    
    private String typeId = "";
    private String countryId = "";
    private String cityId = "";
    private String districtId = "";
    private String settlementId = "";
    private String ownerTypeStatus = "";
    private String activityStatus = "";
    private String address = "";
    private String addressLongitude = "";
    private String addressLatitude = "";
    private String youtubeLink = "";
    private String title = "";
    private String description = "";
    private String amount = "";
    private String rentalPeriodId = "";
    private String pictureIds = "";
    private String createUserFullname = "";
    private String createUserContact = "";
    private String createUserEmail = "";
    private String createUserPhotoId = "";
    private String operationCode = "";
}
