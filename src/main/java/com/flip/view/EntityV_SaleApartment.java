/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.view;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntityV_SaleApartment extends LocalEntity {
    private String ownerTypeStatus = "";
    private String countryId = "";
    private String countryNameAz = "";
    private String countryNameEn = "";
    private String countryNameRu = "";
    private String cityId = "";
    private String cityNameAz = "";
    private String cityNameEn = "";
    private String cityNameRu = "";
    private String address = "";
    private String addressLongitude = "";
    private String addressLatitude = "";
    private String roomCountId = "";
    private String roomCountName = "";
    private String repairId = "";
    private String repairNameAz = "";
    private String repairNameEn = "";
    private String repairNameRu = "";
    private String balconiesCountId = "";
    private String balconiesCountName = "";
    private String floors = "";
    private String buildingFloors = "";
    private String roomArea = "";
    private String livingSpace = "";
    private String elevatorStatusId = "";
    private String elevatorStatusNameAz = "";
    private String elevatorStatusNameEn = "";
    private String elevatorStatusNameRu = "";
    private String elevatorCountId = "";
    private String elevatorCountName = "";
    private String windowCountId = "";
    private String windowCountName = "";
    private String courtyardWindowStatus = "";
    private String streetWindowStatus = "";
    private String metroId = "";
    private String metroNameAz = "";
    private String metroNameEn = "";
    private String metroNameRu = "";
    private String heatingTypeId = "";
    private String heatingTypeNameAz = "";
    private String heatingTypeNameEn = "";
    private String heatingTypeNameRu = "";
    private String deedOfSaleStatus = "";
    private String mortgageStatus = "";
    private String bathroomTypeId = "";
    private String bathroomTypeNameAz = "";
    private String bathroomTypeNameEn = "";
    private String bathroomTypeNameRu = "";
    private String bathroomCountId = "";
    private String bathroomCountName = "";
    private String kitchenTypeId = "";
    private String kitchenTypeNameAz = "";
    private String kitchenTypeNameEn = "";
    private String kitchenTypeNameRu = "";
    private String kitchenArea = "";
    private String furnitureStatus = "";
    private String airConditionerStatus = "";
    private String kitchenFurnitureStatus = "";
    private String guardStatus = "";
    private String youtubeLink = "";
    private String title = "";
    private String description = "";
    private String amount = "";
    private String activityStatus = "";
    private String createUserFullname = "";
    private String createUserContact = "";
    private String createUserEmail = "";
    private String createUserPhotoId = "";
    private String pictureIds = "";
    private String typeId = "";
}
