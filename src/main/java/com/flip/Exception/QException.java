package com.flip.Exception;

import org.apache.log4j.Logger;


public class QException extends Exception {
    private static final Logger log = Logger.getLogger(QException.class);
    private Throwable excp;
    private String message;

    public QException(Exception e) {
        this.excp = e;
        this.message = e.getMessage();
        log.error(e.getMessage(), e);
    }

    public QException(String sourceClassname, Exception e) {
        this.excp = e;
        this.message = e.getMessage();
//        QLogger.saveExceptions(sourceClassname, sourceClassname, message);
        log.error(e.getMessage(), e);
    }

    public QException(String sourceClassname, String methodname, QException e) {
        this.excp = e;
        try {
            this.message = e.getExceptionMessage();
        } catch (Exception ex) {
            this.message = e.getMessage();
        }
//        QLogger.saveExceptions(sourceClassname, methodname, e.getMessage());
        log.error(e.getMessage(), e);

    }

    public QException(String sourceClassname, String methodname, Exception e) {
        this.excp = e;

        try {
            this.message =  ((QException) e).getLocalMessage();
        } catch (Exception ex) {
            this.message = e.getMessage();
        }
       

//        QLogger.saveExceptions(sourceClassname, methodname, this.message);
    }
    
    public QException(String sourceClassname, String methodname, Throwable e) {
        this.excp = e;

        try {
            this.message =  ((QException) e).getLocalMessage();
        } catch (Exception ex) {
            this.message = e.getMessage();
        }
       

//        QLogger.saveExceptions(sourceClassname, methodname, this.message);
    }

    public QException(String message) {
        this.message = message;
    }

    public QException() {
        this.excp = null;
        this.message = "";
    }

    public String getExceptionMessage() {
        return excp.getMessage();
    }

    public String getLocalMessage() {
        return message;
    }

    public void setException(Exception e) {
        this.excp = e;
    }

    public Throwable getException() {
        return excp;
    }

}
