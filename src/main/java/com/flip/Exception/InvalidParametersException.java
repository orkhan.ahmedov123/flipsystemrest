/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.Exception;

/**
 *
 * @author otahmadov
 */
public class InvalidParametersException extends Exception {

    public InvalidParametersException(String message) {
        super(message);
    }
    
}
