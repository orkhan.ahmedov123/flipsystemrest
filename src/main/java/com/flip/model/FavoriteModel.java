/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.model;

import com.flip.domain.EntityFavoriteAnnouncement;
import com.flip.domain.SessionManager;
import com.flip.domain.Transporter;
import com.flip.utils.EntityManager;
import com.flip.view.EntityV_BasicFavoriteAnnouncement;
import com.flip.view.EntityV_FavoriteAnnouncement;
import org.apache.log4j.Logger;

/**
 *
 * @author ahmadov
 */
public class FavoriteModel {
    private static final Logger log = Logger.getLogger(FavoriteModel.class);
   public void addOrDelete() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            String operationType = t.getValue("operationType").toString();
            
            EntityFavoriteAnnouncement ent = new EntityFavoriteAnnouncement();
            if(operationType.equals("ADD")) {
                EntityManager.doInsert(ent);
            } else if(operationType.equals("DELETE")) {
                EntityManager.doDelete(ent);
            }
            
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void allView() {
        try {
            EntityV_FavoriteAnnouncement ent = new EntityV_FavoriteAnnouncement();
            EntityManager.doSelect(ent);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void getUserFavoriteAnnouncementCount() {
        try {
            EntityV_BasicFavoriteAnnouncement ent = new EntityV_BasicFavoriteAnnouncement();
            Transporter transporter = new Transporter();
            transporter.setValue("createUserId", SessionManager.getCurrentUserId());
            transporter.setValue("announcementType", "1");
            int saleCount = EntityManager.getRowCount(ent, transporter);
            ent = new EntityV_BasicFavoriteAnnouncement();
            transporter = new Transporter();
            transporter.setValue("createUserId", SessionManager.getCurrentUserId());
            transporter.setValue("announcementType", "2");
            int rentCount = EntityManager.getRowCount(ent, transporter);
            
            SessionManager.sessionTransporter().setValue("saleFavoriteCount", saleCount);
            SessionManager.sessionTransporter().setValue("rentFavoriteCount", rentCount);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
}
