/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.model;

import com.flip.domain.EntityAnnouncementComplaint;
import com.flip.domain.EntityFavoriteAnnouncement;
import com.flip.domain.SessionManager;
import com.flip.domain.Transporter;
import com.flip.utils.EntityManager;
import com.flip.view.EntityV_BasicFavoriteAnnouncement;
import com.flip.view.EntityV_FavoriteAnnouncement;
import org.apache.log4j.Logger;

/**
 *
 * @author ahmadov
 */
public class ComplaintModel {
    private static final Logger log = Logger.getLogger(ComplaintModel.class);
   public void add() {
        try {
            
            EntityAnnouncementComplaint ent = new EntityAnnouncementComplaint();
            EntityManager.doInsert(ent);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
}
