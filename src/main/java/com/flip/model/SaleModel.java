/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.model;

import com.flip.domain.EntitySale;
import com.flip.domain.EntitySaleDetails;
import com.flip.domain.EntitySaleViewCount;
import com.flip.domain.EntityUserCoinTransaction;
import com.flip.domain.EntityUsers;
import com.flip.domain.SessionManager;
import com.flip.domain.Transporter;
import com.flip.utils.EmailUtils;
import com.flip.utils.EntityManager;
import com.flip.view.EntityV_BasicFavoriteAnnouncement;
import com.flip.view.EntityV_Sale;
import com.flip.view.EntityV_SaleViewCount;
import java.util.Arrays;
import org.apache.log4j.Logger;

/**
 *
 * @author ahmadov
 */
public class SaleModel {
    private static final Logger log = Logger.getLogger(SaleModel.class);
    
    public void add() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            String operationCode = t.getValue("operationCode").toString();
            String createUserEmail = t.getValue("createUserEmail").toString();
            String flipCoin = t.getValue("flipCoin").toString();
            String email = t.getValue("email").toString();
            String sendEmail = email.trim().isEmpty() ? createUserEmail : email;
            EntitySale ent = new EntitySale();
            EntityManager.doInsertSpec(ent, t);
            String id = ent.getId();
            t.removeKey("id");
            t.removeKey("createDate");
            t.removeKey("createUserId");
            t.removeKey("updateDate");
            t.removeKey("updateUserId");
            t.removeKey("active");
            t.setValue("saleId", id);
            EntitySaleDetails details = new EntitySaleDetails();
            EntityManager.doInsertSpec(details, t);
            
            EntitySaleViewCount viewCount = new EntitySaleViewCount();
            Transporter transporter = new Transporter();
            transporter.setValue("saleId", id);
            transporter.setValue("viewCount", "0");
            EntityManager.doInsertSpec(viewCount, transporter);
            
            if(!email.trim().isEmpty()) {
                double coin = Double.valueOf(flipCoin) + 1;
                EntityUsers users = new EntityUsers();
                transporter = new Transporter();
                transporter.setValue("id", SessionManager.getCurrentUserId());
                transporter.setValue("flipCoin", coin);
                EntityManager.doUpdateSpec(users, transporter);
                
                EntityUserCoinTransaction transaction = new EntityUserCoinTransaction();
                transporter = new Transporter();
                transporter.setValue("userId", SessionManager.getCurrentUserId());
                transporter.setValue("statusId", 1002002);
                transporter.setValue("amount", "1.0");
                transporter.setValue("transactionOperationId", 1002004);
                transporter.setValue("operationStatusId", 1002001);
                EntityManager.doInsertSpec(transaction, transporter);
                
                SessionManager.sessionTransporter().setValue("balance", coin);
//                return;
            }
            
            new Thread(() -> {
                EmailUtils utils = new EmailUtils();
                try {
                    utils.sendUserMail(sendEmail, operationCode);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            
            }).start();
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void edit() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            EntitySale ent = new EntitySale();
            EntityManager.doUpdateSpec(ent, t);
            
            t.setValue("saleId", ent.getId());
            t.removeKey("id");
            t.removeKey("createDate");
            t.removeKey("createUserId");
            t.removeKey("updateDate");
            t.removeKey("updateUserId");
            t.removeKey("active");
            EntitySaleDetails details = new EntitySaleDetails();
            EntityManager.doUpdateByOtherParams(details, t, Arrays.asList("saleId"));
            
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void delete() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            
            EntitySale ent = new EntitySale();
            EntityManager.doDeleteSpec(ent, t);
            
            t.setValue("saleId", ent.getId());
            t.removeKey("id");
            t.removeKey("createDate");
            t.removeKey("createUserId");
            t.removeKey("updateDate");
            t.removeKey("updateUserId");
            t.removeKey("active");
            EntitySaleDetails details = new EntitySaleDetails();
            EntityManager.doDeleteByOtherParams(details, t, Arrays.asList("saleId"));
            
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void getSaleList() {
        try {
            String id = SessionManager.sessionTransporter().getValue("id").toString();
            String minPrice = SessionManager.sessionTransporter().getValue("minPrice").toString();
            String maxPrice = SessionManager.sessionTransporter().getValue("maxPrice").toString();
            String roomIds = SessionManager.sessionTransporter().getValue("roomCountIds").toString();
            if(!id.trim().isEmpty()) {
                EntityV_SaleViewCount count = new EntityV_SaleViewCount();
                Transporter transporter = new Transporter();
                transporter.setValue("saleId", id);
                String viewCount = EntityManager.getColumnValueByColumnName(count, "viewCount", transporter);
                viewCount = viewCount.trim().isEmpty() ? "0" : viewCount;
                int cnt = Integer.parseInt(viewCount) + 1;
                EntitySaleViewCount count2 = new EntitySaleViewCount();
                transporter = new Transporter();
                transporter.setValue("saleId", id);
                transporter.setValue("viewCount", cnt);
                EntityManager.doUpdateByOtherParams(count2, transporter, Arrays.asList("saleId"));
            }
            EntityV_Sale ent = new EntityV_Sale();
            if(!(minPrice.trim().isEmpty() && maxPrice.trim().isEmpty())) {
                String query = "";
                if(!minPrice.trim().isEmpty() && !maxPrice.trim().isEmpty()) {
                    query = " ( amount::numeric >=   '" + minPrice + "'::numeric " +
                            " AND amount::numeric <= '" + maxPrice + "'::numeric ) ";
                } else if(!minPrice.trim().isEmpty()) {
                    query = " ( amount::numeric >=   '" + minPrice + "'::numeric ) ";
                } else if(!maxPrice.trim().isEmpty()) {
                    query = " ( amount::numeric >=   '" + maxPrice + "'::numeric ) ";
                }

                ent.addSpecSqlCode(query);
            }
            if(!roomIds.trim().isEmpty()) {
                ent.setAndStatementFildForIn("room_count_id", roomIds);
            }
            EntityManager.doSelect(ent);
            
            String listtagFavorite = "";
            int favoriteCount = 0;
            EntityV_BasicFavoriteAnnouncement announcement = new EntityV_BasicFavoriteAnnouncement();
            Transporter transporter = new Transporter();
            if(!SessionManager.getCurrentUserId().equals("0")) {
                transporter.setValue("createUserId", SessionManager.getCurrentUserId());
                transporter.setValue("announcementType", "1");
                transporter = EntityManager.getEntValuesByCarrier(announcement, transporter);
                listtagFavorite = transporter.convertEntityColumnToListtag("announcementId", announcement.toTableName());
                favoriteCount = transporter.getTableRowCount(announcement.toTableName());
            }
            
            Transporter t = SessionManager.sessionTransporter();
            int rowCount = t.getTableRowCount(ent.toTableName());
            for(int i = 0; i < rowCount; i++) {
                String saleId = t.getValue(ent.toTableName(), i, "id").toString();
                if(listtagFavorite.trim().isEmpty() ||
                   listtagFavorite.indexOf(saleId) < 0) {
                    t.setValue(ent.toTableName(), i, "favoriteStatus", "0");
                    t.setValue(ent.toTableName(), i, "favoriteId", "");
                }
                else {
                    for(int j = 0; j < favoriteCount; j++) {
                        String announcementId = transporter.getValue(announcement.toTableName(), j, "announcementId").toString();
                        String favoriteId = transporter.getValue(announcement.toTableName(), j, "id").toString();
                        if(saleId.equals(announcementId)) {
                            t.setValue(ent.toTableName(), i, "favoriteId", favoriteId); 
                            break;
                        }
                        
                    }
                   t.setValue(ent.toTableName(), i, "favoriteStatus", "1"); 
                }
            }
            
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void advanceSearch() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            String roomIds = t.getValue("roomIds").toString();
            String minPrice = t.getValue("minPrice").toString();
            String maxPrice = t.getValue("maxPrice").toString();
            String minSquare = t.getValue("minSquare").toString();
            String maxSquare = t.getValue("maxSquare").toString();
            String minKitchenArea = t.getValue("minKitchenArea").toString();
            String maxKitchenArea = t.getValue("maxKitchenArea").toString();
            String minLivingSpace = t.getValue("minLivingSpace").toString();
            String maxLivingSpace = t.getValue("maxLivingSpace").toString();
            String minFloors = t.getValue("minFloors").toString();
            String maxFloors = t.getValue("maxFloors").toString();
            String minCreateDate = t.getValue("minCreateDate").toString();
            String maxCreateDate = t.getValue("maxCreateDate").toString();
            String districtIds = t.getValue("districtIds").toString();
            String settlementIds = t.getValue("settlementIds").toString();
            String metroIds = t.getValue("metroIds").toString();
            String heatingTypeIds = t.getValue("heatingTypeIds").toString();
            String repairIds = t.getValue("repairIds").toString();
            EntityV_Sale ent = new EntityV_Sale();
            String query = this.returnBetweenQuery("amount", "numeric", minPrice, maxPrice, "");
            query = this.returnBetweenQuery("square", "numeric", minSquare, maxSquare, query);
            query = this.returnBetweenQuery("kitchen_area", "numeric", minKitchenArea, maxKitchenArea, query);
            query = this.returnBetweenQuery("living_space", "numeric", minLivingSpace, maxLivingSpace, query);
            query = this.returnBetweenQuery("floors", "int", minFloors, maxFloors, query);
            query = this.returnBetweenQuery("create_date", "DATE", minCreateDate, maxCreateDate, query);
            ent.addSpecSqlCode(query);
            if(!roomIds.trim().isEmpty()) {
                ent.setAndStatementFildForIn("room_count_id", roomIds);
            }
            if(!districtIds.trim().isEmpty()) {
                ent.setAndStatementFildForIn("district_id", districtIds);
            }
            if(!settlementIds.trim().isEmpty()) {
                ent.setAndStatementFildForIn("settlement_id", settlementIds);
            }
            if(!metroIds.trim().isEmpty()) {
                ent.setAndStatementFildForIn("metro_id", metroIds);
            }
            if(!heatingTypeIds.trim().isEmpty()) {
                ent.setAndStatementFildForIn("heating_type_id", heatingTypeIds);
            }
            if(!repairIds.trim().isEmpty()) {
                ent.setAndStatementFildForIn("repair_id", repairIds);
            }
            EntityManager.doSelect(ent);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void getOwnSaleList() {
        try {
            
            EntityV_Sale ent = new EntityV_Sale();
            EntityManager.doSelect(ent);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void pushTheAdForward() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            String flipCoin = t.getValue("flipCoin").toString();
            
            EntitySale ent = new EntitySale();
            EntityManager.doUpdate(ent);
            
            double coin = Double.valueOf(flipCoin) - 1;
            EntityUsers users = new EntityUsers();
            Transporter transporter = new Transporter();
            transporter.setValue("id", SessionManager.getCurrentUserId());
            transporter.setValue("flipCoin", coin);
            EntityManager.doUpdateSpec(users, transporter);

            EntityUserCoinTransaction transaction = new EntityUserCoinTransaction();
            transporter = new Transporter();
            transporter.setValue("userId", SessionManager.getCurrentUserId());
            transporter.setValue("statusId", 1002003);
            transporter.setValue("amount", "1.0");
            transporter.setValue("transactionOperationId", 1002007);
            transporter.setValue("operationStatusId", 1002001);
            EntityManager.doInsertSpec(transaction, transporter);
            SessionManager.sessionTransporter().setValue("balance", coin);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
 
    
    private String returnBetweenQuery(String key, String dataType, String value1, String value2, String query) {
        
        if(value1.trim().isEmpty() && value2.trim().isEmpty()) return query;
        
        query = query.trim().isEmpty() ? query : query + " AND ";
        
        if(!(value1.trim().isEmpty() && value2.trim().isEmpty()) && !dataType.equals("DATE")) {
                if(!value1.trim().isEmpty() && !value2.trim().isEmpty()) {
                    query += " ( " + key + "::" + dataType + " >=   '" + value1 + "'::" + dataType +
                            " AND " + key + "::" + dataType + " <= '" + value2 + "'::" + dataType + " ) ";
                } else if(!value1.trim().isEmpty()) {
                    query += " ( " + key + "::" + dataType + " >=   '" + value1 + "'::" + dataType + ") ";
                } else if(!value2.trim().isEmpty()) {
                    query += " ( " + key + "::" + dataType + "  >=   '" + value2 + "'::" + dataType + ") ";
                }
        }
        
        if(!(value1.trim().isEmpty() && value2.trim().isEmpty()) && dataType.equals("DATE")) {
                if(!value1.trim().isEmpty() && !value2.trim().isEmpty()) {
                    query += " ( to_date( " + key + ", 'dd.mm.yyyy') >= to_date('" + value1 + "' , 'dd/mm/yyyy') " +
                            " AND to_date( " + key + ", 'dd.mm.yyyy') <= to_date('" + value2 + "' , 'dd/mm/yyyy') ) ";
                } else if(!value1.trim().isEmpty()) {
                    query += " ( to_date( " + key + ", 'dd.mm.yyyy') >=   to_date('" + value1 + "' , 'dd/mm/yyyy')) ";
                } else if(!value2.trim().isEmpty()) {
                    query += " ( to_date( " + key + ", 'dd.mm.yyyy')  <=  to_date('" + value2 + "' , 'dd/mm/yyyy')) ";
                }
        }
        
        return query;
    }
    
    public void getSimilarAnnouncement() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            String id = t.getValue("id").toString();
            
            EntityV_Sale ent = new EntityV_Sale();
            Transporter transporter = new Transporter();
            transporter.setValue("id", id);
            transporter = EntityManager.getEntValuesByCarrier(ent, transporter);
            if(transporter.getTableRowCount(ent.toTableName()) == 0) return;
            
            String query = " ( id <> '"+ id +"' ) ";
            
            String typeId = transporter.getValue(ent.toTableName(), 0, "typeId").toString();
            String cityId = transporter.getValue(ent.toTableName(), 0, "cityId").toString();
            String roomCountId = transporter.getValue(ent.toTableName(), 0, "roomCountId").toString();
            String livingSpace = transporter.getValue(ent.toTableName(), 0, "livingSpace").toString();
            String square = transporter.getValue(ent.toTableName(), 0, "square").toString();
            String landArea = transporter.getValue(ent.toTableName(), 0, "landArea").toString();
            
            ent = new EntityV_Sale();
            ent.addSpecSqlCode(query);
            transporter = new Transporter();
            transporter.setValue("typeId", typeId);
            transporter.setValue("cityId", cityId);
            transporter.setValue("roomCountId", roomCountId);
            transporter.setValue("livingSpace", livingSpace);
            transporter.setValue("square", square);
            transporter.setValue("landArea", landArea);
            transporter = EntityManager.getEntValuesByCarrier(ent, transporter);
            
            SessionManager.setSessionTransporter(transporter);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public void forgetOperationCode() {
        try {
            Transporter t = SessionManager.sessionTransporter();
            
            String sendEmail = t.getValue("createUserEmail").toString();
            String operationCode = t.getValue("operationCode").toString();
            new Thread(() -> {
                EmailUtils utils = new EmailUtils();
                try {
                    utils.sendUserMail(sendEmail, operationCode);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
}
