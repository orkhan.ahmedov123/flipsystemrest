/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.model;

import com.flip.domain.EntityDictionaries;
//import com.flip.domain.EntityGroupAccess;
//import com.flip.domain.EntityUserGroups;
import com.flip.domain.SessionManager;
import com.flip.domain.Transporter;
//import com.flip.logs.EntityV_LogsDictionaries;
import com.flip.utils.EntityManager;
import com.flip.view.EntityV_Dictionaries;
import java.util.ArrayList;
//import com.flip.view.EntityV_DictionaryTypes;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ahmadov
 */
public class DictionariesModel {
    private static final String className = "Dictionaries";
    public void add() throws Exception {
        Transporter t = SessionManager.sessionTransporter();
        String typeId = t.getValue("typeId").toString();
        String nameAz = t.getValue("nameAz").toString();
        String nameEn = t.getValue("nameEn").toString();
        String nameRu = t.getValue("nameRu").toString();
        EntityDictionaries ent = new EntityDictionaries();
        EntityManager.doInsert(ent);
        
    }
    
    public void edit() throws Exception {
        String id = SessionManager.sessionTransporter().getValue("id").toString();
        EntityDictionaries ent = new EntityDictionaries();
        EntityManager.doUpdate(ent);
    }
    
    public void delete() throws Exception {
        String id = SessionManager.sessionTransporter().getValue("id").toString();
        EntityDictionaries ent = new EntityDictionaries();
        EntityManager.doDelete(ent);
        
    }
    
    public void allView() throws Exception {
        
        EntityV_Dictionaries ent = new EntityV_Dictionaries();
        EntityManager.doSelect(ent);
    }
    
    public void getDictionaryTypeList() throws Exception {
//        EntityV_DictionaryTypes ent = new EntityV_DictionaryTypes();
//        EntityManager.doSelect(ent);
    }
    
    public void getDictionariesListByCommon() throws Exception {
        Transporter t = SessionManager.sessionTransporter();
        String typeId = t.getValue("typeId").toString();
        String parentId = t.getValue("parentId").toString();
        EntityV_Dictionaries ent = new EntityV_Dictionaries();
        if(typeId.equals("1000013") && !parentId.trim().isEmpty()) {
            t.setValue("parentIds", parentId);
            t.removeKey("parentId");
            EntityManager.doSelectByFullLikeParams(ent, Arrays.asList("parentIds"));
        } else {
            EntityManager.doSelect(ent);
        }
         
        
    }
    
    public void getDictionariesListByMultiCode() throws Exception {
        Transporter t = SessionManager.sessionTransporter();
        String tableName = "dictionaries";
        
        int rowCount = t.getTableRowCount(tableName);
        Transporter returnTransporter = new Transporter();
//        Map<String, List> returnMap = new HashMap<>();
        for(int i = 0; i < rowCount; i++) {
            String code = t.getValue(tableName, i, "code").toString();
            String parentId = t.getValue(tableName, i, "parentId").toString();
            EntityV_Dictionaries dic = new EntityV_Dictionaries();
            Transporter transporter = new Transporter();
            transporter.setValue("code", code);
            if(code.equals("metros") && !parentId.trim().isEmpty()) {
                transporter.setValue("parentIds", parentId);
                transporter = EntityManager.doSelectSpecByFullLikeParams(dic, transporter, Arrays.asList("parentIds"));
            } else {
                transporter.setValue("parentId", parentId);
                transporter = EntityManager.getEntValuesByCarrier(dic, transporter);
            }
            
            
            int count = transporter.getTableRowCount(dic.toTableName());
            List<Map<String, String>> list = new ArrayList<>();
            for(int j = 0; j < count; j++) {
                Map<String, String> map = new HashMap<>();
                map.put("id", transporter.getValue(dic.toTableName(), j, "id").toString());
                map.put("name", transporter.getValue(dic.toTableName(), j, "name").toString());
                map.put("parentId", transporter.getValue(dic.toTableName(), j, "parentId").toString());
                list.add(map);
            }
            returnTransporter.setValue(tableName, 0, code, list);
//            returnMap.put(code, list);
            
            
        }
//        returnTransporter.setValue(tableName, 0, "list", returnMap);
        SessionManager.setSessionTransporter(returnTransporter);
    }
}
