/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.controller;


import com.flip.Exception.QException;
import com.flip.db.DBConnect;
import com.flip.domain.OperationResponse;
import com.flip.domain.SessionManager;
import com.flip.domain.Transporter;
import com.flip.enums.RequiredConstants;
import com.flip.enums.ResultCode;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author otahmadov
 */
@Repository
@RestController
@RequestMapping(value = "/local", produces = MediaType.APPLICATION_JSON_VALUE)
public class LocalController {
    
    @Autowired
    private DBConnect dbConnect;
    
    @Autowired
    private RequiredConstants constants;
    
    @RequestMapping(value = "", method = RequestMethod.POST)
    protected OperationResponse forwardLocalCtrl(HttpServletRequest request) throws QException, SQLException, Throwable {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try(Connection connection = dbConnect.getConnection()) {
            String userType= request.getParameter("userType");
            String  ctrl= request.getParameter("ctrl");
            String method= request.getParameter("method");
            String json = request.getParameter("json");
            String userId = request.getParameter("userId");
            long threadId = Thread.currentThread().getId();
            Transporter carrier = new Transporter();
            carrier.fromJson(json);
            String lang = carrier.getValue("lang") != null && !carrier.getValue("lang").toString().trim().isEmpty() ? carrier.getValue("lang").toString().trim() : "az";
            SessionManager.setTransporterMap(threadId, carrier);
            SessionManager.setConnection(threadId, connection);
            SessionManager.setUserId(threadId, userId);
            SessionManager.setUserType(threadId, userType);
            SessionManager.setLang(threadId, lang);
            String  jsonType= request.getParameter("jsonType");
            callService(method, ctrl);
            Transporter result = SessionManager.sessionTransporter();
            operationResponse.setCode(ResultCode.OK);
            
            if(jsonType == null || !jsonType.equals("SIMPLE")) {
                operationResponse.setData(result.getJson());
            } else {
               operationResponse.setData( result.getOperationResponseJson()); 
            }
            SessionManager.cleanSessionThread();
            return  operationResponse;
        } 
        catch (Exception e) {
            e.printStackTrace();
            SessionManager.cleanSessionThread();
            
        } 
//        finally {
////            if(connection != null) connection.close();
//            SessionManager.cleanSessionThread();
//            
////            if (connection != null) connection.close();
////            if(Thread.currentThread().isInterrupted())  { // eger duzelmese interrupt edeceyik Thread.currentThread().interrupt();
////                System.out.println("Thread.currentThread().isInterrupted()-----close");
////                Thread.currentThread().interrupt();
////            }
//        }
        
       return  operationResponse;
    }
    
    private static void callService(String methodName, String ctrlName) throws Exception {
            
            executeControllMethods(methodName, ctrlName);
            
            if (!SessionManager.sessionTransporter().hasError()) {
                executeModelMethods(methodName, ctrlName);
            }   else {
                
            }
    }

    private static void executeControllMethods(String methodName, String ctrlName) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, InstantiationException {
        Class cl = Class.forName("com.flip.validation." + ctrlName + "Validator");
        Object o = cl.newInstance();
        Method m = cl.getMethod(methodName);
        m.invoke(o);
    }


    private static void executeModelMethods(String methodName, String ctrlName) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, InstantiationException {
        Class cl = Class.forName("com.flip.model." + ctrlName + "Model");
        Object o = cl.newInstance();
        Method m = cl.getMethod(methodName);
        m.invoke(o);
    }
    
}
