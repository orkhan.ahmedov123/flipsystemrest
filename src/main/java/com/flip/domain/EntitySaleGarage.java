/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntitySaleGarage extends LocalEntity {
private String ownerTypeStatus = "";
private String countryId = "";
private String cityId = "";
private String address = "";
private String addressLongitude = "";
private String addressLatitude = "";
private String objectBuildingTypeId = "";
private String square = "";
private String metroId = "";
private String deedOfSaleStatus = "";
private String pitsStatus = "";
private String bathroomStatus = "";
private String parkingStatus = "";
private String guardStatus = "";
private String electricityStatus = "";
private String gasStatus = "";
private String sewerageStatus = "";
private String waterSupplyStatus = "";
private String liftStatus = "";
private String developerName = "";
private String rentAmount = "";
private String youtubeLink = "";
private String title = "";
private String description = "";
private String amount = "";
private String activityStatus = "";
private String pictureIds = "";
private String createUserFullname = "";
private String createUserContact = "";
private String createUserEmail = "";
private String operationCode = "";
}
