/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityUserCoinTransaction extends LocalEntity {
    private String userId = "";
    private String statusId = "";
    private String amount = "";
    private String transactionOperationId = "";
    private String operationStatusId = "";
}
