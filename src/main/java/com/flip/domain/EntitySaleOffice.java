/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntitySaleOffice extends LocalEntity {
private String ownerTypeStatus = "";
private String countryId = "";
private String cityId = "";
private String address = "";
private String addressLongitude = "";
private String addressLatitude = "";
private String roomCountId = "";
private String repairId = "";
private String floors = "";
private String buildingFloors = "";
private String objectBuildingTypeId = "";
private String square = "";
private String elevatorStatusId = "";
private String elevatorCountId = "";
private String windowCountId = "";
private String metroId = "";
private String heatingTypeId = "";
private String deedOfSaleStatus = "";
private String mortgageStatus = "";
private String bathroomTypeId = "";
private String kitchenArea = "";
private String furnitureStatus = "";
private String airConditionerStatus = "";
private String kitchenFurnitureStatus = "";
private String guardStatus = "";
private String undergroundParkingId = "";
private String developerName = "";
private String youtubeLink = "";
private String title = "";
private String description = "";
private String amount = "";
private String activityStatus = "";

}
