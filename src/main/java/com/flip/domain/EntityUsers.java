/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author Asan
 */
@Data
public class EntityUsers extends LocalEntity {
    private String email = "";
    private String phone = "";
    private String password = "";
    private String userType = "";
    private String registerType = "";
    private String photoFileId = "";
    private String fullname = "";
    private String confirmStatus = "";
    private String flipCoin = "";
}
