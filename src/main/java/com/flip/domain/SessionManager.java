package com.flip.domain;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.security.auth.Subject;


/**
 *
 * @author otahmadov
 */
public class SessionManager {

    private static final String SEPERATOR = "__";
    private static final String LI_USER_PERMISSION_TYPE_ADMIN = "admin";
    private static final String LI_USER_PERMISSION_TYPE_USER = "user";
    public static final String DEFAULT_LANG = "az";
    public static final String SCHEMA_PREFIX = "";
    public static long THREAD_ID = 0;
    private static boolean schema = false ;

    private static Map<Long, String> userMap = new HashMap<>();
    private static Map<Long, String> langMap = new HashMap<>();
    private static Map<Long, Connection> conn = new HashMap<>();
    private static Map<Long, String> domainMap = new HashMap<>();
    private static Map<Long, String> userIdMap = new HashMap<>();
    private static Map<Long, String> personIdMap = new HashMap<>();
    private static Map<Long, String> userTypeMap = new HashMap<>();
    private static Map<Long, String> userOrgMap= new HashMap<>();
    private static Map<Long, String> userUniMap= new HashMap<>();
    private static Map<Long, String> userOrgFormulaMap = new HashMap<>();
    private static Map<Long, String> companyIdMap = new HashMap<>();
    private static Map<Long, String> domainIdMap = new HashMap<>();

    private static Map<String, Subject> permissionMap = new HashMap<>();
    private static Map<Long, Transporter> transporterMap = new HashMap<>();
    private static Object requiredConstants;

    public static void cleanSessionThread() {
        userMap.remove(getCurrentThreadId());
        langMap.remove(getCurrentThreadId());
        conn.remove(getCurrentThreadId());
        domainMap.remove(getCurrentThreadId());
        domainIdMap.remove(getCurrentThreadId());
        userIdMap.remove(getCurrentThreadId());
        personIdMap.remove(getCurrentThreadId());
        userTypeMap.remove(getCurrentThreadId());
        userOrgMap.remove(getCurrentThreadId());
        userUniMap.remove(getCurrentThreadId());
        userOrgFormulaMap.remove(getCurrentThreadId());
        companyIdMap.remove(getCurrentThreadId());
        transporterMap.remove(getCurrentThreadId());

    }

    public static void setCompanyId(Long threadId, String CompanyId) {
        companyIdMap.put(threadId, CompanyId);
    }

    public static String getCompanyId(Long threadId) {
        return companyIdMap.getOrDefault(threadId, "__2__");
    }

    public static void setDomain(Long threadId, String domain) {
        domainMap.put(threadId, domain);
    }

    public static String getDomain(Long threadId) {
        return domainMap.get(threadId);
    }
    
    public static void setRequiredConstants(Object cons) {
        requiredConstants = cons;
    }

    public static Object getRequiredConstants() {
        return requiredConstants;
    }
    
    
    

    public static String getCurrentDomain() {
        return domainMap.getOrDefault(getCurrentThreadId(), "empro");
    }

    public static Connection getCurrentConnection() {
        return conn.get(getCurrentThreadId());
    }

    public static void setConnection(Long threadId, Connection connection) {
        conn.put(threadId, connection);
    }
    
    public static void setTransporterMap(Long threadId, Transporter transporter) {
        transporterMap.put(threadId, transporter);
    }

    public static void setUserName(Long threadId, String userName) {
        userMap.put(threadId, userName);
    }

    public static void setUserId(Long threadId, String userId) {
        userIdMap.put(threadId, userId);
    }

    public static void setDomainId(Long threadId, String domain) {
        domainIdMap.put(threadId, domain);
    }

    public static void setPersonId(Long threadId, String personId) {
        personIdMap.put(threadId, personId);
    }

    public static void setUserOrg(Long threadId, String orgId) {
        userOrgMap.put(threadId, orgId);
    }

    public static void setUserUni(Long threadId, String uniId) {
        userUniMap.put(threadId, uniId);
    }

    public static void setUserOrgFormula(Long threadId, String orgFormula) {
        userOrgFormulaMap.put(threadId, orgFormula);
    }

    public static void setUserType(Long threadId, String userType) {
        userTypeMap.put(threadId, userType);
    }

    public static void setLang(Long threadId, String lang) {
        langMap.put(threadId, lang);
    }

    public static String getLang(Long threadId) {
        String lang = langMap.getOrDefault(threadId, DEFAULT_LANG);

        return lang;
    }

    public static Transporter getTransporter(Long threadId) {
        Transporter transporter = transporterMap.get(threadId);

        return transporter;
    }

    public static void setSessionTransporter(Transporter c) {
        transporterMap.put(getCurrentThreadId(), c);
    }

    public static String getCurrentLang() {
        return getLang(getCurrentThreadId());
    }

    public static Transporter sessionTransporter() {
        return getTransporter(getCurrentThreadId());
    }

    public static String getCurrentCompanyId() throws Exception {
        String id = getCompanyId(getCurrentThreadId());
        if (id.trim().length() == 0) {
            throw new Exception("company_id is not available");
        }
        return id;
    }

    public static String getUserByThreadId(Long threadId) {
        return userMap.get(threadId);
    }

    public static String getUserIdByThreadId(Long threadId) {
        return userIdMap.get(threadId);
    }

    public static String getDomainIdByThreadId(Long threadId) {
        return domainIdMap.get(threadId);
    }

    public static String getPersonIdByThreadId(Long threadId) {
        return personIdMap.get(threadId);
    }

    public static String getUserOrgByThreadId(Long threadId) {
        return userOrgMap.get(threadId);
    }

    public static String getUserUniByThreadId(Long threadId) {
        return userUniMap.get(threadId);
    }

    public static String getUserOrgFormulaByThreadId(Long threadId) {
        return userOrgFormulaMap.get(threadId);
    }

    public static String getUserTypeByThreadId(Long threadId) {
        return userTypeMap.get(threadId);
    }

    public static boolean isSchema() {
        return schema;
    }

    public static void setSchema(boolean schema) {
        SessionManager.schema = schema;
    }
    
    public static Long getCurrentThreadId() {
        return Thread.currentThread().getId();
    }
    
//    public static Long getCurrentThreadId() {
//        return THREAD_ID;
//    }

    public static String getCurrentUsername() {
        String username = "";
//        username = SessionManager.getUserByThreadId(Thread.currentThread().getId());
        //if (username == null) {
        //  username = "admin1";
        //}
        return username;
    }

    public static String getCurrentUserId() throws Exception {
        return userIdMap.get(getCurrentThreadId());
        
    }

    public static String getCurrentDomainId() throws Exception {
        return domainIdMap.get(getCurrentThreadId());
        
    }

    public static String getCurrentPersonId() throws Exception {
        return personIdMap.get(getCurrentThreadId());
        
    }

    public static String getCurrentUserOrg() throws Exception {
        return userOrgMap.get(getCurrentThreadId());
        
    }

    public static String getCurrentUserUni() throws Exception {
        return userUniMap.get(getCurrentThreadId());
        
    }

    public static String getCurrentUserOrgFormula() throws Exception {
        return userOrgFormulaMap.get(getCurrentThreadId());
        
    }

    public static String getCurrentUserType() throws Exception {
        return userTypeMap.get(getCurrentThreadId());
        
    }


    public static String getCurrentEmployeeId() throws Exception {
        try {
            String usename = SessionManager.getCurrentUsername();

            String id = "";
            if (!usename.trim().equals("")) {
//                EntityCrUser ent = new EntityCrUser();
//                ent.setUsername(usename);
//                EntityManager.select(ent);
//                id = ent.getFkEmployeeId();

            }
            return id;
        } catch (Exception ex) {
            ex.printStackTrace();
//            throw new Exception(new Object() {
//            }.getClass().getEnclosingClass().getName(),
//                    new Object() {
//            }.getClass().getEnclosingMethod().getName(), ex);
        }
        return null;
    }

    public static boolean isCurrentEmployeeAdmin() throws Exception {
        try {
            String usename = SessionManager.getCurrentUsername();
            boolean f = false;
            String type = LI_USER_PERMISSION_TYPE_USER;
            if (!usename.trim().equals("")) {
//                EntityCrUser ent = new EntityCrUser();
//                ent.setUsername(usename);
//                EntityManager.select(ent);
//                type = ent.getLiUserPermissionCode();
            }

            if (type.equals(LI_USER_PERMISSION_TYPE_ADMIN)) {
                f = true;
            }
            return f;
        } catch (Exception ex) {
//            throw new Exception(new Object() {
//            }.getClass().getEnclosingClass().getName(),
//                    new Object() {
//            }.getClass().getEnclosingMethod().getName(), ex);
        }
        return false;
    }

    public static String getCurrentTgUserId() throws Exception {
        try {
            String usename = SessionManager.getCurrentUsername();

            String id = "";
            if (!usename.trim().equals("")) {
//                EntityCrUser ent = new EntityCrUser();
//                ent.setUsername(usename);
//                EntityManager.select(ent);
//                id = ent.getTgUserId();
            }
            return id;
        } catch (Exception ex) {
//            throw new Exception(new Object() {
//            }.getClass().getEnclosingClass().getName(),
//                    new Object() {
//            }.getClass().getEnclosingMethod().getName(), ex);
        }
        return null;
    }

    public static String getFullnameOfCurrentUser() throws Exception {
        try {

//            EntityCrUser ent = new EntityCrUser();
//            ent.setDeepWhere(false);
//            ent.setUsername(SessionManager.getCurrentUsername());
//            ent.setDbname(SessionManager.getCurrentDomain());
//            ent.setStartLimit(0);
//            ent.setEndLimit(0);
//            EntityManager.select(ent);
//
//            String fullname = ent.getUserPersonName() + " "
//                    + ent.getUserPersonSurname() + " " + ent.getUserPersonMiddlename();
//            return fullname;
        } catch (Exception ex) {
//            throw new Exception(new Object() {
//            }.getClass().getEnclosingClass().getName(),
//                    new Object() {
//            }.getClass().getEnclosingMethod().getName(), ex);
        }
        return null;
    }
    
//    public static EntityCrUser getCurrentUserInfo() throws Exception {
//        try {

//            EntityCrUser ent = new EntityCrUser();
//            ent.setDeepWhere(false);
//            ent.setUsername(SessionManager.getCurrentUsername());
//            ent.setDbname(SessionManager.getCurrentDomain());
//            ent.setStartLimit(0);
//            ent.setEndLimit(0);
//            EntityManager.select(ent);

             
//            return ent;
//        } catch (Exception ex) {
//            throw new Exception(new Object() {
//            }.getClass().getEnclosingClass().getName(),
//                    new Object() {
//            }.getClass().getEnclosingMethod().getName(), ex);
//        }
//        return null;
//    }

    /*public static boolean isInWebServiceInterval(String servicename) throws Exception {
        try {
            boolean f;

            //default web service call interval per user
            CommonConfigurationProperties prop = new CommonConfigurationProperties();
            String timeInterval = prop.getProperty(WEB_SERVICE_INTERVAL);
            int timeInt = Integer.valueOf(timeInterval);

            //key=value standard of webservicecallconfiguration file
            //key = USERNAME + __ + SERVICENAME
            //value = DATE + __ + HOUR
            String username = SessionManager.getCurrentUsername();
            String key = username.trim().toUpperCase() + SEPERATOR + servicename.trim().toUpperCase();
            String value = QDate.getCurrentDate() + SEPERATOR + QDate.getCurrentTime();

            String time = "";
            WebServiceCallConfigurationProperties propWeb = new WebServiceCallConfigurationProperties();
            try {
                String v = propWeb.getProperty(key);
                String date = v.split(SEPERATOR)[0];
                if (date.trim().equals(QDate.getCurrentDate())) {
                    time = v.split(SEPERATOR)[1];
                }
            } catch (Exception e) {
            }

            if (time.length() == 0) {
                Date d = new Date();
                d = QDate.addSecond(d, (-1) * (timeInt + 10));
                time = QDate.convertTimeToString(d);
            }

            String currentTime = QDate.getCurrentTime();
            long timeDifference = QDate.getDifferenceInSeconds(QDate.convertStringToTime(time), QDate.convertStringToTime(currentTime));

            f = timeDifference > timeInt;

            if (f) {
                //yeni deyeri elave etmek lazimdir.
                propWeb.setProperty(key, value);
            }

            return f;
        } catch (Exception ex) {
            throw new Exception(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }*/
    public static boolean hasAccessToService(String serviceName) throws Exception {
        Subject subject = permissionMap.get(getCurrentUsername());
        if (subject == null) {
            loadUserPermissions();
            subject = permissionMap.get(getCurrentUsername());
        }
//        return subject.isPermitted("post:srv:" + serviceName);
        return false;
    }

    public static boolean isSysAdmin() throws Exception {
        return hasRole("SYSADMIN");
    }

    public static boolean hasRole(String roleName) throws Exception {
        Subject subject = permissionMap.get(getCurrentUsername());
        if (subject == null) {
            loadUserPermissions();
            subject = permissionMap.get(getCurrentUsername());
        }
//        return subject.hasRole(roleName);
        return false;
    }

    public static boolean hasRule(String ruleName) throws Exception {
        Subject subject = permissionMap.get(getCurrentUsername());
        if (subject == null) {
            loadUserPermissions();
            subject = permissionMap.get(getCurrentUsername());
        }
//        return subject.hasRule(ruleName);
        return false;
    }

    public static boolean isPermitted(String permission) throws Exception {
        Subject subject = permissionMap.get(getCurrentUsername());
        if (subject == null) {
            loadUserPermissions();
            subject = permissionMap.get(getCurrentUsername());
        }
//        return subject.isPermitted(permission);
        return false;
    }

    public static boolean isCurrentUserCompanyAdmin() throws Exception {
        String userId = getCurrentUserId();
//        String permissionCode = "A" + CoreLabel.IN + "AD";
//        EntityCrUser entUsr = new EntityCrUser();
//        entUsr.setDeepWhere(false);
//        entUsr.setDbname(getCurrentDomain());
//        entUsr.setId(userId);
//        entUsr.setLiUserPermissionCode(permissionCode);
//        Carrier cr = EntityManager.select(entUsr);
//        return cr.getTableRowCount(entUsr.toTableName()) > 0;
        return false;
    }

    public static boolean isCurrentUserSimpleDoctor() throws Exception {
        String userId = getCurrentUserId();
        String permissionCode = "D";
//        EntityCrUser entUsr = new EntityCrUser();
//        entUsr.setDeepWhere(false);
//        entUsr.setDbname(getCurrentDomain());
//        entUsr.setId(userId);
//        entUsr.setLiUserPermissionCode(permissionCode);
//        Carrier cr = EntityManager.select(entUsr);
//        return cr.getTableRowCount(entUsr.toTableName()) > 0;
        return false;
    }

    private static void loadUserPermissions() throws Exception {
        String userId = SessionManager.getCurrentUserId();
        Subject subject = new Subject();

//        entityCrRelUserRole.setFkUserId(userId);
//        Carrier crRelUserRole = EntityManager.select(entityCrRelUserRole);
//        
//        for(String fkRoleId: crRelUserRole.getValue("EntityCrRelUserRole", EntityCrRelUserRole.FK_ROLE_ID)) {
//            EntityCrRole entityCrRole = new EntityCrRole();
//            entityCrRole.setId(fkRoleId);
//            EntityManager.select(entityCrRole);
//            subject.addRole(entityCrRole.getRoleName());
//            
//            EntityCrRelRoleRule entityCrRelRoleRule = new EntityCrRelRoleRule();
//            entityCrRelRoleRule.setFkRoleId(fkRoleId);
//            Carrier crRelRoleRule = EntityManager.select(entityCrRelRoleRule);
//            
//            for(String fkRuleId: crRelRoleRule.getValue("EntityCrRelRoleRule", EntityCrRelRoleRule.FK_RULE_ID)) {
//                EntityCrRule entityCrRule = new EntityCrRule();
//                entityCrRule.setId(fkRuleId);
//                EntityManager.select(entityCrRule);
//                subject.addRule(entityCrRule.getRuleName());
//                
//                EntityCrRelRuleAndPermission entityCrRelRulePermission = new EntityCrRelRuleAndPermission();
//                entityCrRelRulePermission.setFkRuleId(fkRuleId);
//                Carrier crRelRulePermission = EntityManager.select(entityCrRelRulePermission);
//                
//                for(String fkPermissionId: crRelRulePermission.getValue("EntityCrRelRulePermission", EntityCrRelRuleAndPermission.FK_PERMISSION_ID)) {
//                    EntityCrPermission entityCrPermission = new EntityCrPermission();
//                    entityCrPermission.setId(fkPermissionId);
//                    EntityManager.select(entityCrPermission);
//                    subject.addPermission(entityCrPermission.getPermissionString());
//                }
//            }
//        }
//        
//        EntityCrRelUserRule entityCrRelUserRule = new EntityCrRelUserRule();
//        entityCrRelUserRule.setFkUserId(userId);
//        Carrier crRelUserRule = EntityManager.select(entityCrRelUserRule);
//        
//        for(String fkRuleId: crRelUserRule.getValue("EntityCrRelUserRule", EntityCrRelUserRule.FK_RULE_ID)) {
//            EntityCrRule entityCrRule = new EntityCrRule();
//            entityCrRule.setId(fkRuleId);
//            EntityManager.select(entityCrRule);
//            subject.addRule(entityCrRule.getRuleName());
//
//            EntityCrRelRuleAndPermission entityCrRelRulePermission = new EntityCrRelRuleAndPermission();
//            entityCrRelRulePermission.setFkRuleId(fkRuleId);
//            Carrier crRelRulePermission = EntityManager.select(entityCrRelRulePermission);
//
//            for(String fkPermissionId: crRelRulePermission.getValue("EntityCrRelRulePermission", EntityCrRelRuleAndPermission.FK_PERMISSION_ID)) {
//                EntityCrPermission entityCrPermission = new EntityCrPermission();
//                entityCrPermission.setId(fkPermissionId);
//                EntityManager.select(entityCrPermission);
//                subject.addPermission(entityCrPermission.getPermissionString());
//            }
//            
//        }
//        
//        permissionMap.put(SessionManager.getCurrentUsername(), subject);
    }

    
    
    

}
