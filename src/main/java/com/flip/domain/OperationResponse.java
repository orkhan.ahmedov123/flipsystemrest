/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.enums.ResultCode;



/**
 *
 * @author otahmadov
 */
public class OperationResponse {
    private ResultCode code;
    private String message;
    private Object serverErrMessage;
    private Object clientErrMessage;
    private Object data;

    public OperationResponse() {
    }

    public OperationResponse(ResultCode code) {
        this.code = code;
    }

    public OperationResponse(ResultCode code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ResultCode getCode() {
        return code;
    }

    public void setCode(ResultCode code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getServerErrMessage() {
        return serverErrMessage;
    }

    public void setServerErrMessage(Object serverErrMessage) {
        this.serverErrMessage = serverErrMessage;
    }

    public Object getClientErrMessage() {
        return clientErrMessage;
    }

    public void setClientErrMessage(Object clientErrMessage) {
        this.clientErrMessage = clientErrMessage;
    }

    @Override
    public String toString() {
        return "OperationResponse{" + "code=" + code + ", message=" + message + ", serverErrMessage=" + serverErrMessage + ", clientErrMessage=" + clientErrMessage + ", data=" + data + '}';
    }
}
