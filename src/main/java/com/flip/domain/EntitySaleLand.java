/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntitySaleLand extends LocalEntity {
private String ownerTypeStatus = "";
private String countryId = "";
private String cityId = "";
private String address = "";
private String addressLongitude = "";
private String addressLatitude = "";
private String fenceStatus = "";
private String gatesStatus = "";
private String landArea = "";
private String metroId = "";
private String electricityStatus = "";
private String gasStatus = "";
private String sewerageStatus = "";
private String waterSupplyStatus = "";
private String distanceMainHighway = "";
private String distanceSea = "";
private String partSellingStatus = "";
private String youtubeLink = "";
private String title = "";
private String description = "";
private String amount = "";
private String activityStatus = "";
}
