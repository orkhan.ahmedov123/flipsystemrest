/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntityDictionaryTypes extends LocalEntity {
    private String code = "";
    private String nameAz = "";
    private String nameEn = "";
    private String nameRu = "";
    private String hiddenStatus = "";
    private String showUserType = "";
}
