/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntityRentHouse extends LocalEntity {
private String ownerTypeStatus = "";
private String countryId = "";
private String cityId = "";
private String address = "";
private String addressLongitude = "";
private String addressLatitude = "";
private String roomCountId = "";
private String bedroomCount = "";
private String repairId = "";
private String mansardId = "";
private String floorCountId = "";
private String garageStatus = "";
private String poolStatus = "";
private String yardBathroomStatus = "";
private String alcoveStatus = "";
private String summerKitchenStatus = "";
private String saunaStatus = "";
private String houseArea = "";
private String landArea = "";
private String metroId = "";
private String heatingTypeId = "";
private String bathroomTypeId = "";
private String bathroomCountId = "";
private String electricityStatus = "";
private String gasStatus = "";
private String telephoneStatus = "";
private String internetStatus = "";
private String sewerageStatus = "";
private String waterSupplyStatus = "";
private String kitchenTypeId = "";
private String kitchenArea = "";
private String furnitureStatus = "";
private String airConditionerStatus = "";
private String kitchenFurnitureStatus = "";
private String securityCameraStatus = "";
private String distanceMainHighway = "";
private String distanceSea = "";
private String youtubeLink = "";
private String title = "";
private String description = "";
private String rentalPeriodId = "";
private String amount = "";
private String activityStatus = "";
}
