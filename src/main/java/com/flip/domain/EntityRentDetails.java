/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntityRentDetails extends LocalEntity {
    private String rentId = "";
    private String roomCountId = "";
    private String repairId = "";
    private String balconiesCountId = "";
    private String elevatorStatusId = "";
    private String elevatorCountId = "";
    private String windowCountId = "";
    private String metroId = "";
    private String heatingTypeId = "";
    private String bathroomTypeId = "";
    private String kitchenTypeId = "";
    private String undergroundParkingId = "";
    private String mansardId = "";
    private String floorCountId = "";
    private String bathroomCountId = "";
    private String objectBuildingTypeId = "";
    private String courtyardWindowStatus = "";
    private String streetWindowStatus = "";
    private String childrenAllowedStatus = "";
    private String animalAllowedStatus = "";
    private String onlyFamilyStatus = "";
    private String airConditionerStatus = "";
    private String kitchenFurnitureStatus = "";
    private String tvStatus = "";
    private String washerStatus = "";
    private String dishWashindStatus = "";
    private String refrigeratorStatus = "";
    private String internetStatus = "";
    private String bathroomStatus = "";
    private String showerCubicleStatus = "";
    private String parkingStatus = "";
    private String guardStatus = "";
    private String electricityStatus = "";
    private String gasStatus = "";
    private String sewerageStatus = "";
    private String waterSupplyStatus = "";
    private String liftStatus = "";
    private String garageStatus = "";
    private String poolStatus = "";
    private String yardBathroomStatus = "";
    private String alcoveStatus = "";
    private String summerKitchenStatus = "";
    private String saunaStatus = "";
    private String telephoneStatus = "";
    private String furnitureStatus = "";
    private String securityCameraStatus = "";
    private String floors = "";
    private String buildingFloors = "";
    private String roomArea = "";
    private String kitchenArea = "";
    private String curfewPayment = "";
    private String square = "";
    private String bedroomCount = "";
    private String houseArea = "";
    private String landArea = "";
    private String distanceMainHighway = "";
    private String distanceSea = "";

}
