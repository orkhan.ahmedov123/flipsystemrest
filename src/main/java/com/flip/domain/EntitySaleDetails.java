/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntitySaleDetails extends LocalEntity {
    private String saleId = "";
    private String roomCountId = "";
    private String repairId = "";
    private String balconiesCountId = "";
    private String elevatorStatusId = "";
    private String elevatorCountId = "";
    private String windowCountId = "";
    private String metroId = "";
    private String heatingTypeId = "";
    private String bathroomTypeId = "";
    private String bathroomCountId = "";
    private String kitchenTypeId = "";
    private String objectBuildingTypeId = "";
    private String mansardId = "";
    private String undergroundParkingId = "";
    private String showcaseWindowsId = "";
    private String courtyardWindowStatus = "";
    private String streetWindowStatus = "";
    private String deedOfSaleStatus = "";
    private String mortgageStatus = "";
    private String furnitureStatus = "";
    private String airConditionerStatus = "";
    private String kitchenFurnitureStatus = "";
    private String guardStatus = "";
    private String pitsStatus = "";
    private String bathroomStatus = "";
    private String parkingStatus = "";
    private String electricityStatus = "";
    private String gasStatus = "";
    private String sewerageStatus = "";
    private String waterSupplyStatus = "";
    private String liftStatus = "";
    private String garageStatus = "";
    private String poolStatus = "";
    private String utilityRoomStatus  = "";
    private String yardBathroomStatus = "";
    private String alcoveStatus = "";
    private String summerKitchenStatus = "";
    private String saunaStatus = "";
    private String telephoneStatus = "";
    private String internetStatus = "";
    private String fenceStatus = "";
    private String gatesStatus = "";
    private String partSellingStatus = "";
    private String floors = "";
    private String buildingFloors = "";
    private String roomArea = "";
    private String livingSpace = "";
    private String kitchenArea = "";
    private String square = "";
    private String developerName = "";
    private String rentAmount = "";
    private String bedroomCount = "";
    private String houseArea = "";
    private String landArea = "";
    private String distanceMainHighway = "";
    private String distanceSea = "";
    private String commendationPayment = "";

}
