/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.domain;

import com.flip.utils.LocalEntity;
import lombok.Data;

/**
 *
 * @author ahmadov
 */
@Data
public class EntityRentNewBuilding extends LocalEntity {
private String ownerType = "";
private String countryId = "";
private String cityId = "";
private String address = "";
private String addressLongitude = "";
private String addressLatitude = "";
private String roomCountId = "";
private String repairId = "";
private String balconiesCountId = "";
private String floors = "";
private String buildingFloors = "";
private String roomArea = "";
private String elevatorStatusId = "";
private String windowCountId = "";
private String courtyardWindowStatus = "";
private String streetWindowStatus = "";
private String metroId = "";
private String heatingTypeId = "";
private String childrenAllowedStatus = "";
private String animalAllowedStatus = "";
private String onlyFamilyStatus = "";
private String bathroomTypeId = "";
private String bathroomCountId = "";
private String kitchenTypeId = "";
private String kitchenArea = "";
private String airConditionerStatus = "";
private String kitchenFurnitureStatus = "";
private String tvStatus = "";
private String washerStatus = "";
private String dishWashindStatus = "";
private String refrigeratorStatus = "";
private String internetStatus = "";
private String bathroomSatus = "";
private String showerCubicleStatus = "";
private String guardStatus = "";
private String undergroundParkingId = "";
private String curfewPayment = "";
private String youtubeLink = "";
private String title = "";
private String description = "";
private String rentalPeriodId = "";
private String amount = "";
private String activityStatus = "";
}
