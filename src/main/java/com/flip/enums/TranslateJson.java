/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.enums;

/**
 *
 * @author Asan
 */
public class TranslateJson {
    public static String az = "\"~join\": \"Qoşul\",\n" +
                            "    \"~ready\": \"Görüşə hazırsınız\",\n" +
                            "    \"~join_meeting\": \"görüşə qoşuldu\",\n" +
                            "    \"~activate_by_teacher\": \"moderator tərəfindən aktiv edildi\",\n" +
                            "    \"~participant_left\": \"görüşdən çıxdı\",\n" +
                            "    \"~new_message\": \"yeni mesaj yazdı\",\n" +
                            "    \"~deactivate_by_teacher\": \"moderator tərəfindən deaktiv edildi\",\n" +
                            "    \"~has_unread_message\": \"Oxunulmamış mesaj var\",\n" +
                            "    \"~teacher_back\": \"moderator geri qayıtdı\",\n" +
                            "    \"~chat\": \"Çat\",\n" +
                            "    \"~participants\": \"İştirakçılar\",\n" +
                            "    \"~blackboard\": \"Lövhə\",\n" +
                            "    \"~screen_share\": \"Ekranı paylaş\",\n" +
                            "    \"~on_off_video\": \"Video\",\n" +
                            "    \"~on_off_audio\": \"Mikrofon\",\n" +
                            "    \"~leave\": \"Görüşdən çıx\",\n" +
                            "    \"~sidebar\": \"Yan panel\",\n" +
                            "    \"~mozaic\": \"Mozaik ekran\",\n" +
                            "    \"~settings\": \"Sazlamalar\",\n" +
                            "    \"~mute_all\": \"Mikrafonları deaktiv et\",\n" +
                            "    \"~raise_up\": \"Əl qaldır\",\n" +
                            "    \"~shareSocketNotReady\": \"Zəhmət olmasa gözləyin\",\n" +
                            "    \"~menu\": \"Menyu\",\n" +
                            "    \"~more_than_three\": \"Artıq 3 nəfər auditoriyaya qoşulub\",\n" +
                            "	\"~write_message\": \"Mesaj yazın\",\n" +
                            "    \"~list_view\": \"Kiçik görünüş\",\n" +
                            "    \"~list_video\": \"Böyük görünüş\",\n" +
                            "	\"~unmute_all\": \"Mikrafonları aktiv et\",\n" +
                            "	\"~warning_muted-by_moderator\": \"Mikrofonunuz moderator tərəfindən deaktiv edilib\",\n" +
                            "	\"~already_raiseUp\": \"Siz artıq əl qaldırmısınız!\",\n" +
                            "	\"~audio_active_by_moderator\": \"Mikrafonunuz moderator tərəfindən aktiv edildi\",\n" +
                            "	\"~audio_deactive_by_moderator\": \"Mikrafonunuz moderator tərəfindən deaktiv edildi\",\n" +
                            "	\"~no_moderator\": \"Görüşdə moderator mövcud deyil, zəhmət olmasa moderatorun qoşulmağını gözləyin\",\n" +
                            "	\"~moderator_exist\": \"Görüşdə artıq moderator mövcuddur, zəhmət olmasa başqa görüş əlavə edin və ya iştirakçı kimi qoşulun.\",\n" +
                            "	\"~no_internet_connection\": \"Zəhmət olmasa internet bağlantınızı yoxlayın\",\n" +
                            "	\"~try_solved_error\": \"Gözlənilməz xəta, yenidən qoşulmaya cəhd edilir!\",\n" +
                            "	\"~socket_error\": \"Bağlantı xətası, yenidən cəhd edilir!\",\n" +
                            "	\"~raiseUp\": \"əl qaldırıb\",\n" +
                            "	\"~preparation\": \"Hazırlanır...\",\n" +
                            "	\"~settings_in_progress\": \"Cihazlar yoxlanılır, xahiş edirik gözləyin\",\n" +
                            "	\"~record\": \"Video yazı\",\n" +
                            "	\"~stopAndDownload\": \"Dayandırın və yükləyin\",\n" +
                            "	\"~teacher_break\": \"Moderatoru gözləyin\",\n" +
                            "	\"~save\": \"Yüklə\",\n" +
                            "	\"~rubber\": \"Pozan\",\n" +
                            "	\"~clear\": \"Təmizlə\",\n" +
                            "	\"~warning_muted_by_moderator\": \"Mikrafonunuz moderator tərəfindən deaktiv edilib\",\n" +
                            "	\"~audioDoesNotMute\": \"Siz artıq moderator tərəfindən aktiv edilmisiniz\",\n" +
                            "	\"~moderator\": \"Moderator\",\n" +
                            "	\"~saveToLocal\": \"Cihazınıza yükləyin\",\n" +
                            "	\"~saveToServer\": \"Görüşə bərkidin\", \n" +
                            "	\"~fullScreen\": \"Tam ekran\",\n" +
                            "	\"~exitFullScreen\": \"Tam ekrandan çıx\",\n" +
                            "	\"~userIdAlreadyExist\": \"İstifadəçi artıq görüşdədir\",\n" +
                            "	\"~somethingWentWrong\": \"Gözlənilməz xəta\",\n" +
                            "	\"~someWhoSharedScreen\": \"Ekran və ya lövhə artıq paylaşılıb\",\n" +
                            "	\"~updateSuccess\": \"Qiymət yazıldı\", \n" +
                            "	\"~students\": \"Tələbələr\",\n" +
                            "	\"~confirm\": \"Təsdiq et\",\n" +
                            "	\"~confirmSuccess\": \"Təsdiq olundu\",\n" +
                            "	\"~journal\": \"Jurnal\",\n" +
                            "	\"~forAll\": \"Hamıya\",\n" +
                            "	\"~alreadyConfirmed\": \"Təsdiqləndi\",\n" +
                            "	\"~none\": \"yoxdur\",\n" +
                            "	\"~notBeEmpty\": \"Qiymət seçilməlidir!\",\n" +
                            "	\"~openInFull\": \"Paylaşılan ekranı böyüt\",\n" +
                            "	\"~closeFullscreen\": \"Paylaşılan ekranı kiçilt\",\n" +
                            "	\"~whiteBoardActive\": \"Lövhə aktivdir\",\n" +
                            "	\"~someWhoSharedWhiteboard\": \"Ekran və ya lövhə artıq paylaşılıb\",\n" +
                            "	\"~hasErrorCode\": \"İstisna! Xəta kodu var\",\n" +
                            "	\"~updatedTime\": \"Diqqət! Qeyd edilən vaxtda sistemdə profilaktik iş nəzərdə tutulur:\",\n" +
                            "	\"~check_cam\": \"Kamera\",\n" +
                            "	\"~check_mic\": \"Mikrafon\",\n" +
                            "	\"select_cehk_cam\": \"Kamera seçin\",\n" +
                            "	\"select_cehk_mic\": \"Mikrafon seçin\",\n" +
                            "	\"~exit\": \"Çıxış\",\n" +
                            "	\"~conversation\": \"Yazışmalar\",\n" +
                            "	\"~toAll\": \"Hamıya\",\n" +
                            "	\"~firstanme\": \"Ad\",\n" +
                            "	\"~lastname\": \"Soyad\",\n" +
                            "	\"~agenda\": \"Agenda\",\n" +
                            "	\"~startAgenda\": \"Başlat\",\n" +
                            "	\"~min\": \"dəqiqə\",\n" +
                            "	\"~stopAgenda\": \"Dayandır\",\n" +
                            "	\"~agendaUserNotConnected\": \"İştirakçı görüşdə yoxdur\",\n" +
                            "	\"~hasActiveUserAgenda\": \"hal hazırda çıxış edir\",\n" +
                            "	\"~agendaUserOffline\": \"Çıxış edən iştirakçı görüşdə yoxdur\",\n" +
                            "	\"~noFirstname\": \"Adınızı daxil edin\",\n" +
                            "	\"~noLastname\": \"Soyadınızı daxil edin\",\n" +
                            "	\"~agendaListIsEmpty\": \"Agenda yoxdur\",\n" +
                            "	\"~poll\": \"Səsvermə\",\n" +
                            "    \"~startPoll\": \"Başlat\",\n" +
                            "    \"~stopPoll\": \"Dayandır\",\n" +
                            "    \"~confirmPoll\": \"Təsdiq et\",\n" +
                            "    \"~vote\": \"səs\",\n" +
                            "    \"~pollListIsEmpty\": \"Səsvermə yoxdur\",\n" +
                            "    \"~thkyForPoll\": \"Səsiniz qeydə alındı\",\n" +
                            "    \"~hasActivePoll\": \"Hal hazırda aktiv səsvermə var\",\n" +
                            "    \"~sharePollResult\": \"Nəticəni paylaş\",\n" +
                            "    \"~pollResult\": \"Səsvermənin nəticəsi\",\n" +
                            "    \"~refreshPoll\": \"Yenidən başlat\",\n" +
                            "    \"~userNotSessionInCurrentTime\": \"Nəzərə alın ki, seçilən iştirakcı görüşdə yoxdur\",\n" +
                            "    \"~moderatorSharePoll\": \"Səsvermənin nəticəsi paylaşıldı\",\n" +
                            "    \"~saveChanges\": \"Yadda saxla\",\n" +
                            "    \"~close\": \"Geri\"";
    public static String en = "\"~join\": \"Join\",\n" +
"    \"~ready\": \"Ready for meeting\",\n" +
"    \"~join_meeting\": \"joined meeting\",\n" +
"    \"~activate_by_teacher\": \"activated by moderator\",\n" +
"    \"~participant_left\": \"left the meeting\",\n" +
"    \"~new_message\": \"wrote a new message\",\n" +
"    \"~deactivate_by_teacher\": \"deactivated by moderator\",\n" +
"    \"~has_unread_message\": \"There is an unread message\",\n" +
"    \"~teacher_back\": \"the moderator returned\",\n" +
"    \"~chat\": \"Chat\",\n" +
"    \"~participants\": \"Participants\",\n" +
"    \"~blackboard\": \"Board\",\n" +
"    \"~screen_share\": \"Screen Share\",\n" +
"    \"~on_off_video\": \"Video\",\n" +
"    \"~on_off_audio\": \"Microphone\",\n" +
"    \"~leave\": \"Leave room\",\n" +
"    \"~mozaic\": \"Mosaic mood\",\n" +
"    \"~sidebar\": \"Sidebar\",\n" +
"    \"~settings\": \"Settings\",\n" +
"    \"~mute_all\": \"Mute all\",\n" +
"    \"~raise_up\": \"Raise hand\",\n" +
"    \"~shareSocketNotReady\": \"Please, wait\",\n" +
"    \"~menu\": \"Menu\",\n" +
"    \"~more_than_three\": \"3 participants have already joined the audience\",\n" +
"	\"~write_message\": \"Write message\",\n" +
"	\"~list_view\": \"Small view\",\n" +
"	\"~list_video\": \"Large view\",\n" +
"	\"~unmute_al\": \"Mute all\",\n" +
"	\"~warning_muted-by_moderator\": \"Your microphone deactivated by moderator\",\n" +
"	\"~already_raiseUp\": \"You have already raised your hand!\",\n" +
"	\"~audio_active_by_moderator\": \"You are activated by moderator\",\n" +
"	\"~audio_deactive_by_moderator\": \"Moderator has ended your speech. \",\n" +
"	\"~no_moderator\": \"There is no moderator at the meeting, please wait for the moderator to join\",\n" +
"	\"~moderator_exist\": \"The meeting already has a moderator, please create another meeting or join as a participant.\",\n" +
"	\"~no_internet_connection\": \"Please check your internet connection\",\n" +
"	\"~try_solved_error\": \"Unexpected problem, we try to solve it!\",\n" +
"	\"~socket_error\": \"Connection error, we try to solve it!\",\n" +
"	\"~raiseUp\": \"raised his/her hand\",\n" +
"	\"~preparation\": \"Preparation...\",\n" +
"	\"~settings_in_progress\": \"Devices are being checked, please wait\",\n" +
"	\"~record\": \"Video recording\",\n" +
"	\"~stopAndDownload\": \"Stop and download\",\n" +
"	\"~teacher_break\": \"Wait for moderator\",\n" +
"	\"~save\": \"Save\",\n" +
"	\"~rubber\": \"Rubber\",\n" +
"	\"~clear\": \"Clear\",\n" +
"	\"~warning_muted_by_moderator\": \"You have been deactivated by moderator\",\n" +
"	\"~audioDoesNotMute\": \"You have already been activated by moderator\",\n" +
"	\"~moderator\": \"Moderator\",\n" +
"	\"~saveToLocal\": \"Save to your device\",\n" +
"	\"~saveToServer\": \"Save to the meeting\",\n" +
"	\"~fullScreen\": \"Full screen\",\n" +
"	\"~exitFullScreen\": \"Exit full screen\",\n" +
"	\"~userIdAlreadyExist\": \"User is already in the meeting\",\n" +
"	\"~somethingWentWrong\": \"Unexpected problem\",\n" +
"	\"~someWhoSharedScreen\": \"Screen has already been shared\",\n" +
"	\"~updateSuccess\": \"Point updated\", \n" +
"	\"~students\": \"Students\",\n" +
"	\"~confirm\": \"Confirm\",\n" +
"	\"~confirmSuccess\": \"Confirmed\",\n" +
"	\"~journal\": \"Journal\",\n" +
"	\"~forAll\": \"For all\",\n" +
"	\"~alreadyConfirmed\": \"Confirmed\",\n" +
"	\"~none\": \"none\",\n" +
"	\"~notBeEmpty\": \"Point must be selected!\",\n" +
"	\"~openInFull\": \"Full size of shared screen\",\n" +
"	\"~closeFullscreen\": \"Close full size of shared screen\",\n" +
"	\"~whiteBoardActive\": \"Board is active\",\n" +
"	\"~someWhoSharedWhiteboard\": \"Board is already shared\",\n" +
"	\"~hasErrorCode\": \"Exception! Has error code\",\n" +
"	\"~updatedTime\": \"Attention! At the shown time, preventive work is planned in the system:\",\n" +
"	\"~check_cam\": \"Camera\",\n" +
"	\"~check_mic\": \"Microphone\",\n" +
"	\"select_cehk_cam\": \"Select camera\",\n" +
"	\"select_cehk_mic\": \"Select microphone\",\n" +
"	\"~exit\": \"Leave\",\n" +
"	\"~conversation\": \"Chat\",\n" +
"	\"~toAll\": \"To all\",\n" +
"	\"~firstanme\": \"Firstname\",\n" +
"	\"~lastname\": \"Lastname\",\n" +
"	\"~agenda\": \"Agenda\",\n" +
"	\"~startAgenda\": \"Start\",\n" +
"	\"~min\": \"minute(s)\",\n" +
"	\"~stopAgenda\": \"Stop\",\n" +
"	\"~agendaUserNotConnected\": \"Participant is not in the meeting\",\n" +
"	\"~hasActiveUserAgenda\": \"is already active\",\n" +
"	\"~agendaUserOffline\": \"Participant is not in the meeting\",\n" +
"	\"~noFirstname\": \"Enter your firstname\",\n" +
"	\"~noLastname\": \"Enter your lastname\",\n" +
"	\"~agendaListIsEmpty\": \"No agenda\",\n" +
"	\"~poll\": \"Poll\",\n" +
"    \"~startPoll\": \"Start\",\n" +
"    \"~stopPoll\": \"Stop\",\n" +
"    \"~confirmPoll\": \"Confirm\",\n" +
"    \"~vote\": \"vote(s)\",\n" +
"    \"~pollListIsEmpty\": \"No poll\",\n" +
"    \"~thkyForPoll\": \"Vote is confirmed\",\n" +
"    \"~hasActivePoll\": \"There already exists active poll\",\n" +
"    \"~sharePollResult\": \"Share results\",\n" +
"    \"~pollResult\": \"Poll results\",\n" +
"    \"~refreshPoll\": \"Restart\",\n" +
"    \"~userNotSessionInCurrentTime\": \"Please note that the selected participant is not present at the meeting\",\n" +
"    \"~moderatorSharePoll\": \"Poll results are shared\",\n" +
"    \"~saveChanges\": \"Save\",\n" +
"    \"~close\": \"Back\"";
    
    public static String ru = "\"~join\": \"Присоединиться\",\n" +
"    \"~ready\": \"Готовы к встрече\",\n" +
"    \"~join_meeting\": \"подключился к встрече\",\n" +
"    \"~activate_by_teacher\": \"активирован преподавателем\",\n" +
"    \"~participant_left\": \"покинул встречу\",\n" +
"    \"~new_message\": \"написал новое сообщение\",\n" +
"    \"~deactivate_by_teacher\": \"деактивирован преподаватель\",\n" +
"    \"~has_unread_message\": \"Есть непрочитанное сообщение\",\n" +
"    \"~teacher_back\": \"учитель вернулся\",\n" +
"    \"~chat\": \"Чат\",\n" +
"    \"~participants\": \"Участники\",\n" +
"    \"~blackboard\": \"Доска\",\n" +
"    \"~screen_share\": \"Демонстрация экрана\",\n" +
"    \"~on_off_video\": \"Bидео\",\n" +
"    \"~on_off_audio\": \"Микрофон\",\n" +
"    \"~leave\": \"Покинуть комнату\",\n" +
"    \"~mozaic\": \"Весь экран\",\n" +
"    \"~sidebar\": \"Боковая панель\",\n" +
"    \"~settings\": \"Hастройки\",\n" +
"    \"~mute_all\": \"Отключить все\",\n" +
"    \"~raise_up\": \"Поднять руку\",\n" +
"    \"~shareSocketNotReady\": \"пожалуйста, подождите\",\n" +
"    \"~menu\": \"Mеню\",\n" +
"    \"~more_than_three\": \"3 участника уже присоединились к аудитории\",\n" +
"	\"~write_message\": \"Напиши сообщение\",\n" +
"	\"~list_view\": \"Маленький вид\",\n" +
"	\"~list_video\": \"Большой вид\",\n" +
"	\"~unmute_al\": \"Включить все\",\n" +
"	\"~warning_muted-by_moderator\": \"Ваш микрофон отключен учителем\",\n" +
"	\"~already_raiseUp\": \"Вы уже подняли руку!\",\n" +
"	\"~audio_active_by_moderator\": \"Вы активированы учителем\",\n" +
"	\"~audio_deactive_by_moderator\": \"Преподаватель закончил вашу речь. \",\n" +
"	\"~no_moderator\": \"На собрании нет преподаватель, подождите, пока учитель присоединится\",\n" +
"	\"~moderator_exist\": \"У встречи уже есть преподаватель, создайте другую встречу или присоединитесь как участник.\",\n" +
"	\"~no_internet_connection\": \"Пожалуйста, проверьте подключение к интернету\",\n" +
"	\"~try_solved_error\": \"Неожиданная проблема, мы пытаемся ее решить!\",\n" +
"	\"~socket_error\": \"Ошибка подключения, мы пытаемся ее решить!\",\n" +
"	\"~raiseUp\": \"поднял(а) руку\",\n" +
"	\"~preparation\": \"Подготовка...\",\n" +
"	\"~settings_in_progress\": \"Устройства проверяются, подождите\",\n" +
"	\"~record\": \"Запись видео\",\n" +
"	\"~stopAndDownload\": \"Остановить и скачать\",\n" +
"	\"~teacher_break\": \"Подождите преподавателя\",\n" +
"	\"~save\": \"Сохранить\",\n" +
"	\"~rubber\": \"Резинка\",\n" +
"	\"~clear\": \"Очистить\",\n" +
"	\"~warning_muted_by_moderator\": \"Вы были деактивированы учителем\",\n" +
"	\"~audioDoesNotMute\": \"Вы уже были активированы учителем\",\n" +
"	\"~moderator\": \"Модератор\",\n" +
"	\"~saveToLocal\": \"Сохранить на свое устройство\",\n" +
"	\"~saveToServer\": \"Сохранить на встречу\",\n" +
"	\"~fullScreen\": \"Полноэкранный\",\n" +
"	\"~exitFullScreen\": \"Выйти из полноэкранного режима\",\n" +
"	\"~userIdAlreadyExist\": \"Пользователь уже на встрече\",\n" +
"	\"~somethingWentWrong\": \"Неожиданная проблема\",\n" +
"	\"~someWhoSharedScreen\": \"Экран уже был предоставлен другим участникам\",\n" +
"	\"~updateSuccess\": \"Oценка обновляется\", \n" +
"	\"~students\": \"Ученики\",\n" +
"	\"~confirm\": \"Подтвердить\",\n" +
"	\"~confirmSuccess\": \"Подтверждено\",\n" +
"	\"~journal\": \"Журнал\",\n" +
"	\"~forAll\": \"Для всех\",\n" +
"	\"~alreadyConfirmed\": \"Подтверждено\",\n" +
"	\"~none\": \"никакой\",\n" +
"	\"~notBeEmpty\": \"Оценка должна быть выбрана!\",\n" +
"	\"~openInFull\": \"Полный размер общего экрана\",\n" +
"	\"~closeFullscreen\": \"Закрыть общий экран в полный размер\",\n" +
"	\"~whiteBoardActive\": \"Доска активна\",\n" +
"	\"~someWhoSharedWhiteboard\": \"Доска уже опубликована\",\n" +
"	\"~hasErrorCode\": \"Исключение! Имеет код ошибки\",\n" +
"	\"~updatedTime\": \"Внимание! В указанное время в системе запланированы профилактические работы:\",\n" +
"	\"~check_cam\": \"Камера\",\n" +
"	\"~check_mic\": \"Микрофон\",\n" +
"	\"select_cehk_cam\": \"Выбрать камеру\",\n" +
"	\"select_cehk_mic\": \"Выберите микрофон\",\n" +
"	\"~exit\": \"Выход\",\n" +
"	\"~conversation\": \"Чат\",\n" +
"	\"~toAll\": \"Все\",\n" +
"	\"~firstanme\": \"Имя\",\n" +
"	\"~lastname\": \"Фамилия\",\n" +
"	\"~agenda\": \"Повестка дня\",\n" +
"	\"~startAgenda\": \"Начинать\",\n" +
"	\"~min\": \"минут\",\n" +
"	\"~stopAgenda\": \"Стоп\",\n" +
"	\"~agendaUserNotConnected\": \"Участник не на встрече\",\n" +
"	\"~hasActiveUserAgenda\": \"уже активен\",\n" +
"	\"~agendaUserOffline\": \"Участник не на встрече\",\n" +
"	\"~noFirstname\": \"Введите свое имя\",\n" +
"	\"~noLastname\": \"Введите вашу фамилию\",\n" +
"	\"~agendaListIsEmpty\": \"Нет повестки дня\",\n" +
"	\"~poll\": \"Опрос\",\n" +
"    \"~startPoll\": \"Начинать\",\n" +
"    \"~stopPoll\": \"Стоп\",\n" +
"    \"~confirmPoll\": \"Подтверждать\",\n" +
"    \"~vote\": \"голос (а)\",\n" +
"    \"~pollListIsEmpty\": \"Нет опроса\",\n" +
"    \"~thkyForPoll\": \"Голосование подтверждено\",\n" +
"    \"~hasActivePoll\": \"Уже существует активный опрос\",\n" +
"    \"~sharePollResult\": \"Поделиться результатами\",\n" +
"    \"~pollResult\": \"Результаты опроса\",\n" +
"    \"~refreshPoll\": \"Перезапуск\",\n" +
"    \"~userNotSessionInCurrentTime\": \"Обратите внимание, что выбранный участник не присутствует на встрече\",\n" +
"    \"~moderatorSharePoll\": \"Результаты опроса опубликованы\",\n" +
"    \"~saveChanges\": \"Сохранить\",\n" +
"    \"~close\": \"Назад\"";
}
