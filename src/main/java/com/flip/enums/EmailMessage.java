/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.enums;

/**
 *
 * @author Asan
 */
public class EmailMessage {
    public static final String ADD_PARTICIPANT_H1 = "Bizi seçdiyiniz üçün təşəkkür edirik.";
    public static final String ADD_PARTICIPANT_P = "Zəhmət olmasa şifrənizi yeniləmək üçün kodu daxil edin:  ";
    public static final String ADD_PARTICIPANT_SUBJECT = "Yeni görüş";
}
