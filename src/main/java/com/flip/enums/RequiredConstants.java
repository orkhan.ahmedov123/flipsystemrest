/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flip.enums;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 *
 * @author ahmadov
 */
@Component
//@PropertySource("classpath:required.properties")
//@ConfigurationProperties
@Data
public class RequiredConstants {
    
    private String addDictionaries = "typeId,code,nameAz,nameEn,nameRu";
    private String editDictionaries = "id,typeId,code,nameAz,nameEn,nameRu";
    private String deleteDictionaries = "id";
    private String getDictionaries = "typeId";
    private String addUserGroups = "nameAz,nameEn,nameRu";
    private String addUserGroupOperations = "operationId";
    private String editUserGroups = "id,nameAz,nameEn,nameRu";
    private String deleteUserGroups = "id";
    private String addUser = "username,password,pincode,birthdate,genderId,citizenshipId,firstname,lastname,patronymic,groupId,organizationId,maritalId";
    private String editUser = "id,username,pincode,birthdate,genderId,citizenshipId,firstname,lastname,patronymic,groupId,organizationId";
    private String deleteUser = "id";
    private String addOrganization = "parentId,dictionaryNameId";
    private String editOrganization = "id,parentId,dictionaryNameId";
    private String deleteOrganization = "id";
    private String transferOrganization = "id,parentId";
    private String addEduYear = "name,startDate,endDate";
    private String editEduYear = "id,name,startDate,endDate";
    private String deleteEduYear = "id";
    private String addTranslate = "code,nameAz,nameEn,nameRu";
    private String editTranslate = "id,code,nameAz,nameEn,nameRu";
    private String deleteTranslate = "id";
    private String addOrder = "typeId,serial,orderDate,formId,eduLevelId";
    private String editOrder = "id,typeId,serial,orderDate,formId,eduLevelId";
    private String deleteOrder = "id";
    private String addStudentPersonalInfo = "firstname,lastname,patronymic,pincode,maritalId,citizenshipId,militaryId,genderId,birthdate";
    private String editStudentPersonalInfo = "id,firstname,lastname,patronymic,pincode,maritalId,citizenshipId,militaryId,genderId,birthdate";
    private String addContact = "personId,typeId,contact";
    private String editContact = "id,personId,typeId,contact";
    private String deleteContact = "id,personId";
    private String addAddress = "personId,typeId,addressId";
    private String editAddress = "id,personId,typeId,addressId";
    private String deleteAddress = "id,personId";
    private String addRelationShip = "personId,typeId,fullname,contact";
    private String editRelationShip = "id,personId,typeId,fullname,contact";
    private String deleteRelationShip = "id,personId";
    private String addDocument = "personId,typeId,serial,num";
    private String editDocument = "id,personId,typeId,serial,num";
    private String deleteDocument = "id,personId";
    private String addEducationDocument = "studentId,typeId,serial,num";
    private String editEducationDocument = "id,studentId,typeId,serial,num";
    private String deleteEducationDocument = "id,studentId";
    private String addStudent = "personId,inOrderId,orgId,educationTypeId,educationPaymentTypeId,educationLangId,score";
    private String editStudent = "id,personId,inOrderId,orgId,educationTypeId,educationPaymentTypeId,educationLangId,score";
    private String deleteStudent = "id,inOrderId,personId";
    private String addOrderStudent = "orderId,studentId";
    private String deleteOrderStudent = "orderId,studentId";
    private String addEducationGroup = "name,organizationId,educationLevelId,educationTypeId";
    private String editEducationGroup = "id,name,organizationId,educationLevelId,educationTypeId";
//    private String addEducationGroup = "name,organizationId,educationLevelId,educationTypeId,educationLangId,educationYearId,tyutorId";
//    private String editEducationGroup = "id,name,organizationId,educationLevelId,educationTypeId,educationLangId,educationYearId,tyutorId";
    private String deleteEducationGroup = "id";
    private String addEducationGroupStudent = "educationGroupId,studentId";
    private String deleteEducationGroupStudent = "id";
    private String viewEducationGroupStudent = "orgId,eduLevelId,educationTypeId";
    private String transferEducationGroupStudent = "educationGroupId,studentId";
    private String addTechnicalBase = "typeId,name";
    private String editTechnicalBase = "id,typeId,name";
    private String deleteTechnicalBase = "id";
    private String addAnnouncement = "organizationId,priorityTypeId,titleAz,titleEn,titleRu,contentAz,contentEn,contentRu,startDate,endDate,activityStatus";
    private String editAnnouncement = "id,organizationId,priorityTypeId,titleAz,titleEn,titleRu,contentAz,contentEn,contentRu,startDate,endDate,activityStatus";
    private String deleteAnnouncement = "id";
    private String announcementGroups = "userGroupId";
    private String addTeacher = "personId,inActionId,inActionDate,organizationId,staffTypeId,positionId,contractTypeId,teaching";
    private String editTeacher = "id,personId,inActionId,inActionDate,organizationId,staffTypeId,positionId,contractTypeId,teaching";
    private String deleteTeacher = "id,personId";
    private String addWorkDocument = "teacherId,typeId,serial,num";
    private String editWorkDocument = "id,teacherId,typeId,serial,num";
    private String deleteWorkDocument = "id,teacherId";
    private String addResearch = "personId,typeId,name,publishDate";
    private String editResearch = "id,personId,typeId,name,publishDate";
    private String deleteResearch = "id,personId";
    private String addSubjectCatalog = "subjectNameId,organizationId";
    private String editSubjectCatalog = "id,subjectNameId,organizationId";
    private String deleteSubjectCatalog = "id";
    private String addEduPlan = "name,organizationId,educationTypeId,educationLevelId";
    private String editEduPlan = "id,name,organizationId,educationTypeId,educationLevelId";
    private String deleteEduPlan = "id";
    private String addEduPlanSubjectGroup = "educationPlanId,semesterId,subjectBlockId,mHours,sHours,lHours,fmHours,inHours,outHours,credit,weekCharge,specialization,courseWork,chosenStatus";
    private String addEduPlanSubject = "subjectId,code";
    private String editEduPlanSubjectGroup = "id,educationPlanId,subjectGroupId,semesterId,subjectBlockId,mHours,sHours,lHours,fmHours,inHours,outHours,credit,weekCharge,specialization,courseWork,chosenStatus";
    private String deleteEduPlanSubjectGroup = "id,educationPlanId,subjectGroupId";
    private String addParallelSubject = "educationPlanId,educationPlanSubjectId,subjectGroupId,parallelSubjectId";
    private String addDependenceSubject = "educationPlanId,educationPlanSubjectId,subjectGroupId,dependenceSubjectId";
    private String deleteDependenceSubject = "id,educationPlanSubjectId,subjectGroupId";
    private String addEpEvaluationType = "educationPlanSubjectId,evaTypeId,subjectGroupId";
    private String addEpEvaluation = "evaTypeId,manual";
    private String addEpSubjectTopic = "educationPlanSubjectId,topicName,subjectGroupId";
    private String deleteEpSubjectTopic = "id,educationPlanSubjectId,subjectGroupId";
    private String addEpExperience = "educationPlanId,semesterId,credit,experienceId";
    private String editEpExperience = "id,educationPlanId,semesterId,credit,experienceId";
    private String addCourse = "code,educationPlanSubjectId,semesterId,educationLangId,educationPlanId,educationTypeId,educationYearId,evaluationTypeId,courseWork";
    private String editCourse = "id,code,educationPlanSubjectId,semesterId,educationLangId,educationPlanId,educationTypeId,educationYearId,evaluationTypeId,courseWork";
    private String addCourseTeacher = "courseId,teacherId,lessonTypeId";
    private String editCourseTeacher = "id,courseId,teacherId,lessonTypeId";
    
}
